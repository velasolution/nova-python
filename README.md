
DIRECTORY STRUCTURE
-------------------

      fb_crawler/             contains crawler (scrapy framework)
      service/           contains all bash files


REQUIREMENTS
------------

- Python 2.7
- Scrapy
- Mysql-python
- Elasticsearch-python
- Scrapy_splash

CONFIGURATION
-------------

### Database

Edit  the file `fb_crawler\fb_crawler\spiders\extra_config.cfg` with real data, for example:
(Note: use web-app database)

```
[mysql]
db_username = use_webapp_db
db_password = pass
db_name = use_webapp_db
db_host = 127.0.0.1
table_page = fb_page
table_post = fb_post
table_comment = fb_post_comment
table_reply = fb_post_replies
```

### Elastic

Edit the file `fb_crawler\fb_crawler\spiders\extra_config.cfg` elastic search server, for example:
(Note: use web-app database)

```
[elasticsearch]
dns = http://localhost:9200/
server = localhost
port = 9200
username =
password =
index = ainova
doc_type = data
```

INSTALLATION
------------

### Coppy and run

Change path and run script (or schedule by crontab) `\service\crawler_data_page.sh` for crawl post

Change path and run script (or schedule by crontab) `\service\crawler_data_reply.sh` for crawl reply

Change path and run script (or schedule by crontab) `\service\crawler_data_comment.sh` for crawl comment

Change path and run script (or schedule by crontab) `\service\crawler_data_reaction.sh` 

Change path and run script (or schedule by crontab) `\service\crawler_data_proxy.sh` for auto load proxy

Change path and run script (or schedule by crontab) `\service\token.sh` for remove token die

Change path and run script (or schedule by crontab) `\service\crawler_data_search.sh` for crawl facebook search by proxy

