# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import ConfigParser
from elasticsearch import Elasticsearch
from elasticsearch import helpers
import MySQLdb
import json
from scrapy.conf import settings
import urllib2, requests

class fb_crawlerPipeline(object):
    def __init__(self):
        config = ConfigParser.ConfigParser()
        config.read('extra_config.cfg')
#        self.es_doc_type = "data"
#        self.es_index = "ainova"
#        self.es = Elasticsearch(["http://localhost:9200/"])
        self.es_doc_type = config.get('elasticsearch', 'doc_type')
        self.es_index = config.get('elasticsearch', 'index')
        self.es = Elasticsearch([config.get('elasticsearch', 'dns')])
        self.es_batch = []
        self.es_batch_size = 100

    def process_item(self, item, spider):
        duplicate = self.es.exists(index=self.es_index, doc_type=self.es_doc_type, id=item['fb_post_id'])
        if(duplicate is True):
            print "==========================================="
            print "DUPLICATE"
            print "================= " + str(item['fb_post_id'].strip().encode('utf-8'))
            print "==========================================="
                                # self.updateStatusUser(response.meta["fb_post_id"])
            pass
        else:
            try:
                print "==========================================="
                print "INSERT: " + str(item['fb_post_id'].strip().encode('utf-8'))
                # print item
                print "=============== SUCCESSFULLY =============="
                print "==========================================="
                self.es.index(index=self.es_index, doc_type=self.es_doc_type, id=item['fb_post_id'], body=dict(item))
                pass
            except Exception,e:
                print "==========================================="
                print str(e)
                print "===================== ERROR ==> INSERT"
                print "==========================================="
