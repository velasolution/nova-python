# -*- coding: utf-8 -*-
import scrapy
from scrapy.conf import settings
from scrapy.http import Request
from scrapy.http import FormRequest
from elasticsearch import Elasticsearch
from elasticsearch import helpers
import time
import math
from datetime import datetime
from datetime import timedelta
from scrapy.http.headers import Headers
from scrapy_splash import SplashRequest
from scrapy.selector import Selector
import json
from pytz import timezone
import pytz
import urllib
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import openpyxl
import dateutil.parser
import calendar
import random
import six
from token_facebook import tokenfacebook
from cookie_facebooks import cookiefacebooks
import ConfigParser
import MySQLdb

class UpdateAccountSpider(scrapy.Spider):
    def __init__(self, brand_id = None, object_id=None, type=None):
        config = ConfigParser.ConfigParser()
        config.read('extra_config.cfg')
        self.extra_config = config
        self.parent_table = "fb_page"
        self.mysqConnect()
        self.DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        self.DATETIME_FORMAT_BEGIN = "%Y-%m-%d 00:00:00"
        self.DATETIME_FORMAT_END = "%Y-%m-%d 23:59:59"
        self.actions=[]

        self.xpath_box = './/div[@class="hidden_elem"]//comment()'
        self.xpath_post = './/p/text() | .//p//a//text() | .//div[@class="mtm"]//div/text() | .//div[@class="mtm"]//a/text() | (.//span[@role="presentation"])[last()]/text()'
        self.xpath_post_user_name = './/h5//a/text()'
        self.xpath_post_user_id = './/a/@data-hovercard'
        self.xpath_post_created = './/abbr/@data-utime'
        self.DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S%z"
        
        self.es_doc_type = config.get('elasticsearch', 'doc_type')
        self.es_index = config.get('elasticsearch', 'index')
        self.es = Elasticsearch([config.get('elasticsearch', 'dns')])

        tokens = tokenfacebook().getconfigToken1()
        tms = len(tokens)
        type_channel = random.randrange(0,tms)
        self.limit_docs = 50
        token = "access_token="+tokens[type_channel]["access_token"]
        self.access_token = token

        dispatcher.connect(self.spider_closed, signals.spider_closed)

    handle_httpstatus_list = [403, 404, 400]
    name = "updateInfo_mysql"
    # download_delay = 2
    allowed_domains = ["facebook.com"]

    custom_settings = {
        'URLLENGTH_LIMIT': 20830,
        'CONCURRENT_REQUESTS': 1
    }

    def start_requests(self):
        try:
            print("\n------------------------")
            sql = """SELECT id, uid FROM fb_user where status =10"""
            curr = self.query(sql)
            if curr :
                list_data = curr.fetchall()
                print list_data
                if len(list_data) > 0:
                    for row in list_data:
                        print row
                        id, uid = row
                        print id, uid
                        url_request = "https://graph.facebook.com/v1.0/"+uid+"?"+self.access_token
                        yield Request(url_request, meta={"fb_id": uid}, dont_filter = True)
                        self.update_keyword(id)
        except Exception:
            print "Error"

    def parse(self, response):
        fb_id = response.meta["fb_id"]

        try:
            info_request = json.loads(response.body_as_unicode())

            # get share count
            try:
                sex = info_request["gender"]
            except Exception:
                sex = ""

            try:
                location = info_request["location"]["name"]
            except Exception:
                location = ""
            try:
                hometown = info_request["hometown"]["name"]
            except Exception:
                hometown = ""

            try:
                education = info_request["education"][0]["school"]["name"]
            except Exception:
                education = ""

            try:
                education_type = info_request["education"][0]["type"]
            except Exception:
                education_type = ""

            try:
                relationship = info_request["relationship_status"]
            except Exception:
                relationship = ""

            work_type = ""
            try:
                list_work = info_request["work"]
                for work in list_work:
                    work_name = work["employer"]["name"]
                    work_type += work_name + "\n"
            except Exception:
                work_type = ""

            try:
                birthday = info_request["birthday"]
            except Exception:
                birthday = ""
            try:
                name = info_request["name"]
            except Exception:
                name = ""
            try:
                mobile_phone = info_request["mobile_phone"]
            except Exception:
                mobile_phone = ""
            try:
                interested_in = info_request["interested_in"]
            except Exception:
                interested_in = ""
            try:
                email = info_request["email"]
            except Exception:
                email = ""
            try:
                favorite_athletes = info_request["favorite_athletes"]["name"]
            except Exception:
                favorite_athletes = ""

            self.update_user(fb_id,relationship,"VN",hometown,mobile_phone,birthday,education,name,sex,work_type,interested_in,location,email,10,1)
            action = {
                "fb_id": fb_id,
                "location": location,
                "education": education,
                "sex": sex,
                "education_type": education_type,
                "work_type": work_type,
                "relationship": relationship,
                "birthday": birthday,
                "name": name,
                "mobile_phone": mobile_phone,
                "interested_in": interested_in,
                "email": email,
                "hometown": hometown,
                "favorite_athletes": favorite_athletes
            }
        except Exception,f:
            action = {
                "fb_id": fb_id,
                "location": "",
                "education": "",
                "sex": "",
                "education_type": "",
                "work_type": "",
                "relationship": "",
                "birthday": "",
                "name": "",
                "mobile_phone": "",
                "interested_in": "",
                "email": "",
                "hometown": "",
                "favorite_athletes": "",
            }

        self.actions.append(action)

    def mysqConnect(self):
        self.conn = MySQLdb.connect(user=self.extra_config.get('mysql','db_username'),
                                    passwd=self.extra_config.get('mysql','db_password'),
                                    db=self.extra_config.get('mysql','db_name'),
                                    host=self.extra_config.get('mysql','db_host'),
                                    charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()
    def mysqlQuery(self, sql, params=()):
        try:
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except (AttributeError, MySQLdb.OperationalError) as e:
            print 'exception generated during sql connection: ', e
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor

    def getListUid(self):
        sql = """SELECT account, keywords FROM account_keywords WHERE status = 1 AND account = %s"""
        curr = self.mysqlQuery(sql, (self.account,))
        if curr :
            return curr.fetchall()
        else:
            return []
    def update_keyword(self,_id):
        current_time = time.time()
        sql1 = """UPDATE fb_user set status=0 WHERE id = %s"""
        process_info = self.query(sql1,([_id]))

    def update_user(self,uid,relationship_status,locale,hometown,phone,birthday,education,name,gender,work,interesteds,location,email,status,created_by):
        current_time = time.time()
        sql = """INSERT INTO fb_user_info(uid,relationship_status,locale,hometown,phone,birthday,education,name,gender,work,interesteds,location,email,status,created_by,created_at,updated_at) VALUE (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
        curr = self.query(sql,(uid,relationship_status,locale,hometown,phone,birthday,education,name,gender,work,interesteds,location,email,status,created_by,current_time,current_time))

    def query(self, sql, params=()):
        try:
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except (AttributeError, MySQLdb.OperationalError) as e:
            print 'exception generated during sql connection: ', e
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor

    def spider_closed(self, spider, reason):

        print self.actions

        print "==================="
        wb = openpyxl.load_workbook('/data/python_prj/fb_crawler/user_info/example91.xlsx')
        sheet = wb.get_sheet_by_name('Sheet1')
        i = 1
        for info in self.actions:
            try:
                sheet['A' + str(i + 6)].value = str(info["fb_id"])
            except Exception:
                sheet['A' + str(i + 6)].value = info["fb_id"]
            try:
                sheet['B' + str(i + 6)].value = str(info["location"])
            except Exception:
                sheet['B' + str(i + 6)].value = info["location"]
            try:
                sheet['C' + str(i + 6)].value = str(info["education"])
            except Exception:
                sheet['C' + str(i + 6)].value = info["education"]
            try:
                sheet['D' + str(i + 6)].value = str(info["education_type"])
            except Exception:
                sheet['D' + str(i + 6)].value = info["education_type"]
            try:
                sheet['E' + str(i + 6)].value = str(info["sex"])
            except Exception:
                sheet['E' + str(i + 6)].value = info["sex"]
            try:
                sheet['F' + str(i + 6)].value = str(info["work_type"])
            except Exception:
                sheet['F' + str(i + 6)].value = info["work_type"]
            try:
                sheet['G' + str(i + 6)].value = str(info["relationship"])
            except Exception:
                sheet['G' + str(i + 6)].value = info["relationship"]
            try:
                sheet['H' + str(i + 6)].value = str(info["birthday"])
            except Exception:
                sheet['H' + str(i + 6)].value = info["birthday"]
            try:
                sheet['I' + str(i + 6)].value = str(info["name"])
            except Exception:
                sheet['I' + str(i + 6)].value = info["name"]
            try:
                sheet['I' + str(i + 6)].value = str(info["mobile_phone"])
            except Exception:
                sheet['I' + str(i + 6)].value = info["mobile_phone"]
            try:
                sheet['J' + str(i + 6)].value = str(info["interested_in"])
            except Exception:
                sheet['J' + str(i + 6)].value = info["interested_in"]
            try:
                sheet['K' + str(i + 6)].value = str(info["email"])
            except Exception:
                sheet['K' + str(i + 6)].value = info["email"]
            try:
                sheet['L' + str(i + 6)].value = str(info["hometown"])
            except Exception:
                sheet['L' + str(i + 6)].value = info["hometown"]
            try:
                sheet['M' + str(i + 6)].value = str(info["favorite_athletes"])
            except Exception:
                sheet['M' + str(i + 6)].value = info["favorite_athletes"]
            i+=1

        file_name = "DATA_EXPORT_PROFILE"
        file_name_export = str(file_name) + ".xlsx"
        wb.save("/data/python_prj/fb_crawler/user_info/"+file_name_export)
        print "END"
