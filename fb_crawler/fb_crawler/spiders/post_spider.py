# -*- encoding: utf-8 -*-
#from pip._vendor.requests import api

import scrapy
import re
import urllib2
import json
import time
import calendar
import dateutil.parser
import MySQLdb
import ConfigParser
import datetime
import sys
from elasticsearch import Elasticsearch

TAG_RE = re.compile(r'<[^>]+>')
MATCH_GROUP_NAME_RE = re.compile(r'[a-zA-Z]')

from fb_crawler.items import fb_crawlerItem
from scrapy.http import Request
import os

from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from helper import Helper
from helper_v2 import HelperV2
import random

class PostSpider(scrapy.Spider):
    def __init__(self, service_id=None,pass_day=None,limit=None,group=None, str_func=None, *args, **kwargs):
#        config = ConfigParser.ConfigParser()
#        config.read('../extra_config.cfg')

        if(group == None):
            print "You must put group before running scrape."
            sys.exit(0)

        self.group_page = group
        self.str_func = str_func
#        self.extra_config = config
#        self.parent_table = config.get('mysql','table_page')
        self.process_id = os.getpid()
#        self.mysqConnect()

#        dispatcher.connect(self.spider_closed, signals.spider_closed)
#        dispatcher.connect(self.spider_opened, signals.spider_opened)

        self.es_doc_type = "data"
        self.es_index = "ainova"
        self.es = Elasticsearch(["http://localhost:9200/"])
        self.service_id = service_id
        self.since = ""
        self.limit = ""
        if(limit == None) :
            if(pass_day == None):
                pass_day = 7
            pass_day = int(pass_day)
            s = int(time.time()) - (24*60*60*pass_day)
            self.since = "&since=" + str(s)
        else:
            self.limit = "&limit=" + str(limit)

        self.app_index = 0
#        self.setConfig()
        # self.access_token = self.getAcessToken()
        self.access_token_user = self.setConfigAccessToken()

    handle_httpstatus_list = [400,403,404]
    name = "post"
    allowed_domains = ["facebook.com"]

    def mysqConnect(self):
        self.conn = MySQLdb.connect(user=self.extra_config.get('mysql','db_username'),
                                    passwd=self.extra_config.get('mysql','db_password'),
                                    db=self.extra_config.get('mysql','db_name'),
                                    host=self.extra_config.get('mysql','db_host'),
                                    charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    def query(self, sql, params=()):
        try:
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except (AttributeError, MySQLdb.OperationalError) as e:
            print 'exception generated during sql connection: ', e
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor

    def setConfig(self):
        #self.configFacebookApp = Helper().getconfigFacebookApp+self.str_func()
        #self.configFacebookApp = Helper().globals()["getconfigFacebookApp"+self.str_func]()
        mod = globals()['Helper']()
        func = getattr(mod, "getconfigFacebookApp"+self.str_func)
        self.configFacebookApp = func()

    def setConfigAccessToken(self):
        tokens = HelperV2().getconfigToken1()
        return "access_token="+tokens[random.randrange(0, len(tokens))]["access_token"]

    def getAcessToken(self):
        app_id = self.configFacebookApp[self.app_index]['app_id']
        app_secret = self.configFacebookApp[self.app_index]['app_secret']
        api_endpoint = "https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id="+app_id+"&client_secret="+app_secret
        api_request = urllib2.Request(api_endpoint)
        api_response = urllib2.urlopen(api_request)
        return "access_token=" + json.loads(api_response.read())["access_token"]

    def isGroupName(self, page):
        if MATCH_GROUP_NAME_RE.match(page):
            return True
        return False

    def start_requests(self):
        page = 1
        limit = 1000
        dataPost = [           
            '{"page_id":"390567570966109","page_name":"kenh14.vn"}',
            '{"page_id":"1552352435062030","page_name":"Tiệm Bánh Của Mẹ"}',
            '{"page_id":"1374630332778154","page_name":"Xưởng Đóng Bàn Ghế"}',
            '{"page_id":"241732652689799","page_name":"TRÀ SỮA BOBO"}',
            '{"page_id":"786492024722015","page_name":"Dịch Vụ In Ấn - Phát Tờ Rơi"}',
            '{"page_id":"210695369058001","page_name":"foody.vn"}',
            '{"page_id":"272524772774528","page_name":"Tuyên Huyên| 宣萱| Jessica Hsuan| Jx818"}',
            '{"page_id":"1135643579831239","page_name":"Coffee Technolista"}',
            '{"page_id":"313642455434763","page_name":"FM Style Shop"}',
            '{"page_id":"371275326277591","page_name":"Việc làm tại Huế"}',
            '{"page_id":"318830008241901","page_name":"Foody.vn Cần Thơ"}',
            '{"page_id":"558545457535246","page_name":"HANA shop - hàng Nhật xách tay"}',
            '{"page_id":"1386573881660574","page_name":"now.vn sài gòn"}',
            '{"page_id":"198300300518356","page_name":"Hàng Nhật xách tay - IKID"}',
            '{"page_id":"516277795239633","page_name":"KINGF TEA"}',
            '{"page_id":"283006981907413","page_name":"Bánh bông lan trứng muối Mỹ Tho"}',
            '{"page_id":"1431628793805977","page_name":"Troll Game"}',
            '{"page_id":"1509435412435707","page_name":"theanh28 entertainment"}',
            '{"page_id":"156454297897235","page_name":"Heo Kưng Cosmetics"}',
            '{"page_id":"1755137044728338","page_name":"Tạp Hóa Online - Đồ Chơi Công Nghệ"}',
            '{"page_id":"277479312444326","page_name":"Hội Tìm Việc Làm Thêm Cho Sinh Viên Tại TP HCM"}',
            '{"page_id":"1417490071829575","page_name":"Thông tin nhà ở - việc làm tại Úc"}',
            '{"page_id":"1416013331973504","page_name":"Cộng đồng người Việt Nam tại Úc"}',
            '{"page_id":"722668454466197","page_name":"VNU Confessions"}',
            '{"page_id":"162163980605830","page_name":"Son Môi Huế -15 Phùng Hưng / 151 Bà Triệu- Huế"}',
            '{"page_id":"261308823995364","page_name":"Việc Làm An Giang"}',
            '{"page_id":"644375145575787","page_name":"Sỉ Lẻ Đủ Loại"}',
            '{"page_id":"206569683133255","page_name":"TocoToco 28B Nguyễn Khang"}',
            '{"page_id":"1416696378568167","page_name":"Abu Square"}',
            '{"page_id":"153690848014058","page_name":"Địa điểm ăn uống Sài Gòn"}',
            '{"page_id":"319126941479053","page_name":"Casa Felice"}',
            '{"page_id":"949518358511080","page_name":"La Beau"}',
            '{"page_id":"276882936005887","page_name":"Hanoi Looktique"}',
            '{"page_id":"1541329326197421","page_name":"Cười Té Ghế"}',
            '{"page_id":"1163467910432793","page_name":"Hàng Mỹ do DungSandy bán"}',
            '{"page_id":"159567087726464","page_name":"Ăn Vặt vũng tàu"}',
            '{"page_id":"1656728887939074","page_name":"Bệnh viện thẩm mỹ Kim Cương AB"}',
            '{"page_id":"127868227242849","page_name":"vietnam travel"}',
            '{"page_id":"1467295283508499","page_name":"jamja - tiết kiệm hơn"}',
            '{"page_id":"516603895096036","page_name":"Đạt Mobile - dienthoaihan.com.vn"}',
            '{"page_id":"313589082105940","page_name":"Bobapop - Taiwan Lattea"}',
            '{"page_id":"452452758098716","page_name":"AV Travel"}',
            '{"page_id":"1374692722757670","page_name":"Hạnh Sun Boutique"}',
            '{"page_id":"337853029734107","page_name":"Yến Magui"}',
            '{"page_id":"1059061844118767","page_name":"DINGTEA 238 Lê Duẩn Đà Nẵng"}',
            '{"page_id":"302693133178064","page_name":"Dương Pháp"}',
            '{"page_id":"949408848471163","page_name":"Gold Health Global - Sức khỏe Vàng Toàn cầu"}',
            '{"page_id":"519821708207340","page_name":"Art House"}',
            '{"page_id":"263385294000372","page_name":"TiBi Cosmetics and More"}',
            '{"page_id":"1455677631318349","page_name":"Trà sữa 126"}',
            '{"page_id":"1780121672262674","page_name":"TocoToco  62 Hoàng Văn Thụ-Thành Phố Bắc Giang"}',
            '{"page_id":"1459221317726533","page_name":"Đồ Gia Dụng HotDeal"}',
            '{"page_id":"489800454546526","page_name":"DING TEA Linh Đàm"}',
            '{"page_id":"1220127181408557","page_name":"Gift Cosmetics"}',
            '{"page_id":"633929393342667","page_name":"Foody.vn Đồng Nai"}',
            '{"page_id":"773816816057373","page_name":"NANA Beauty Care"}',
            '{"page_id":"973177232768126","page_name":"Luyến Ái Confession"}',
            '{"page_id":"225020207839658","page_name":"Foody.vn Sài Gòn"}',
            '{"page_id":"419823474884099","page_name":"KoiCha"}',
            '{"page_id":"185022748572988","page_name":"Beauty Garden - Hà Nội"}',
            '{"page_id":"280091202323938","page_name":"Hasaki Beauty  Spa"}',
            '{"page_id":"140144812679554","page_name":"Địa điểm ăn uống"}',
            '{"page_id":"297196556964643","page_name":"38 Degree Flowers"}',
            '{"page_id":"500304566806229","page_name":"Trà Mộc Shop Online"}',
            '{"page_id":"617904511623790","page_name":"Cao Cà Gai Leo An Xuân - Cho lá gan mãi tuổi xuân"}',
            '{"page_id":"776894339035695","page_name":"MASK Store"}',
            '{"page_id":"150013168440296","page_name":"blogtamsu chuyện chúng mình"}',
            '{"page_id":"1687504171538005","page_name":"X-Men Club"}',
            '{"page_id":"676193952414913","page_name":"Xing Cha Vietnam"}',
            '{"page_id":"858593590924862","page_name":"Barbie Luta wedding"}',
            '{"page_id":"702348586477677","page_name":"Đậu Đậu Cosmetics  Skincare"}',
            '{"page_id":"1748280385404076","page_name":"TocoToco -39 Đại Lộ Hồ Chí Minh - TP Hải Dương"}',
            '{"page_id":"1649950155319190","page_name":"TocoToco Sài Gòn"}',
            '{"page_id":"1465402500361720","page_name":"LoTA skincare - Natural  Handmade"}',
            '{"page_id":"648804785279027","page_name":"DING TEA Cao Bằng"}',
            '{"page_id":"300596996982649","page_name":"Takashi shop - Mang nước Nhật đến ngôi nhà bạn"}',
            '{"page_id":"555528764572028","page_name":"Học viên Phòng Không - Không Quân Confession"}',
            '{"page_id":"438957932967242","page_name":"Chuyên sỉ , lẻ mỹ phẩm ,túi xách - Ella Shop"}',
            '{"page_id":"546346558716119","page_name":"Trái Tim Sơn La"}',
            '{"page_id":"1694594017450437","page_name":"MỸ PHẨM GIÁ SỈ"}',
            '{"page_id":"736524279721393","page_name":"Khải-Nguyên Gia Tộc"}',
            '{"page_id":"1488047638130716","page_name":"HAPUMEDICENTER"}',
            '{"page_id":"383162305103649","page_name":"Con Gái Trà Vinh"}',
            '{"page_id":"872435882862515","page_name":"Kitchen La Fleur"}',
            '{"page_id":"309667069128727","page_name":"Shop mỹ phẩm chính hãng Nana Vũng Tàu"}',
            '{"page_id":"951327781622826","page_name":"Little Sam store"}',
            '{"page_id":"1296292230485937","page_name":"Trà Thảo Mộc Giảm Cân VyTea - Đà Nẵng- 0938700530"}',
            '{"page_id":"1035064076575367","page_name":"Yêu Plus"}',
            '{"page_id":"404921289712897","page_name":"meete.co"}',
            '{"page_id":"206739292685613","page_name":"ivivu"}',
        ]
        while True:
            print("\n------------------------")
            print("#Page: " + str(page) + "\n")
            start = (page - 1) * limit
#            sql = """SELECT id, page_id, page_type, page_name, branch, sub_branch FROM """+self.parent_table+""" WHERE group_page = %s ORDER BY id DESC LIMIT %s,%s"""
#            curr = self.query(sql,(self.group_page, start,limit))
#            if curr :
#                list_data = curr.fetchall()
#                if len(list_data) > 0:
#                    for row in list_data:
#                        id, fb_page_id, fb_page_type, fb_page_name, branch, sub_branch = row
            if len(dataPost) >0:
                if len(dataPost) >0:
                    for fb_id in dataPost:

                        output = json.loads(fb_id)
                        fb_id = output['page_id']

                        fb_page_id = fb_id
                        fb_page_type = "pages"
                        fb_page_name = output['page_name']
                        branch = "DRINK"
                        sub_branch = ""
                        graph = "posts"
                        point_involve = 0
                        if fb_page_type == "group":
                            graph = "feed"
                            point_involve = 1
                        if(self.access_token_user == None or self.access_token_user == ''):
                            self.access_token_user = self.setConfigAccessToken()

                        if fb_page_type == "group":
                            url = "https://graph.facebook.com/"+fb_page_id+"/"+graph+"/?"+self.access_token_user+"&fields=type,id,shares,link,from,message,created_time,updated_time,likes.summary(true)" + self.since + self.limit
                        else:
                            url = "https://graph.facebook.com/"+fb_page_id+"/"+graph+"/?"+self.access_token_user+"&fields=type,id,shares,link,from,message,created_time,updated_time,likes.summary(true)" + self.since + self.limit
                        if self.isGroupName(fb_page_id):
                            # print("Name: https://graph.facebook.com/"+page_id+"/?"+self.getAcessToken())
                            # url = "https://graph.facebook.com/"+page_id+"/?"+self.getAcessToken()
                            # url = "https://graph.facebook.com/search/?q="+page_id+"&"+self.getAcessToken() + "&type=group"
                            # print(url)
                            pass
                        else:
                            meta = {"fb_page_id": fb_page_id, "fb_page_name": fb_page_name, "branch": branch,"sub_branch": sub_branch, "first_page": 1, "id": id, "point_involve": point_involve}
                            # yield self.make_requests_from_url(url)
                            yield Request(url, meta=meta)
                else:
                    break
            else:
                break
            page = page + 1
            break

    def parse(self, response):
        current_time = time.time()
        jsonresponse = json.loads(response.body_as_unicode())

        if(response.status == 400 or response.status == 404):
            pass
            #if response.meta['first_page'] == 1:
                #self._set_status_code(response.status, response.meta['id'])
        if response.status == 403:
            # size_config = len(self.configFacebookApp)
            # insert app blocked to mysql
            # self._set_denied_app()
            # renew app new
            # self.app_index = self.app_index + 1
            # if self.app_index == size_config:
            #     self.app_index = 0
            # renew acesstoken
            self.access_token_user = self.setConfigAccessToken()
        else:
            if 'data' in jsonresponse:
                posts = jsonresponse['data']
                if len(posts) > 0:
                    for post in posts:
                        if 'message' in post:
                            item = fb_crawlerItem()
                            item['fb_from_name'] = ''
                            item['fb_from_uid'] = ''
                            if 'from' in post:
                                if 'name' in post['from']:
                                    item['fb_from_name'] = post['from']['name']
                                if 'id' in post['from']:
                                    item['fb_from_uid'] = post['from']['id']

                            item['fb_message'] = self.get_parse_data('message', post)
                            item['fb_created'] = ''
                            if 'created_time' in post:
                                item['fb_created'] = post['created_time']
                            else:
                                if 'updated_time' in post:
                                    item['fb_created'] = post['updated_time']

                            item['fb_updated'] = ''
                            item['fb_updated_int'] = 0
                            if 'updated_time' in post:
                                item['fb_updated'] = post['updated_time']
                                x = dateutil.parser.parse(post['updated_time'])
                                stamp = calendar.timegm(x.timetuple())
                                item['fb_updated_int'] = stamp

                            # a = post['updated_time']
                            # x.astimezone(pytz.timezone('Asia/Ho_Chi_Minh'))
                            # item['fb_updated_int'] = time.mktime(x.timetuple())

                            if 'shares' in post:
                                item['fb_share_count'] = post['shares']['count']
                            else:
                                item['fb_share_count'] = 0

                            item['fb_post_id'] = self.get_parse_data('id', post)
#                            item['fb_post_type'] = self.get_parse_data('type', post)
                            item['fb_post_type'] = 0

                            if 'link' in post:
                                item['fb_post_link'] = post['link']
                            else:
                                item['fb_post_link'] = ''

                            item['crawled_time'] = int(current_time)
                            item['fb_page_id'] = response.meta['fb_page_id']
                            item['fb_page_name'] = response.meta['fb_page_name']
                            item['branch'] = response.meta['branch']
                            item['sub_branch'] = response.meta['sub_branch']

                            like_count = 0
                            if 'likes' in post:
                                if 'summary' in post['likes']:
                                    if 'total_count' in post['likes']['summary']:
                                        like_count = post['likes']['summary']['total_count']

                            item['fb_like_count'] = like_count
                            #item['fb_data'] = json.dumps(post)
                            item['fb_data'] = ''
                            item['is_crawl'] = 0
                            item['crawled_first_time'] = item['crawled_time']
                            item['tag'] = ''
                            item['object'] = ''
                            item['is_duplicated'] = 1
                            item['is_hashed'] = 0
                            item['child_count'] = 0
                            item['parent_id'] = ''
                            item['grand_parent_id'] = ''
                            item['fb_parent_id'] = ''
                            item['fb_grand_parent_id'] = ''
                            item['post_type'] = 0
                            item['page_name_object'] = ''
                            item['group_page'] = self.group_page

                            duplicate = self.es.exists(index=self.es_index, doc_type=self.es_doc_type, id=item['fb_post_id'])
                            if(duplicate is True):
                                print "==========================================="
                                print "DUPLICATE"
                                print "================= " + str(item['fb_post_id'].strip().encode('utf-8'))
                                print "==========================================="
                                # self.updateStatusUser(response.meta["fb_post_id"])
                                pass
                            else:
                                try:
                                    print "==========================================="
                                    print "INSERT: " + str(item['fb_post_id'].strip().encode('utf-8'))

                                    # print item
                                    print "=============== SUCCESSFULLY =============="
                                    print "==========================================="

                                    self.es.index(index=self.es_index, doc_type=self.es_doc_type, id=item['fb_post_id'], body=dict(item))
                                    pass
                                except Exception,e:
                                    print "==========================================="
                                    print str(e)
                                    print "===================== ERROR ==> INSERT"
                                    print "==========================================="
#                            yield item

                    if(self.limit == ""):
                        if 'paging' in jsonresponse:
                            paging = jsonresponse['paging']
                            if 'next' in paging:
                                next = paging['next']
                                response.meta['first_page'] = 0
                                yield Request(next, meta=response.meta)
            # elif response.meta['first_page'] == 1:
            #     self._set_priority(response.meta['id'])


    def get_parse_data(self, key, data):
        if key in data:
            return data[key]
        else:
            return ''

    def spider_opened(self,spider):
        if (self.service_id != None):
            current_time = time.time()
            sql1 = """SELECT id, process_id FROM crawl_manager_service_job WHERE service_id = %s AND status = 0 AND group_page = %s AND server_version = %s ORDER BY id DESC LIMIT 1"""
            process_info = self.query(sql1,(self.service_id, self.group_page, Helper().getServerVersion()))
            list_data = process_info.fetchone()
            if(list_data):
                sql = """DELETE FROM crawl_manager_service_job WHERE id = %s"""
                self.query(sql,(list_data[0],))
                command_str = 'kill -9 ' + list_data[1]
                os.system(command_str)

            sql = """INSERT INTO crawl_manager_service_job(service_id, process_id, status, start_time, group_page, server_version) VALUE (%s, %s, %s, %s, %s, %s)"""
            curr = self.query(sql,(self.service_id, self.process_id, 0, current_time, self.group_page, Helper().getServerVersion()))

    def spider_closed(self, spider, reason):
        if (self.service_id != None):
            current_time = time.time()
            sql = """UPDATE crawl_manager_service_job SET `status` = %s, `finish_time` = %s, `finish_reason` = %s WHERE `service_id` = %s AND `process_id` = %s"""
            curr = self.query(sql,(1,current_time,reason, self.service_id, self.process_id))

    def _set_priority(self, page_id):
        sql = """UPDATE """ + self.parent_table + """ SET `is_priority` = %s WHERE `id` = %s"""
        curr = self.query(sql,(0,page_id))

    def _set_status_code(self, code, page_id):
        sql = """UPDATE """ + self.parent_table + """ SET `status_code` = %s WHERE `id` = %s"""
        curr = self.query(sql,(code,page_id))

    def _set_lastest_post(self, time_stamp, page_id):
        sql = """UPDATE """ + self.parent_table + """ SET `lastest_post` = %s WHERE `id` = %s"""
        curr = self.query(sql,(time_stamp,page_id))

    def _set_is_code(self, code, id):
        sql = """UPDATE fb_page SET `is_code` = %s WHERE `id` = %s"""
        curr = self.query(sql,(code, id))

    def _set_is_filtered_time(self, code, id):
        sql = """UPDATE fb_page SET `filtered_time` = %s WHERE `id` = %s"""
        curr = self.query(sql,(code, id))

    def _set_denied_app(self):
        current_time = time.time()
        sql = """INSERT INTO fb_denied_app(app_id, created) VALUE (%s, %s)"""
        curr = self.query(sql,(self.configFacebookApp[self.app_index]['app_id'], current_time))