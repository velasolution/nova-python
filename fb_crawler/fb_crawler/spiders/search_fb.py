# -*- coding: utf-8 -*-

from _mysql import result
import scrapy
from scrapy.conf import settings
from scrapy.http import Request
from scrapy.http import FormRequest
import MySQLdb
from elasticsearch import Elasticsearch
import time
from datetime import datetime
from datetime import timedelta
from dateutil import parser
import dateutil.parser
import calendar
from datetime import date
from scrapy.http.headers import Headers
from scrapy_splash import SplashRequest
from scrapy.selector import Selector
import json
from pytz import timezone
import pytz
import urllib
import urllib2
from random import randrange
import cStringIO
import random
import re
import requests
from token_facebook import tokenfacebook
from proxy_https_v2 import *
from fb_crawler.items import fb_crawlerItem
import ConfigParser
from helper_v2 import HelperV2


class SearchSpider(scrapy.Spider):
    def __init__(self, account=None, uid=None, real_type=None, type_channel=None):
        self.headers = Headers({
                'User-Agent'  : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
        })
        self.DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S%z"
        config = ConfigParser.ConfigParser()
        config.read('extra_config.cfg')
        self.extra_config = config
#        dispatcher.connect(self.spider_closed, signals.spider_closed)
#        dispatcher.connect(self.spider_opened, signals.spider_opened)
        self.app_index = 0
#        self.es_doc_type = "data"
#        self.es_index = "ainova"
#        self.es = Elasticsearch(["http://localhost:9200/"])

        self.es_doc_type = config.get('elasticsearch', 'doc_type')
        self.es_index = config.get('elasticsearch', 'index')
        self.es = Elasticsearch([config.get('elasticsearch', 'dns')])

        if(type_channel == None):
            self.type_channel = 0
        else:
            self.type_channel = int(type_channel)

        self.limit_docs = 50
        self.access_token = self.setConfigAccessToken()
        print self.access_token
        self.proxies = PROXIES_LIST_BUYING_HTTPS_V2
        print "[PROXY] random proxy list " + str(self.proxies)

    handle_httpstatus_list = [403, 404, 400]
    name = "searchfb"
    # download_delay = 2
    allowed_domains = ["facebook.com"]
    custom_settings = {
        'CONCURRENT_REQUESTS': 1,
#        '/home/web_crawl/wb/wb.middlewares.RandomUserAgentMiddleware': 400,
#        '/home/web_crawl/wb/wb.middlewares.RandomProxyBuying': 410,
    }
#    custom_settings = {
#        "DOWNLOADER_MIDDLEWARES": {
#            'updatePage.middlewares.RandomUserAgentMiddleware': 400,
#            'updatePage.middlewares.RandomProxyBuying': 410,
#            'wb.middlewares.RandomProxyBuyingTEST': 410,
#        }
#    }

    def mysqConnect(self):
        self.conn = MySQLdb.connect(user=self.extra_config.get('mysql','db_username'),
                                    passwd=self.extra_config.get('mysql','db_password'),
                                    db=self.extra_config.get('mysql','db_name'),
                                    host=self.extra_config.get('mysql','db_host'),
                                    charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    def query(self, sql, params=()):
        try:
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except (AttributeError, MySQLdb.OperationalError) as e:
            print 'exception generated during sql connection: ', e
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor

    def getListUid(self):
        sql = """SELECT account, keywords FROM account_keywords WHERE status = 1 AND account = %s"""
        curr = self.mysqlQuery(sql, (self.account,))
        if curr :
            return curr.fetchall()
        else:
            return []
    def setConfigAccessToken(self):
        tokens = HelperV2().getconfigToken1()
        return "access_token="+tokens[random.randrange(0, len(tokens))]["access_token"]

    def start_requests(self):
#        accounts = self.getListUid()
        account = "ainova"
#        for acc_info in accounts:
#            account, keywords = acc_info
#            keywords = keywords.split(";")
        keywords_list =u'trà sữa;tra sua'
        keywords = keywords_list.split(";")
        for keyword in keywords:
            keyword = u''.join(keyword).encode('utf-8').strip()
            url_request = "https://m.facebook.com/search/live_conversation/?bqf=stories-live(stories-keyword("+keyword+"))"
            meta = {"account": account, "keyword": keyword, "page": 1}
            requests = Request(url_request, headers=self.headers,meta=meta)
            pick = random.choice(self.proxies)
            print "[PROXY] picked proxy "  + pick + " for request "

            requests.meta['proxy'] = pick
#                requests.meta["proxy"]='https://68.183.133.23:80'
            yield requests

    def parse(self, response):
        content = response.body
        fb_ids = content.split('result_id&quot;:&quot;')
        i = 0
        fb_data = []
        for string_fb in fb_ids:
            if i > 0:
                string_fb = string_fb.split(':')[0].strip()
                if string_fb not in fb_data:
                    fb_data.append(string_fb)
                    duplicate = self.es.exists(index=self.es_index, doc_type=self.es_doc_type, id=string_fb)
                    if(duplicate is True):
                        print "==========================================="
                        print "DUPLICATE"
                        print "================= " + str(string_fb.strip().encode('utf-8'))
                        print "==========================================="
                        pass
                    else:
                        print "==========================================="
                        print "GET POST ID"
                        print "==========================================="
                        meta = {"fb_post_id": string_fb, "branch" : "PERSONAL", "sub_branch": "", "account": response.meta["account"]}
                        url_request = "https://graph.facebook.com/v1.0/"+str(string_fb)+"?"+self.access_token
                        yield Request(url_request, callback=self.parse_result_info, meta=meta)
            i=i+1

        print "================"
        print fb_data
        print "================"

        # print "==========================="
        # filename = "response.html"
        # open(filename, 'wb').write(response.body)
        # print "==========================="
    def getInfoByPostId(self, post_id):
        api_endpoint = "https://graph.facebook.com/v1.0/"+post_id+"?"+self.access_token
#        access_token=EAAAAUaZA8jlABAKvm9DDwP8rAdsWgfyfVh20MYYlXL0Ku8NBHwuCGNTRAUD7POZBn1wD75iitMbi0wLPe5JQy5vH7Y7KRSgpYUAO4RY8GywF1HZCcI9bOlUEUZCpzQwaer8PbomKaQGKuvZBXEIvMZBOvxHfqpXYyu5yroCXqrp8Buarn84efC"

        print "==================="
        print api_endpoint
        print "==================="

        api_request = urllib2.Request(api_endpoint)
        api_response = urllib2.urlopen(api_request)
        return json.loads(api_response.read())

    def parse_result_info(self, response):
        true_info = json.loads(response.body)

        info = {}
        info["fb_user_id"] = true_info["from"]["id"]
        info["fb_user_name"] = true_info["from"]["name"]
        info["fb_share"] = 0
        info["fb_like"] = 0
        info["fb_message"] = ""
        if "message" in true_info:
            info["fb_message"] = true_info["message"]

        if "story" in true_info:
            info["fb_message"] = info["fb_message"] +" "+ true_info["story"]

        if "description" in true_info:
            info["fb_message"] = info["fb_message"] +" "+ true_info["description"]

        if "name" in true_info:
            info["fb_message"] = info["fb_message"] +" "+ true_info["name"]

        print info["fb_message"]

        if "updated_time" in true_info:
            x = dateutil.parser.parse(true_info['updated_time'])
        elif "created_time" in true_info:
            x = dateutil.parser.parse(true_info['created_time'])

        if "updated_time" in true_info:
            info["fb_created"] = true_info['updated_time']
        if "created_time" in true_info:
            info["fb_created"] = true_info['created_time']

        time_int = calendar.timegm(x.timetuple())

        current_time = int(time.time())

        item = fb_crawlerItem()
        # INFO INSERT DATA
        item['fb_from_name'] = info["fb_user_name"]
        item['fb_from_uid'] = str(info["fb_user_id"])
        item['fb_message'] = info["fb_message"]
        item['fb_created'] = info["fb_created"]
        item['fb_updated'] = info["fb_created"]
        item['fb_updated_int'] = time_int
        item['fb_share_count'] = info["fb_share"]
        item['is_duplicated'] = 2

        item['fb_post_id'] = response.meta["fb_post_id"]
        item['fb_post_type'] = 0
        item['fb_post_link'] = ''
        item['crawled_time'] = int(current_time)
        item['fb_page_id'] = str(info["fb_user_id"])
        item['fb_page_name'] = info["fb_user_name"]
        item['branch'] = response.meta['branch']
        item['sub_branch'] = response.meta['sub_branch']

        item['fb_like_count'] = info["fb_like"]
        item['fb_data'] = ''
        item['is_crawl'] = 0
        item['crawled_first_time'] = item['crawled_time']
        item['tag'] = ''
        item['object'] = ''
        item['is_hashed'] = 0
        item['child_count'] = 0
        item['parent_id'] = ''
        item['grand_parent_id'] = ''
        item['fb_parent_id'] = ''
        item['fb_grand_parent_id'] = ''
        item['post_type'] = 0
        item['page_name_object'] = ''
        item['group_page'] = "UGR1"

        if (item['fb_post_id'].find('_') != -1):
            duplicate = self.es.exists(index=self.es_index, doc_type=self.es_doc_type, id=item['fb_post_id'])
            if(duplicate is True):
                print "==========================================="
                print "DUPLICATE"
                print "================= " + str(response.meta["fb_post_id"].strip().encode('utf-8'))
                print "==========================================="
                # self.updateStatusUser(response.meta["fb_post_id"])
                pass
            else:
                try:
                    print "==========================================="
                    print "INSERT: " + str(response.meta["fb_post_id"].strip().encode('utf-8'))

                # insert to DB mysql
#                self._set_raw(response.meta["fb_post_id"], item)

                # update status DB
#                self.updateStatusUser(response.meta["fb_post_id"])

                # print item
                    print "=============== SUCCESSFULLY =============="
                    print "==========================================="

                    self.es.index(index=self.es_index, doc_type=self.es_doc_type, id=item['fb_post_id'], body=dict(item))
                    pass
                except Exception,e:
                    print "==========================================="
                    print str(e)
                    print "===================== ERROR ==> INSERT"
                    print "==========================================="
                    actions_error = {
                    "fb_id": response.meta["fb_post_id"]
                    }
        else: 
            print ("id ngan")
            post_id_new = item['fb_from_uid']+"_"+item['fb_post_id']
            print post_id_new
            meta_new = {"fb_post_id": post_id_new, "branch" : "PERSONAL", "sub_branch": ""}
            url_request_new = "https://graph.facebook.com/v1.0/"+str(post_id_new)+"?"+self.access_token
            yield Request(url_request_new, callback=self.parse_result_info2, meta=meta_new)

    def parse_result_info2(self, response):
        true_info = json.loads(response.body)

        info = {}
        info["fb_user_id"] = true_info["from"]["id"]
        info["fb_user_name"] = true_info["from"]["name"]
        info["fb_share"] = 0
        info["fb_like"] = 0
        info["fb_message"] = ""
        if "message" in true_info:
            info["fb_message"] = true_info["message"]

        if "story" in true_info:
            info["fb_message"] = info["fb_message"] +" "+ true_info["story"]

        if "description" in true_info:
            info["fb_message"] = info["fb_message"] +" "+ true_info["description"]

        if "name" in true_info:
            info["fb_message"] = info["fb_message"] +" "+ true_info["name"]

        print info["fb_message"]

        if "updated_time" in true_info:
            x = dateutil.parser.parse(true_info['updated_time'])
        elif "created_time" in true_info:
            x = dateutil.parser.parse(true_info['created_time'])

        if "updated_time" in true_info:
            info["fb_created"] = true_info['updated_time']
        elif "created_time" in true_info:
            info["fb_created"] = true_info['created_time']

        time_int = calendar.timegm(x.timetuple())

        current_time = int(time.time())

        item = fb_crawlerItem()
        # INFO INSERT DATA
        item['fb_from_name'] = info["fb_user_name"]
        item['fb_from_uid'] = str(info["fb_user_id"])
        item['fb_message'] = info["fb_message"]
        item['fb_created'] = info["fb_created"]
        item['fb_updated'] = info["fb_created"]
        item['fb_updated_int'] = time_int
        item['fb_share_count'] = info["fb_share"]
        item['is_duplicated'] = 2

        item['fb_post_id'] = response.meta["fb_post_id"]
        item['fb_post_type'] = 0
        item['fb_post_link'] = ''
        item['crawled_time'] = int(current_time)
        item['fb_page_id'] = str(info["fb_user_id"])
        item['fb_page_name'] = info["fb_user_name"]
        item['branch'] = response.meta['branch']
        item['sub_branch'] = response.meta['sub_branch']

        item['fb_like_count'] = info["fb_like"]
        item['fb_data'] = ''
        item['is_crawl'] = 0
        item['crawled_first_time'] = item['crawled_time']
        item['tag'] = ''
        item['object'] = ''
        item['is_hashed'] = 0
        item['child_count'] = 0
        item['parent_id'] = ''
        item['grand_parent_id'] = ''
        item['fb_parent_id'] = ''
        item['fb_grand_parent_id'] = ''
        item['post_type'] = 0
        item['page_name_object'] = ''
        item['group_page'] = "UGR1"

        

        duplicate = self.es.exists(index=self.es_index, doc_type=self.es_doc_type, id=item['fb_post_id'])
        if(duplicate is True):
            print "==========================================="
            print "DUPLICATE"
            print "================= " + str(response.meta["fb_post_id"].strip().encode('utf-8'))
            print "==========================================="
            # self.updateStatusUser(response.meta["fb_post_id"])
            pass
        else:
            try:
                print "==========================================="
                print "INSERT: " + str(response.meta["fb_post_id"].strip().encode('utf-8'))

                # insert to DB mysql
#                self._set_raw(response.meta["fb_post_id"], item)

                # update status DB
#                self.updateStatusUser(response.meta["fb_post_id"])

                # print item
                print "=============== SUCCESSFULLY =============="
                print "==========================================="

                self.es.index(index=self.es_index, doc_type=self.es_doc_type, id=item['fb_post_id'], body=dict(item))
                pass
            except Exception,e:
                print "==========================================="
                print str(e)
                print "===================== ERROR ==> INSERT"
                print "==========================================="
                actions_error = {
                "fb_id": response.meta["fb_post_id"]
                }