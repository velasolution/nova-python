# -*- encoding: utf-8 -*-

import scrapy
import urllib2
import json
import time
import MySQLdb
from fb_crawler.items import fb_crawlerItem
from scrapy.http import Request
import ConfigParser
import os
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import sys
from datetime import datetime
from datetime import timedelta
import math
import random

#import pymongo
#from pymongo import MongoClient
from scrapy.utils.project import get_project_settings
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from helper import Helper
from helper_v2 import HelperV2


class UpdateReactionSpider(scrapy.Spider):
    def __init__(self,service_id=None,group=None, str_func=None, *args, **kwargs):
        config = ConfigParser.ConfigParser()
        config.read('extra_config.cfg')
        self.extra_config = config

        if(group == None):
            print "You must put group before running scrape."
            sys.exit(0)

        self.group_page = group
        self.str_func = str_func
#        dispatcher.connect(self.spider_closed, signals.spider_closed)
#        dispatcher.connect(self.spider_opened, signals.spider_opened)

        self.service_id = service_id
        self.process_id = os.getpid()
        self.app_index = 0
        # self.setConfig()
        # self.access_token = self.getAcessToken()
        self.access_token = self.getAcessToken()
        self.settings = get_project_settings()
#        self.es_doc_type = "data"
#        self.es_index = "ainova"
#        self.es = Elasticsearch(["http://localhost:9200/"])

        self.es_doc_type = config.get('elasticsearch', 'doc_type')
        self.es_index = config.get('elasticsearch', 'index')
        self.es = Elasticsearch([config.get('elasticsearch', 'dns')])
        s = int(time.time()) - (24*60*60*7)
        self.since = "&since=" + str(s)
        self.limit = "&limit=500"
        self.actions=[]
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    handle_httpstatus_list = [403]
    name = "update_reaction"
    allowed_domains = ["facebook.com"]

    def mongoConnect(self):
        client = MongoClient(self.extra_config.get('mongodb', 'host'), int(self.extra_config.get('mongodb', 'port')))
        self.mongoDb = client.crawler

    #mysql connection db
    def mysqConnect(self):
        self.conn = MySQLdb.connect(user=self.extra_config.get('mysql','db_username'),
                                    passwd=self.extra_config.get('mysql','db_password'),
                                    db=self.extra_config.get('mysql','db_name'),
                                    host=self.extra_config.get('mysql','db_host'),
                                    charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    # mysql query db
    def mysqlQuery(self, sql, params=()):
        try:
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except (AttributeError, MySQLdb.OperationalError) as e:
            print 'exception generated during sql connection: ', e
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor

    # config app
    # def setConfig(self):
    #     #self.configFacebookApp = Helper().getconfigFacebookApp01()
    #     mod = globals()['Helper']()
    #     func = getattr(mod, "getconfigFacebookApp"+self.str_func)
    #     self.configFacebookApp = func()

    def getAcessToken(self):
        tokens = HelperV2().getconfigToken1()
        return "access_token="+tokens[random.randrange(0, len(tokens))]["access_token"]

    # #get fb accesstoken
    # def getAcessToken(self):
    #     app_id = self.configFacebookApp[self.app_index]['app_id']
    #     app_secret = self.configFacebookApp[self.app_index]['app_secret']
    #
    #     api_endpoint = "https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id="+app_id+"&client_secret="+app_secret
    #     api_request = urllib2.Request(api_endpoint)
    #     api_response = urllib2.urlopen(api_request)
    #     return "access_token=" + json.loads(api_response.read())["access_token"]

    def start_requests(self):

        diff = timedelta(days=7)
        now = datetime.now()
        passed = now - diff
        begin_time = int(time.mktime(passed.timetuple()))
        end_time = int(time.mktime(now.timetuple()))
        print begin_time
        print end_time

        page = 1
        limit = 1000
        while True:
            if(page != 0):
                begin = ((page-1)*limit);
            else :
                begin = 0;
            self.es.indices.refresh(index=self.es_index)
            docs = {
                "sort" : [
                    { "fb_like_count": { "order": "desc" }},
                    { "fb_share_count": { "order": "desc"}}
                ],
                "_source": ["fb_like_count", "fb_share_count", "fb_post_id", "child_count"],
                "query": {
#                    "filtered" : {
#                        "query" : {
                    "bool" : {
                        "must" : {
                            "match": { "group_page": self.group_page }
                        },
                        "filter" : {
                            "bool" : {
                                "must" : [
                                    {"term" : { "post_type" : 0 }},
                                    {"term" : { "is_crawl" : 0 }},
                                    {"range" : {
                                            "crawled_time" : {
                                                "gte" : ""+str(begin_time)+"",
                                                "lte":""+str(end_time)+""
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    }
                },
                "size": limit,
                "from" : begin
            }
            res = self.es.search(index=self.es_index, body=docs,request_timeout=30)
            if(len(res['hits']['hits']) == 0):
                break

            if(self.access_token == None or self.access_token == ''):
                self.access_token = self.getAcessToken()

            for hit in res['hits']['hits']:
                info = hit["_source"]
                fb_post_id = info["fb_post_id"]
                fb_like_count = info["fb_like_count"]
                fb_share_count = info["fb_share_count"]
#                point_involve = info["point_involve"]
                child_count = info["child_count"]
                id = hit["_id"]

                #self.mongoDb[self.mongo_collection].update({'_id': id}, {'$set': {'is_crawl': 1}}, True)
                meta={'fb_post_id':fb_post_id, 'fb_like_count':fb_like_count, 'fb_share_count':fb_share_count, 'child_count': child_count, '_id': id}

                url = "https://graph.facebook.com/v1.0/"+fb_post_id+"?"+self.access_token
                yield Request(url, meta=meta)

                # self.es.update(index=self.es_index, doc_type=self.es_doc_type, id=id, body=dict({'doc': {'is_crawl': 1}}))

            page = page + 1

    def parse(self, response):

        _id = response.meta["_id"]
        fb_like_count = response.meta["fb_like_count"]
        fb_share_count = response.meta["fb_share_count"]
        child_count = response.meta["child_count"]

        try:
            info_request = json.loads(response.body_as_unicode())

            # get share count
            try:
                share_count = int(info_request["shares"]["count"])
            except Exception:
                share_count = fb_share_count

            try:
                like_count = int(info_request["likes"]["count"])
            except Exception:
                like_count = fb_like_count

            try:
                comment_count = int(info_request["comments"]["count"])
            except Exception:
                comment_count = child_count

            if like_count > 0 or share_count > 0 or comment_count > 0:

                # print "==================="
                # print fb_id
                # print source["message"]
                # print "==================="

                action = {
                    "_index": self.es_index,
                    '_op_type': 'update',
                    "_type": self.es_doc_type,
                    "_id": _id,
                    "doc": {
                        "fb_like_count": like_count,
                        "fb_share_count": share_count,
                        "child_count": comment_count
                    }
                }

                self.actions.append(action)
        except Exception,f:
            print f

    def spider_closed(self, spider, reason):
        print "=================== UPDATE ======================"
        print self.actions
        helpers.bulk(self.es, self.actions)
        print "END"