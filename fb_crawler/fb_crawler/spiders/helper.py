import os, time

class Helper:

    def getconfigFacebookApp61(self):
        configFacebookApp = [
            {
                'app_id': "858722547570474",
                'app_secret': "dff648fa4a7cc4a1258e3e5af3a67a82"
            },
            {
                'app_id': "1209232925760686",
                'app_secret': "a5917ba5faccf17175df9eb2bf49e36e"
            },
            {
                'app_id': "1656144747965034",
                'app_secret': "afe3d532075b0069a42f50aeef1e1e56"
            },
            {
                'app_id': "742856202485652",
                'app_secret': "2841986f00087cd3984975da026c3242"
            },
            {
                'app_id': "167656850248856",
                'app_secret': "2a60acd57a0606e3c1dde262ea8b6591"
            },
            {
                'app_id': "319941248200357",
                'app_secret': "bc9a6122017e17ac1f5d003c6e2d0967"
            },
            {
                'app_id': "543710442443158",
                'app_secret': "ec9b48583de2e57d00aff75c1c9b152f"
            },
            {
                'app_id': "490120754495952",
                'app_secret': "ef3c133a4de569c19a9ced3ce77b284e"
            },
            {
                'app_id': "1061678047205880",
                'app_secret': "153f1efd4ece78857d2c7f932c1d1a7a"
            },
            {"app_id": "671557189689574", "app_secret": "caa142da61ff2951642de0664f204e22"},
            {"app_id": "870399153097237", "app_secret": "032b5d31d7d2784b32b661a5b6c4bea0"},
            {"app_id": "223106811480266", "app_secret": "16da2f195c09422915edc68d537656b8"},
            {"app_id": "390967977907715", "app_secret": "928c3324fc9ab602542cd586146989c2"}
        ]
        return configFacebookApp

    def getconfigFacebookApp62(self):
        configFacebookApp = [
            {"app_id": "132321137428521", "app_secret": "de1f8684ed97d28022195592c60a3b13"},
            {"app_id": "1956720111244611", "app_secret": "e06a7cca0835a6ae3aae10821e786d6a"},
            {"app_id": "1763057453782426", "app_secret": "6e09afa07456fcee7c1c1d1b3a7d0cd0"},
            {"app_id": "1944895662416996", "app_secret": "4709efc109640aa4962056201934486d"},
            {"app_id": "130300334272714", "app_secret": "c0660c0ca03b9b6c86a18785c147ad68"},
            {"app_id": "1815240535216554", "app_secret": "cef14013baecb154374258bac1018423"},
            {"app_id": "1707324599286947", "app_secret": "e2573a8f3916ff8782556c4909148007"},
            {"app_id": "508653266179703", "app_secret": "0b1a88c3ed6a5ff0f223bccbf2bb5538"},
            {"app_id": "1867292266631731", "app_secret": "8db6ae7e3f1662d6d06f883cf263d59e"},
            {"app_id": "1984681351809726", "app_secret": "6ead19b2d1712ab81bbb12b897156028"},
            {"app_id": "323342221465461", "app_secret": "e36895d78e34d944ec8012a441cee67b"},
            {"app_id": "294356674414432", "app_secret": "e4b5e9ac12e501a38fa0b5d913f0784d"},
            {
                'app_id': "1659712467625893",
                'app_secret': "0733bc4fcd62f575cbbafe3299dfa6b2"
            },
            {
                'app_id': "1506370196330419",
                'app_secret': "1704e81dd2847ae46ac2d79d22ec1100"
            },
            {
                'app_id': "1675968429348160",
                'app_secret': "f706661245963bc72d94a8b94c98020a"
            },
            {
                'app_id': "939528896136317",
                'app_secret': "93d17063f978c96fd9e6699e4c6319b4"
            },
            {
                'app_id': "1665916770351872",
                'app_secret': "946718f09aed78d2fd463fbc8a9bf684"
            },
            {
                'app_id': "1222136101133846",
                'app_secret': "a6f0efdbe7fa0973240690d7445c8ae3"
            },
            {
                'app_id': "1664134473868497",
                'app_secret': "811415871ed1a5817bb2849b45aa26c4"
            }
        ]
        return configFacebookApp

    def getconfigFacebookApp63(self):
        configFacebookApp = [
            {"app_id": "1403357266630551", "app_secret": "5d84d8a96fa1588a1446804072bd7f5e"},
            {"app_id": "1733678530207064", "app_secret": "2e9da94aa2ee356ffc6bf4301873ea62"},
            {"app_id": "1024916607599669", "app_secret": "d001b54ef7bdd4d6cfa51ef0da1ce5ae"},
            {"app_id": "1722100321380232", "app_secret": "4141cc739fc64829661ac4cfcc7db3d7"},
            {"app_id": "584689971702312", "app_secret": "089607f8b0c126b6482a0535ce5907c9"}
        ]
        return configFacebookApp

    def getconfigFacebookApp64(self):
        configFacebookApp = [
            {"app_id": "123263831702238", "app_secret": "1472bb41ba6ab13e6d573190690c9c2b"},
            {"app_id": "291245338030997", "app_secret": "a19c797dacc0f272e607915ede404a3f"},
            {"app_id": "1776703859295976", "app_secret": "0e75e3caf69f2507e6a7623e3e4fc675"},
            {"app_id": "1618959898156447", "app_secret": "b233f3dbdde20c4d433d239ca379310a"},
            {"app_id": "1550930231642075", "app_secret": "3a5bbccecef916d861977277c45ed96a"},
        ]
        return configFacebookApp

    def getconfigFacebookApp65(self):
        configFacebookApp = [
            {'app_id': "168165356880990",'app_secret': "3f957e23fb42c068d6a04ef3ebdf44b8"},
            {"app_id": "1786175391679965", "app_secret": "74c1e7daa2fa788875acd00e26626967"},
            {"app_id": "937036699767772", "app_secret": "7df7d3a34326889abba4e16f6497d012"},
            {"app_id": "330048674133959", "app_secret": "94cd9b880644d8d5b60367f8789ca0d9"},
            {"app_id": "189286014953678", "app_secret": "6dc9ad21614d6e179183a744783a97c1"},
        ]
        return configFacebookApp

    def getconfigFacebookApp66(self):
        configFacebookApp = [
            {"app_id": "291821054657876", "app_secret": "395e4d621d619fc096782c7db6a4ebad"},
            {"app_id": "125439928125985", "app_secret": "328a5a54988125bd2fd30db703fe9a5e"},
            {'app_id': "1231964560150824",'app_secret': "cd0486f6a4b16aa83fe02a085d470c17"},
            {'app_id': "1663842863895417",'app_secret': "bd95ff8dddf17c548343dd189634f929"},
            {'app_id': "476821112502501",'app_secret': "3d248a63913c119f2a08d4651a056181"}
        ]
        return configFacebookApp

    def getconfigFacebookApp67(self):
        configFacebookApp = [
            {"app_id": "161555891124556", "app_secret": "d49149e8a6ffe02eb634f98445d15642"},
            {"app_id": "134419220651939", "app_secret": "9911bfe5adde92b517133831db0a395e"},
            {"app_id": "1551929425109780","app_secret": "399e0c7ce43b6c039b75350bd91a4019"},
            {"app_id": "1022710947782514","app_secret": "758acd6b83da038388e24dec8fc70038"},
            {"app_id": "1723228954633168","app_secret": "9003b172a9e200a190bbd05922b258ec"},
            {"app_id": "1752590831694666","app_secret": "f2b8cd570ea2782d437b86fd1dcb0a65"},
            {"app_id": "484060041787568","app_secret": "54368a46e78ad8c014bf2d1668b61931"},
            {"app_id": "494985450706074","app_secret": "d11c233ecf682ad348c132c60d6ebf1b"},
            {"app_id": "800651990070496","app_secret": "4d5f0efd2d3065689010cad524f9e770"},
            {"app_id": "563807893801312","app_secret": "3e6a248f45881576c8a956abf1704e39"}
        ]
        return configFacebookApp

    def getconfigFacebookApp68(self):
        configFacebookApp = [
            {"app_id": "134721936936563","app_secret": "fb5026438768279091b2ef6fe75dc22e"},
            {"app_id": "611007042395008","app_secret": "fa5931ddb3945f1e1519b47bb0398056"},
            {"app_id": "1731535260432815","app_secret": "c1c12f84c6810d14c67636145e744187"},
            {"app_id": "621790894637171","app_secret": "5c58e5354f2cede090a11cd9458da1de"},
            {"app_id": "1044758278942586","app_secret": "9d01fa5ccb116b6cdc9c5a86e1399e66"}
        ]
        return configFacebookApp

    def getconfigFacebookApp69(self):
        configFacebookApp = [
            {"app_id": "1579909472339076","app_secret": "786912d6c874d4689547b66bda86f490"},
            {"app_id": "722684917767964","app_secret": "2fa5ac6ac0f6fcce5c5ee8706c18eeec"},
            {"app_id": "1618775668413557","app_secret": "e42ebd7f3e2851912cd040c12165220a"},
            {"app_id": "1026988307391223","app_secret": "0a8fbc6158fb8263a21701f7767e26a7"},
            {"app_id": "1184396538250662","app_secret": "b098c8b3b02401dbc959a5aef16c2a66"}
        ]
        return configFacebookApp

    def getconfigFacebookApp70(self):
        configFacebookApp = [
            {"app_id": "1086102954782421","app_secret": "b63b20579c630f1c643a27fab6f43b00"},
            {"app_id": "129332627483061","app_secret": "dc705dd8e769487564936c4e9ddab39f"},
            {"app_id": "1077123579010932","app_secret": "1a958980aaffeb8fea43a5b7c709adf7"},
            {"app_id": "1628931130762723","app_secret": "e17bb60cea65f6f7e5ea85b6771ae989"},
            {"app_id": "497679370428773","app_secret": "d2992f1c46ecfa7e3c2c8dc35b1b646b"}
        ]
        return configFacebookApp

    def getconfigFacebookApp71(self):
        configFacebookApp = [
            {"app_id": "1690183214590249","app_secret": "a6ba19e29af4f7044e67df6d5e9c20f4"},
            {"app_id": "1025101860877448","app_secret": "091c6d40cb5f8a3763bfbe2759a69fad"},
            {"app_id": "246212252408654","app_secret": "a3513a47098218d0e1c1d70ab8956e1b"},
            {"app_id": "290839677923057","app_secret": "03258f9904921e0ec965340c9d78d3a4"},
            {"app_id": "1730237173911344","app_secret": "25729a0bdb856c1b8b9c6051f1e144ef"}
        ]
        return configFacebookApp

    def getconfigFacebookApp72(self):
        configFacebookApp = [
            {"app_id": "1230700627009436","app_secret": "2dd7002a407be880cad6e51078db6f9d"},
            {"app_id": "1243057349066226","app_secret": "19cfea80cb9e4569ead3cc021be50876"},
            {"app_id": "1848097652098933","app_secret": "866316e513c9adfb1f7534edc0bd5a16"},
            {"app_id": "390018608002034","app_secret": "8b97edd63e4db299735c7aad465f1925"},
            {"app_id": "129365160894050","app_secret": "b995469b5498a783a889562b3d8629cb"},
            {"app_id": "1613271222333714","app_secret": "a07119610ffcaa5c23592436abdef6de"},
            {"app_id": "141794412901432","app_secret": "10f539f75d5b0065fed4ab42fef4662c"},
            {"app_id": "223648294687445","app_secret": "6b2615f60b952d85ed8e763c7c3bfb1d"},
            {"app_id": "240418226335900","app_secret": "dfc0e7285546b631157b4ea3b65cc1b0"},
            {"app_id": "1755807731370120","app_secret": "e8fcb7c55f64ac949864d200d3d21bfd"}
        ]
        return configFacebookApp

    def getconfigFacebookApp73(self):
        configFacebookApp = [
            {"app_id": "559176984259455","app_secret": "81edae3764374de4a9b03e6d5648f654"},
            {"app_id": "244011225961593","app_secret": "d0d8586367bab8e0b58a63738573479e"},
            {"app_id": "259372431083675","app_secret": "0447ae1a824d1f4fe1a2ed1a368a2d90"},
            {"app_id": "591019937745599","app_secret": "3763b9615a769e89ce97ca06a3f46777"},
            {"app_id": "1745207985754940","app_secret": "1b4308cc895694d1ae8237ebaf8cc9fd"}
        ]
        return configFacebookApp

    def getconfigFacebookApp74(self):
        configFacebookApp = [
            {"app_id": "1328236547205174","app_secret": "3a6afaa744ad68494d680580b34c562a"},
            {"app_id": "1611360035820891","app_secret": "944a9a5db34075595ad87e42b7a9582c"},
            {"app_id": "806371922830845","app_secret": "abfd7ae84e7cfc3c90e42095974a4c08"},
            {"app_id": "646777485475150","app_secret": "db76d4beaea4a84f2ea37d474b108742"},
            {"app_id": "828374120629689","app_secret": "379b5f12e0db2c4f54252e29098e9f76"}
        ]
        return configFacebookApp

    def getconfigFacebookApp75(self):
        configFacebookApp = [
            {"app_id": "124782761550557", "app_secret": "3f156b07d92d54e9d5c29085e5dfc863"},
            {"app_id": "299165630601236", "app_secret": "f327b7feb3ae8fb6a8dfb839396452d4"},
            {"app_id": "798756720306297", "app_secret": "b7d7de3f575c256d93e99fddd2363732"},
            {"app_id": "1752388961729038", "app_secret": "76bbe6837f0783f426afeb772e0edb99"},
            {"app_id": "499568407092049", "app_secret": "47d0c101f1c55abc865a1cac9481490f"},
            {"app_id": "1306199396098037","app_secret": "4a1ce69857155963a9b000a7661bc23d"},
            {"app_id": "1323465770998848","app_secret": "b0bb8bb70fe8fe5b2894db6e6f185af8"},
            {"app_id": "1333293153368838","app_secret": "d3ddc6b279098cbc654b4703bf76cc68"},
            {"app_id": "221669721579727","app_secret": "f561ceff00d0feec1e7229c16a133854"},
            {"app_id": "2136461693246126","app_secret": "d9d9c309c66004f7b2f4635ffaf93d8c"},
            {"app_id": "1690882187853118","app_secret": "282fa7d678c5b32d23252e829ef9c01e"},
            {"app_id": "235745040135593","app_secret": "5df456c08ec5152b82291ae7f1ee2df6"},
            {"app_id": "263825803970641","app_secret": "a2fa8251d32400d2170be98afe0318c5"},
            {"app_id": "1755334594681247","app_secret": "65aef36234f6caf90d3846809bdb93de"},
            {"app_id": "1742262906058047","app_secret": "c82305470f422e40c0b895618b47bfca"}
        ]
        return configFacebookApp

    def getconfigFacebookApp76(self):
        configFacebookApp = [
            {"app_id": "760952974100880", "app_secret": "df7f592965e8168a9db1b6e0b352cfe4"},
            {"app_id": "550195098652423", "app_secret": "d6f8b63841e5795e269b279695a889e6"},
            {"app_id": "370914716649461", "app_secret": "00324b3287d0dfce8d8dfa6aa0507a78"},
            {"app_id": "190690461492222", "app_secret": "2a401b94b90551c261b38ad427b1d6e8"},
            {"app_id": "2002735559968219", "app_secret": "111757d73132a3715efa6ceb175fe99d"},
            {"app_id": "1788656481205927", "app_secret": "af8b93354b99cf28a409f5a740237c28"},
            {"app_id": "142654946488223", "app_secret": "d5e69ff68e0cd3c68fbb8a23b1522876"},
            {"app_id": "2199249780309328", "app_secret": "6169b4e4c7a9bffda66fc1fa0902e16f"},
            {"app_id": "2000332733547708", "app_secret": "76ca5b3f81553fd869ec566badff309a"},
        ]
        return configFacebookApp

    def getconfigFacebookApp77(self):
        configFacebookApp = [
            {"app_id": "1952066191722713", "app_secret": "b754dcdd472e9dab15bd66019dfb9a0f"},
            {"app_id": "1931160617112661","app_secret": "0e302aa5a68da3ee45dc995e9fcde0a7"},
            {"app_id": "1708317646149664","app_secret": "0736df08e6debe32129c29264515445f"},
            {"app_id": "1617419991900328","app_secret": "0b9b8a0a0432a02cdcfbbeb803d0b661"},
            {"app_id": "411050385893809","app_secret": "939a76829c7c5159951348569901e7a9"},
            {"app_id": "140855196405541","app_secret": "e3a77ba72f518ddc77d4f8419d5427af"},
            {"app_id": "1787512911465770","app_secret": "0990b1324c2002bd700cba48177b2c8d"},
            {"app_id": "581705435371969","app_secret": "26744f6702fa93cad18114570ff3aab4"},
            {"app_id": "913316385470089","app_secret": "ce7c487fe0f78d7484f414642888b7a4"}
        ]
        return configFacebookApp

    def getconfigFacebookApp78(self):
        configFacebookApp = [
            {"app_id": "740391266113253","app_secret": "212646c3d94f087346262c733d23a820"},
            {"app_id": "230400290698981","app_secret": "39ee85b2cdbccafc24d6f269feffcc7c"},
            {"app_id": "1185552074856034","app_secret": "125866807afd8305e70903126ddd66bb"},
            {"app_id": "1662372224061470","app_secret": "bda6fcc490de4ea9e783965e62cf1e4b"},
            {"app_id": "1188088874561728","app_secret": "782db441032c707ee97e79e3848b0904"},
            {"app_id": "388891821448342","app_secret": "f3ae3eb44d47ec1baedb14b90bad4d85"},
            {"app_id": "359856681043505","app_secret": "a42c550d72f5345be18e0d26fe70a782"},
            {"app_id": "188929531573615","app_secret": "5e13375bf84f829715ea75a8c7836c67"},
            {"app_id": "353042655088541","app_secret": "068ece176ac10744f596a682346c919f"}
        ]
        return configFacebookApp

    def getconfigFacebookApp79(self):
        configFacebookApp = [
            {"app_id": "166518810601274", "app_secret": "9db12a56c074b7e7e1e16a023f3488a1"},
            {"app_id": "218137722059236", "app_secret": "4b30d38b3410bd3c367c8380ffec20ca"},
            {"app_id": "152358065379900", "app_secret": "ebf2e958466f55b9792ffb1dacd952e4"},
            {"app_id": "318560048622450", "app_secret": "811451e85a67181b87e7efe92a3a9b16"},
            {"app_id": "1608566805830702", "app_secret": "438b34a7dfc55f7c158c17a7f59e5dcd"},
            {"app_id": "1651429411586939", "app_secret": "74e0a5f5b5cb6ae1b5bed1b2e986b382"},
            {"app_id": "504794046564111", "app_secret": "2b732bd5e687d9ff54e03a0f2380268f"},
            {"app_id": "356260618149867", "app_secret": "bf18abff9e95caf617822c0ff922b4d0"},
            {"app_id": "1928793660775960", "app_secret": "f3eb91cf43228fa08f8a3ad27ba9a2c3"},
        ]
        return configFacebookApp

    def getconfigFacebookApp80(self):
        configFacebookApp = [
            {"app_id": "137041513617866", "app_secret": "75aed6656cbb8fe4464473152ccd15ef"},
            {"app_id": "135951670394395", "app_secret": "9663999f7e82e0911f930f49a07da59c"},
            {"app_id": "142341813077091", "app_secret": "6179cd6548a895fd2856e5f4075e046b"},
            {"app_id": "126698174766170", "app_secret": "f54f568fd7ce50fe452c7681869b50eb"},
            {"app_id": "1366518963470817", "app_secret": "b339b2d5fd429f6630ff0cdcbfc7191d"},
            {"app_id": "540478322972220", "app_secret": "9407983617fc953993263d125b069a20"},
            {"app_id": "1277187565719766", "app_secret": "af805bbdbcf681418f1cd88127eccb12"},
            {"app_id": "289363081550557", "app_secret": "fa79fe0560db022405fe910d343a8c6f"},
            {"app_id": "128222244557531", "app_secret": "c31177f0b260f91ee3d64cff4287c2f0"},
            {"app_id": "347187365747403", "app_secret": "0af428851907bec65edfb118264c8c2e"},
        ]
        return configFacebookApp

    def getconfigFacebookApp81(self):
        configFacebookApp = [
            {"app_id": "484122378636734", "app_secret": "ced67584f3026045b39603a219300248"},
            {"app_id": "149261345821214", "app_secret": "a08461f9d734dcb7025e6825d17e9109"},
            {"app_id": "1540195379369232", "app_secret": "0d1a8374fa0e2bc74801958c70dffdc4"},
            {"app_id": "520698208294731", "app_secret": "bf49dbd610c7546a1d659ce564718c75"},
            {"app_id": "470672199999656", "app_secret": "28090d539140c4989385d394a5072c7f"},
            {"app_id": "777993822388168", "app_secret": "761445690f07a26f235abc4621c6b98a"},
            {"app_id": "1523400564442453", "app_secret": "cd483c4dc659e8ade30dec68f52d49dc"},
            {"app_id": "195229974380588", "app_secret": "a4efa3ff39a46d7912ddf5524fe460b1"},
            {"app_id": "2035438310009588", "app_secret": "7a42bd4a6fcff66427147ca75b02fb2e"},
        ]
        return configFacebookApp

    def getconfigFacebookApp82(self):
        configFacebookApp = [
            {"app_id": "183777792179571", "app_secret": "25d0a0dcdd2928d57bdc8515b21ffe5a"},
            {"app_id": "634609646929528", "app_secret": "709cfb36de375027a0b7728195364fa1"},
            {"app_id": "407887469628440", "app_secret": "a6e59b0b981e3be0d861bc7e78aeca12"},
            {"app_id": "1177911459006867", "app_secret": "167e4dd79abb1cb5c77958260f5f53bb"},
            {"app_id": "1960847254155971", "app_secret": "1579ad7d658e6cbfd37620385e833353"},
            {"app_id": "125470478145260", "app_secret": "ab37e18b88c831c5ab5f43cddbfdc990"},
            {"app_id": "467895066944060", "app_secret": "b09e36c696d656bf1e5720dc19491e77"},
            {"app_id": "537053396639453", "app_secret": "e6842cfc4a1cddc643e8cebfa5891a56"},
            {"app_id": "1657887450941721", "app_secret": "0d6ea03411370fa413d175624761ea3b"},
        ]
        return configFacebookApp

    def getconfigFacebookApp83(self):
        configFacebookApp = [
            {"app_id": "183777792179571", "app_secret": "25d0a0dcdd2928d57bdc8515b21ffe5a"},
            {"app_id": "634609646929528", "app_secret": "709cfb36de375027a0b7728195364fa1"},
            {"app_id": "407887469628440", "app_secret": "a6e59b0b981e3be0d861bc7e78aeca12"},
            {"app_id": "1177911459006867", "app_secret": "167e4dd79abb1cb5c77958260f5f53bb"},
            {"app_id": "1960847254155971", "app_secret": "1579ad7d658e6cbfd37620385e833353"},
            {"app_id": "125470478145260", "app_secret": "ab37e18b88c831c5ab5f43cddbfdc990"},
            {"app_id": "467895066944060", "app_secret": "b09e36c696d656bf1e5720dc19491e77"},
            {"app_id": "537053396639453", "app_secret": "e6842cfc4a1cddc643e8cebfa5891a56"},
            {"app_id": "1657887450941721", "app_secret": "0d6ea03411370fa413d175624761ea3b"},
        ]
        return configFacebookApp

    def getconfigFacebookApp1(self):
        configFacebookApp = [
            {
                'app_id': "1243590545656660",
                'app_secret': "7c2b6cf0f41487d64a95ef24692e5c35"
            },
            {
                'app_id': "1698462017051139",
                'app_secret': "46355ebf76a3125218132cb7a604d240"
            },
            {
                'app_id': "1511042029194584",
                'app_secret': "4e982ffc986863f3b3c97f9af0f03a34"
            },
            {
                'app_id': "1498067920494476",
                'app_secret': "afdde6ddb95b2ae16a4a34901a4d612c"
            },
            {
                'app_id': "1638243186450724",
                'app_secret': "7486a7eb4c798ba6fa949d051b75b77a"
            },
            {
                'app_id': "734041893392526",
                'app_secret': "a8f360ef01d98d753a5ec2e8c837a4c3"
            },
            {
                'app_id': "756830331112513",
                'app_secret': "84fa6c30ab1f7d0034e062dc94bace56"
            },
            {
                'app_id': "797478660397357",
                'app_secret': "6e9f9abd91008b20d0402fcac825203e"
            },
            {
                'app_id': "561522333996030",
                'app_secret': "dcd1834a51f5776a9a3e47501323627d"
            },
            {
                'app_id': "1038179686203215",
                'app_secret': "1a5e5e9c4c2d7c4c715a34117f69721f"
            }
        ]
        return configFacebookApp

    def getconfigFacebookApp2(self):
        configFacebookApp = [
            {"app_id": "181506535579429", "app_secret": "df0187ff757cfe38376a68c2479f1ebd"},
            {"app_id": "1056910417702737", "app_secret": "11c4864f3a4e0799f7be06b4008141f5"},
            {"app_id": "274535276228580", "app_secret": "fd5bac43c551c7e1164c7a7a5779abae"},
            {"app_id": "558966224275848", "app_secret": "6aa7fe4cfcb9b54cc8b3484e67bd2bf7"},
            {"app_id": "1790525727848557", "app_secret": "9c5e289b61e7baf8f703a3fc97d196d8"}
        ]
        return configFacebookApp

    def getconfigFacebookApp3(self):
        configFacebookApp = [
            {"app_id": "235591540150624", "app_secret": "70f344c389000ea37747782ab6fe38c0"},
            {"app_id": "1621412671512171", "app_secret": "47f9bdfb3ce7997db0be2c67aead25c2"},
            {"app_id": "1186949368006614", "app_secret": "df055b516340f8f7b4824069e5a53d6c"},
            {"app_id": "910362909073181", "app_secret": "7fa82a653b796f30a4d7c35bf64efe18"},
            {"app_id": "878036338989072", "app_secret": "374a7c04165bb5d23350e085a38043d4"}
        ]
        return configFacebookApp

    def getconfigFacebookApp4(self):
        configFacebookApp = [
            {"app_id": "769654869838760", "app_secret": "3663ff4bb0010ed27a2f844e4eab42ce"},
            {"app_id": "178267215907496", "app_secret": "17d3accf76afb27444d5b9fce1d83c62"},
            {"app_id": "1375359402480948", "app_secret": "176e51fe95ad5e8f7c27bea99e459992"},
            {"app_id": "236908466686241", "app_secret": "3b199ba138a6a802c7b8e1f674c5afc5"},
            {"app_id": "454158294792339", "app_secret": "df59a0df13d19b9abd34bc81703afd42"}
        ]
        return configFacebookApp

    def getconfigFacebookApp5(self):
        configFacebookApp = [
            {"app_id": "533861193452724", "app_secret": "3d231a2df778fca7c5145a8822a1110b"},
            {"app_id": "1103225259753073", "app_secret": "165b80c13856d4de775d1cb9ea62a0fd"},
            {"app_id": "867532586707911", "app_secret": "cf75d9fdaabc009be06c6c8f8be5cdd4"},
            {"app_id": "274473866234713", "app_secret": "89a81dfc0404a8fe3cee7963758b1a21"},
            {"app_id": "1594928717486750", "app_secret": "c3096cc73638897112a6eaa92f2603fe"}
        ]
        return configFacebookApp

    def getconfigFacebookApp6(self):
        configFacebookApp = [
            {"app_id": "1099563673436704", "app_secret": "a35a21447bdb7de7200f00b7e65240ba"},
            {"app_id": "1194374920581517", "app_secret": "e930ca12440c543b594b7a3e4c6dbdef"},
            {"app_id": "579141072256779", "app_secret": "fba1ea5532ad6e869a8dd142cf56bf37"},
            {"app_id": "1388977704461965", "app_secret": "60561d29df9816cab0fe086d0538ea73"},
            {"app_id": "1576936189266258", "app_secret": "e67b6ac587b27855803731a5fc4d5ba1"}
        ]
        return configFacebookApp

    def getconfigFacebookApp7(self):
        configFacebookApp = [
            {"app_id": "519841848199863", "app_secret": "5696c01ece88bada75f853e2938ac90b"},
            {"app_id": "133601197050340", "app_secret": "9915b5afea64ef187d1fbc841731e6ea"},
            {"app_id": "192595411138983", "app_secret": "d53328cef317b488fcd78325c8e67e73"},
            {"app_id": "1006653916054856", "app_secret": "9ddbf173d4bb0f17b85b63a0a8308452"},
            {"app_id": "1696706243927335", "app_secret": "7bbe94153de6d9bb1741a961d8d842bf"}
        ]
        return configFacebookApp

    def getconfigFacebookApp8(self):
        configFacebookApp = [
            {"app_id": "963971783680934", "app_secret": "5453b1d245d4d3185598541b828ce948"},
            {"app_id": "247798595578809", "app_secret": "448d09c7c9a9ce4581f489596da6d3a3"},
            {"app_id": "864685496994211", "app_secret": "f8816f378ce667084af9953623bfdee9"},
            {"app_id": "1162528520465849", "app_secret": "e96deb89761671776a302ebbe2223488"},
            {"app_id": "1048008745287014", "app_secret": "3722862f6368e9cc222a3693a55887a2"}
        ]
        return configFacebookApp

    def getconfigFacebookApp9(self):
        configFacebookApp = [
            {"app_id": "1551662551802735", "app_secret": "4cb75e1e3dab206137242bf11bc3743f"},
            {"app_id": "1149797231739691", "app_secret": "3b6920540ecac72bffea4a5c75a9f7fd"},
            {"app_id": "1208254079206215", "app_secret": "c1ccb16724f7dc981f3120f7b13c103d"},
            {"app_id": "1752591154976386", "app_secret": "69d791d330938cd8b99f8d75c394832b"},
            {"app_id": "134337706978338", "app_secret": "f4265b7b61487d14855711989cdae03e"}
        ]
        return configFacebookApp

    def getconfigFacebookApp10(self):
        configFacebookApp = [
            {"app_id": "975516629235742", "app_secret": "72dd9a510aa7418e01b445478fcdfe6f"},
            {"app_id": "819203664891388", "app_secret": "599dde53fc814741ac000643aa501004"},
            {"app_id": "1595420480769390", "app_secret": "0e509176a65c233d9c1f01a6e39038ff"},
            {"app_id": "954065804691221", "app_secret": "a1b36b6bb4386f366fa0149a2144c674"},
            {"app_id": "1595420480769390", "app_secret": "0e509176a65c233d9c1f01a6e39038ff"}
        ]
        return configFacebookApp

    def getconfigFacebookApp11(self):
        configFacebookApp = [
            {"app_id": "1616146212037096", "app_secret": "14bec427ceca39aa87176fb557e61cf6"},
            {"app_id": "548445098668983", "app_secret": "fbbd5c8b85aab00b97d8346eca6e6179"},
            {"app_id": "1748794418674966", "app_secret": "881f4df946d1ae79a4f8d08c8b709227"},
            {"app_id": "1763460887207689", "app_secret": "8ca7efdead3a4f0fc0351b1a38a46db2"},
            {"app_id": "1023817747710055", "app_secret": "760f5e20a287e165cc5f634a5276af3c"}
        ]
        return configFacebookApp

    def getconfigFacebookApp12(self):
        configFacebookApp = [
            {"app_id": "1578356525796828", "app_secret": "226f2415dea927d2ffe1e910a595f2b4"},
            {"app_id": "1730467780540062", "app_secret": "86747ce562330bb6a2e2e820276b7e75"},
            {"app_id": "904744202970066", "app_secret": "c513a8bcb04892dc80023da7d6f1eabd"},
            {"app_id": "169854886750168", "app_secret": "961ef3c3ec9b4b2224a113187be75f24"},
            {"app_id": "241890536186205", "app_secret": "4a5b06c31ebeecaca94bd6985ac89225"}
        ]
        return configFacebookApp

    def getconfigFacebookApp13(self):
        configFacebookApp = [
            {"app_id": "498517410355335", "app_secret": "544099a1529ce9036487abc4a2c09746"},
            {"app_id": "700086553464428", "app_secret": "242f72ec18db3e18c22aa7c9787f9766"},
            {"app_id": "909230739175672", "app_secret": "bbcdc2cd5c9e67b1eae8fdeff41ee239"},
            {"app_id": "626501610832663", "app_secret": "892c6feaff2a25faa4077ce14ef27f09"},
            {"app_id": "1728894704040612", "app_secret": "e6bed538fc6028677e5511c520b70d85"}
        ]
        return configFacebookApp

    def getconfigFacebookApp14(self):
        configFacebookApp = [
            {"app_id": "1722666984642401", "app_secret": "ab79b2763e89d5383c0d27854fd209b7"},
            {"app_id": "261799410841625", "app_secret": "b68055aba9243050f3b483ff61a8a5b3"},
            {"app_id": "1030630207028231", "app_secret": "7411ba13d049353624a941a9b61d81ec"},
            {"app_id": "618098271682562", "app_secret": "3689570d3a03d18ba4fb59de9bf86ac3"},
            {"app_id": "1172887486076051", "app_secret": "7a2c115c8df5cdbb005ac54d0207657a"}
        ]
        return configFacebookApp

    def getconfigFacebookApp15(self):
        configFacebookApp = [
            {"app_id": "1852869231607000", "app_secret": "722731d6aee5b9b988b86348037b740a"},
            {"app_id": "230158517366696", "app_secret": "d3cf88f74063c8b0392ab675bed4af73"},
            {"app_id": "229077610806707", "app_secret": "790b19e856e5e66b01e04886b71100e6"},
            {"app_id": "239789376398535", "app_secret": "1a9fcc130ab4d55ef0fd0cfe83e95246"},
            {"app_id": "675915729224538", "app_secret": "a95b061b1357f1ca20781999ae717508"}
        ]
        return configFacebookApp

    def getconfigFacebookApp16(self):
        configFacebookApp = [
            {"app_id": "184803541919413", "app_secret": "b021f1c1637b435feb324959718e52fc"},
            {"app_id": "1050598371691933", "app_secret": "e79db8e1eb7191388a797f71b44d337e"},
            {"app_id": "268239133525016", "app_secret": "461e747f0d6c9ed2293e5a03b5b36667"},
            {"app_id": "1735809479994326", "app_secret": "052ea6fb498f7b6b1db0418309372b82"},
            {"app_id": "275682329486155", "app_secret": "f002860972b279e3243508d3dd916904"}
        ]
        return configFacebookApp

    def getconfigFacebookApp17(self):
        configFacebookApp = [
            {"app_id": "1688565724736686", "app_secret": "b24f0309e6656363bb649e867f734b22"},
            {"app_id": "130755724006314", "app_secret": "8b8f49a2f2e5ba5f70a10fd5c0fd5f6c"},
            {"app_id": "1027740837295044", "app_secret": "909b9930722772d0e932cb4b0f00bce9"},
            {"app_id": "458121507717802", "app_secret": "d3956359b9e0756e43e3b9bf72311c56"},
            {"app_id": "1757746337802412", "app_secret": "9e9ae66db88ee917cd1d6d5182f97d9d"}
        ]
        return configFacebookApp

    def getconfigFacebookApp18(self):
        configFacebookApp = [
            {"app_id": "245120669184432", "app_secret": "beb569ba0221042cc58ccb4431fdd715"},
            {"app_id": "231827143865014", "app_secret": "dbd0a870e71a767c6b64b2eea2b89659"},
            {"app_id": "1736366169937042", "app_secret": "edb4a25d1ecd638c6eba7b18b94b06a3"},
            {"app_id": "1723460244601650", "app_secret": "098574e659baf4a282c41352897eaa82"},
            {"app_id": "1071258176268138", "app_secret": "b3eaaa1bbbeb3c24978a7e506f1b2fe5"}
        ]
        return configFacebookApp

    def getconfigFacebookApp19(self):
        configFacebookApp = [
            {"app_id": "238730176498402", "app_secret": "09155aa44b76fe400f8db547245d81d9"},
            {"app_id": "1057158161040370", "app_secret": "70eb4b14afa1582c4f6d8c4b73e402bb"},
            {"app_id": "509610742576781", "app_secret": "3b98d4ee9fdf1b9de631877c95920320"},
            {"app_id": "253608518337589", "app_secret": "ab6ce6ad7d47349a94c6f0283a4323e5"},
            {"app_id": "737000183106997", "app_secret": "6648a1712836f3dc6bed93196155a78d"}
        ]
        return configFacebookApp

    def getconfigFacebookApp20(self):
        configFacebookApp = [
            {"app_id": "1160417550644235", "app_secret": "81ca838d619a3e48c3f4d1089f8ff319"},
            {"app_id": "1183577631654451", "app_secret": "277cec6924ccf08be54a8deafd03dc40"},
            {"app_id": "969049679878229", "app_secret": "ee009c5b9e1e96ca27b39e005d6baa85"},
            {"app_id": "738029053004889", "app_secret": "b57c321154648207b74a002b79404713"},
            {"app_id": "243434706028095", "app_secret": "5c2279d8e6a4b451d0250e6b6fbfac85"}
        ]
        return configFacebookApp

    def getconfigFacebookApp21(self):
        configFacebookApp = [
            {"app_id": "1107077319356355", "app_secret": "1c8d0c0a327a6bb4c50e7267358760f7"},
            {"app_id": "227615357619995", "app_secret": "1834f7a65069aa7930434a78bd05df3a"},
            {"app_id": "1536862759953500", "app_secret": "186017cb7ae3227f14cf2e215f2b2d5a"},
            {"app_id": "705357999604472", "app_secret": "5116ebb32dc5d50ac0dd34b1cb4db3db"},
            {"app_id": "1609246389365337", "app_secret": "0b2e3fedb53549fbc3c4824d44f22a54"}
        ]
        return configFacebookApp

    def getconfigFacebookApp22(self):
        configFacebookApp = [
            {"app_id": "131934127210633", "app_secret": "32ad307e0ed2238c5135d217264a5aa1"},
            {"app_id": "479385495590514", "app_secret": "474a48b307cf9569e23923de39633055"},
            {"app_id": "134811316932931", "app_secret": "c67eee14a413d41b7e29a18e00ec9ae0"},
            {"app_id": "274473549571383", "app_secret": "fbc11c03656729b2cb06283081fcb01e"},
            {"app_id": "421754194687304", "app_secret": "8b07a776f604b0ae1af39dd99bb2a27a"}
        ]
        return configFacebookApp

    def getconfigFacebookApp23(self):
        configFacebookApp = [
            {"app_id": "265220050532934", "app_secret": "98d45482dea23686e13a3b620ecc56b3"},
            {"app_id": "1025043187585373", "app_secret": "e3e01dffd51815171c3680ab46743e0f"},
            {"app_id": "491475037704807", "app_secret": "1cd20cbd5d6e64100030344f0c07e6f1"},
            {"app_id": "123449588071690", "app_secret": "2f9305885a1115ef25914ac8853c1597"},
            {"app_id": "112210519197760", "app_secret": "6f0bb12d5c9f287d0a2406a8500e0667"}
        ]
        return configFacebookApp

    def getconfigFacebookApp24(self):
        configFacebookApp = [
            {"app_id": "660065047474330", "app_secret": "d9ff72039442fbada3fa13d8b8567aa6"},
            {"app_id": "1812437872345005", "app_secret": "1915083104d66a780faec15404cd3bef"},
            {"app_id": "786912898077053", "app_secret": "114283238fc4b3f3900ef68237950c12"},
            {"app_id": "1708119009438408", "app_secret": "f09da828dc92a4d684c6a4d2047ba3ed"},
            {"app_id": "665454796928417", "app_secret": "4bd9004a3d23e428eed66be9674ae8fd"}
        ]
        return configFacebookApp

    def getconfigFacebookApp25(self):
        configFacebookApp = [
            {"app_id": "1739861546227176", "app_secret": "0ae7000325e4c61021c83b2033b3fb7c"},
            {"app_id": "1612500195734126", "app_secret": "6c8255212d9196f8a14984f40d1d3bc0"},
            {"app_id": "1574662209499403", "app_secret": "bc390b4f8e440eaabe71b220a3cffdff"},
            {"app_id": "1085328151511189", "app_secret": "7c70a797ad600dbd1e39241178bded67"},
            {"app_id": "510319605759546", "app_secret": "d87b21bd8bbda7c362e50ab8678bdf5e"}
        ]
        return configFacebookApp

    def getconfigFacebookApp26(self):
        configFacebookApp = [
            {"app_id": "608592269290380", "app_secret": "40a185a3e24d0cd5b13b2613b79b7fd1"},
            {"app_id": "990331911021783", "app_secret": "ebed4fd885dc5c6defaa861d95bbb017"},
            {"app_id": "1550293725275665", "app_secret": "108e3489606b5ee9be1c2915910b6af2"},
            {"app_id": "131670077247672", "app_secret": "ed1a4faafebed99563aeb0eea2831bf6"},
            {"app_id": "1062524247151876", "app_secret": "1440823de5683f064889ae216474c046"}
        ]
        return configFacebookApp

    def getconfigFacebookApp27(self):
        configFacebookApp = [
            {"app_id": "1770880446459858", "app_secret": "ee3641b4f78a57f53278c81742248917"},
            {"app_id": "883812508430972", "app_secret": "3d298d8d9e68f80e10cb78900c6e3505"},
            {"app_id": "286588285007203", "app_secret": "98d35316153ede10e1ba2ef7cdb45aaf"},
            {"app_id": "1177952155562865", "app_secret": "7be9e4c71d52d39be84f5b5400b5b429"},
            {"app_id": "1018445758263400", "app_secret": "0fa90523a07203d25c11d79e43638c65"}
        ]
        return configFacebookApp

    def getconfigFacebookApp28(self):
        configFacebookApp = [
            {"app_id": "242991242728970", "app_secret": "5ac8a395ae4e8c3d7a555f164f3f020d"},
            {"app_id": "1574855516140625", "app_secret": "dca10d5b446503a56595035d1b92b2da"},
            {"app_id": "1535968280045182", "app_secret": "8cdd5fe26086a65558a47ec9ab1f0d88"},
            {"app_id": "1067055676703266", "app_secret": "68a0240cb8eb8592ead0fb0164c9ec37"},
            {"app_id": "1013866082029594", "app_secret": "b37c8d8a39abb82f936d06f9b1a2f75c"}
        ]
        return configFacebookApp

    def getconfigFacebookApp29(self):
        configFacebookApp = [
            {"app_id": "132165170524703", "app_secret": "3bd4f7477d90036cdddb69bf144bfdf2"},
            {"app_id": "136347453444956", "app_secret": "986fa61d307c1b4a261120a30e4f5791"},
            {"app_id": "132427647163327", "app_secret": "7abe959db49051888333d49052449882"},
            {"app_id": "582227745270672", "app_secret": "f89a7499809c4354c36162635c4aa3c9"},
            {"app_id": "257130441342712", "app_secret": "9f69a9a2770ef7b46e2a68648d3c3f4f"}
        ]
        return configFacebookApp

    def getconfigFacebookApp30(self):
        configFacebookApp = [
            {"app_id": "950787388352146", "app_secret": "fd8bc6fa96b041dbf01371d8342f3acb"},
            {"app_id": "189278768137584", "app_secret": "0ed67f37a03b9306b189a7fb33b4ab90"},
            {"app_id": "171865353215298", "app_secret": "6346403be476eaa710ebffaf122325ea"},
            {"app_id": "244537129242334", "app_secret": "c5401fbaa1e30f5232d26acdf9d2c979"},
            {"app_id": "902726619872846", "app_secret": "45e55da0a575042021fa244d8575bba9"}
        ]
        return configFacebookApp

    def getconfigFacebookApp31(self):
        configFacebookApp = [
            {"app_id": "1030946033609124", "app_secret": "62a2e144a3354a4c8176b26ace7385ac"},
            {"app_id": "282763915394693", "app_secret": "e1f927656440b392b0784c590e0f5b38"},
            {"app_id": "229694127412655", "app_secret": "3ddb1006c64255e1c6bb74bb57436576"},
            {"app_id": "258015637922029", "app_secret": "aa3e2b7c42a39693ae850a4932145482"},
            {"app_id": "976277102485739", "app_secret": "1205d8d8a6095ec8252f0c8b3d05385a"}
        ]
        return configFacebookApp

    def getconfigFacebookApp32(self):
        configFacebookApp = [
            {"app_id": "114709062340674", "app_secret": "04427484c2e1ab130f93b51f38349921"},
            {"app_id": "658626360972995", "app_secret": "1cf4ebd08e15c79c75b412b72918b638"},
            {"app_id": "246304642451697", "app_secret": "3081b1f064c7150c544cd99a125d9ab5"},
            {"app_id": "686268631541093", "app_secret": "0d28374de99b63f14c2777bc25b64a01"},
            {"app_id": "1845364115706994", "app_secret": "fba599e301d033a21fb16f0ba7c31dd8"},
            {"app_id": "1002807153131074", "app_secret": "bebecc3419d5f298701304cbdae2c21c"},
            {"app_id": "134986493580047", "app_secret": "b70b01966f73704854949b5b70bc39ac"},
            {"app_id": "816049251862398", "app_secret": "a653ace04b311053384a252f96b4791c"},
            {"app_id": "627643547389327", "app_secret": "002fc6198fe3b449b28dd2f0ff40da08"},
            {"app_id": "1012794815422605", "app_secret": "d8dcb4b9fe6c040cc8c6f4c6136c7ae5"},
            {"app_id": "1942490092740881", "app_secret": "aa7d86dde1e228d5746bbe5ef7fd544d"},
            {"app_id": "370374453409583", "app_secret": "16bd0b466af4a156c79cfc7a6a7a76ad"},
            {"app_id": "2056563504577796", "app_secret": "6f92d0eb00c8dc3ea537b15371df7e23"},
            {"app_id": "143237509763416", "app_secret": "0c7bd79b82baaa3fcdc89aeb1cd26244"},
            {"app_id": "1941969586127456", "app_secret": "afa5f7c54d17bd40cb573708a871150d"},
            {"app_id": "1361174593924666", "app_secret": "cd57348c5ad78f11383aacc69ea319f1"},
            {"app_id": "1330503357002291", "app_secret": "682ef0d4b7cc4c9e6ef17a3bb0ab2faa"},
            {"app_id": "1257764690949716", "app_secret": "448cc80c91a74db19329e344ed804fb5"},
            {"app_id": "626763190844431", "app_secret": "2c9fd29a3c76dc4a293b686434b83582"},
            {"app_id": "953140064787547", "app_secret": "1c446a26476ed3d098a612eed1ca2511"}
        ]
        return configFacebookApp

    def getconfigFacebookApp33(self):
        configFacebookApp = [
            {"app_id": "1181665315232425", "app_secret": "a98e1e1e56c078e0b313db29bd84b465"},
            {"app_id": "505147976359071", "app_secret": "1935b08e3bc958b84547bd68a6b835f2"},
            {"app_id": "1693851784213366", "app_secret": "84b558c1d610ea60d97c0b3149576b16"},
            {"app_id": "117305448687111", "app_secret": "9148055593f5c0b3fab865779cc1da35"},
            {"app_id": "614595502030470", "app_secret": "42dd2e2945f7f0ea2b3631e8971a2587"}
        ]
        return configFacebookApp

    def getconfigFacebookApp34(self):
        configFacebookApp = [
            {"app_id": "175097722891667", "app_secret": "57cdac9e7744c1337186c30ea90c83cc"},
            {"app_id": "1617199405263696", "app_secret": "4411a24e57f01ea2d1c6a31c9332d2da"},
            {"app_id": "1711657322453330", "app_secret": "5f37032bb6980df0b87930c618169fd2"},
            {"app_id": "1751403778469294", "app_secret": "4a0ed49e7e58668a5d67589845d85bdb"},
            {"app_id": "1532358360407405", "app_secret": "2a9616f02e8aeb7c262ceacf03d0404e"}
        ]
        return configFacebookApp

    def getconfigFacebookApp35(self):
        configFacebookApp = [
            {"app_id": "1695174620744580", "app_secret": "d951a7f16170882d7e86276a10b8005d"},
            {"app_id": "101791793568479", "app_secret": "ec9540958e2a66401ec95c6a89dc74a7"},
            {"app_id": "131510157257667", "app_secret": "def0b5d28e29db39888f5b4687187444"},
            {"app_id": "1591332307863305", "app_secret": "5b61ba569dea39b9f19b72579164ead6"},
            {"app_id": "244075732623257", "app_secret": "8e0dde22436868a3d1a22d32950abb36"}
        ]
        return configFacebookApp

    def getconfigFacebookApp36(self):
        configFacebookApp = [
            {"app_id": "621674947996785", "app_secret": "0458ed63f2db326c1e17599fddc6696d"},
            {"app_id": "1694469630803329", "app_secret": "4dde7020551905f2cff645972b9f8524"},
            {"app_id": "1809450415954580", "app_secret": "17b7eb138f5da2eb501645df1bfaa681"},
            {"app_id": "1782490495318220", "app_secret": "0fb1dfd06c79f4827f87452effe45448"},
            {"app_id": "603339236508859", "app_secret": "da3b71d2bf8751acac6a0ef5543420cc"}
        ]
        return configFacebookApp

    def getconfigFacebookApp37(self):
        configFacebookApp = [
            {"app_id": "1739124083060533", "app_secret": "d2efe7d431242794643fb579e8494569"},
            {"app_id": "142640393026836", "app_secret": "46ebb4d96c0e45b1317c3f7a2d8c20a2"},
            {"app_id": "159031461502387", "app_secret": "b0553e92a6d642445c87dda361cb4f28"},
            {"app_id": "146001712690005", "app_secret": "96f7813c5419d79e299a7eacefc47974"},
            {"app_id": "361906117590911", "app_secret": "4e91f0d67c0e761dd6eeb25f2d6b2c8c"},
            {"app_id": "1700493110213574", "app_secret": "ccb1f8e4d585a1e3ef0162e1f846aef4"},
            {"app_id": "239520803084801", "app_secret": "b0fd849de83d6361dbb4a6b987d996d0"},
            {"app_id": "284478665232828", "app_secret": "66528062656fffba1f8ff6f57feea31d"},
            {"app_id": "1776132755956790", "app_secret": "33ba71bf97fc447f48baac594b628e5e"},
            {"app_id": "290486014621657", "app_secret": "33f6b6fd0004a1abf2ecd3d943d95f4c"}
        ]
        return configFacebookApp

    def getconfigFacebookApp38(self):
        configFacebookApp = [
            {"app_id": "150269882385644", "app_secret": "08c0c13ba3b1aceb0ad24d063597bbca"},
            {"app_id": "366242067155188", "app_secret": "b83d1ed8629ce2b775a78da3cd4922a3"},
            {"app_id": "153788901896928", "app_secret": "0242d955348d21524adcc88a7c6b5790"},
            {"app_id": "181013792450296", "app_secret": "a09f67f6c5d0382f3a9d26f69739a018"},
            {"app_id": "517930201921008", "app_secret": "f5b25f90b95f9a151336451de715c0e8"},
            {"app_id": "296747937495186", "app_secret": "2ea4d32d0c6a144b6b0293af94c2507b"},
            {"app_id": "332126577254795", "app_secret": "5581a7ca29205da0b92cb08075f805de"},
            {"app_id": "390667028034115", "app_secret": "89c629bb7d7676eede40110c30bc59b9"},
            {"app_id": "1534965726623623", "app_secret": "76db9aa43951c4232ed2bceaa66b5cf7"},
            {"app_id": "110323689695543", "app_secret": "55e2e29bb9dab54acaae46258e9d3aaa"},
            {"app_id": "134091713668728", "app_secret": "e84bc517d867437b58ee8ffee5ef2968"},
            {"app_id": "1703876466544303", "app_secret": "48422761a8ebc04c5790b4b98b2bb6c8"},
            {"app_id": "493991640786678", "app_secret": "bec06ca4133eff602bb8b1749d67df05"},
            {"app_id": "108443229575589", "app_secret": "92c95e09af5e6ba9aaaeb829d002c7dc"},
            {"app_id": "604404089717725", "app_secret": "da1f0bb1d2e92c4a3910d4a142156309"}
        ]
        return configFacebookApp

    def getconfigFacebookApp39(self):
        configFacebookApp = [
            {"app_id": "288345971685380", "app_secret": "57782dc870b0a9fd3b282c62034e2964"},
            {"app_id": "2074979196064471", "app_secret": "243c6d0662323df4cf05df3f5b16fe51"},
            {"app_id": "398071407277442", "app_secret": "0c12f1f87ef1786fd9d4180612f859c5"},
            {"app_id": "144747346147583", "app_secret": "de7f39ec6cff5cd91afc60e4bee38c4a"},
            {"app_id": "294424244382765", "app_secret": "5e5b0805979f93ef9723f5369a3ffc56"},
            {"app_id": "1114825365223133", "app_secret": "66f4cd8dda85d9aae6412c65ed5edd67"},
            {"app_id": "235365876837468", "app_secret": "71d4524548a9fdf8a6453ea51e135487"},
            {"app_id": "180435692356527", "app_secret": "66ca50753c5136e47bf4533e5b5ea738"},
            {"app_id": "1716025228645366", "app_secret": "73048559730d7307dab2f07e91f46f6e"},
            {"app_id": "243330752695900", "app_secret": "aa43b6020ab8e7c270de611943d7ea13"}
        ]
        return configFacebookApp

    def getconfigFacebookApp40(self):
        configFacebookApp = [
            {"app_id": "1009587282453034", "app_secret": "babd674255e0297dd21655925d7d1f6c"},
            {"app_id": "1553437734958875", "app_secret": "a09b107981977a8277c0f4ddc27d280e"},
            {"app_id": "1119271814803259", "app_secret": "0a640b54ac15c332204687ba2b47a19d"},
            {"app_id": "1809170479311360", "app_secret": "9cd17076fe252fd8f5f45e51c5f40175"},
            {"app_id": "1059450920802729", "app_secret": "1ad743f0db097e62afe16c0de75886e6"}
        ]
        return configFacebookApp

    def getconfigFacebookApp41(self):
        configFacebookApp = [
            {"app_id": "1059454990802322", "app_secret": "7f2fd2aefe804114e0eb3770e659c0f6"},
            {"app_id": "1059455284135626", "app_secret": "d57e6b73ca0c5576eee145ca26c7ad21"},
            {"app_id": "1059457404135414", "app_secret": "206f52ea6e1d3f5df3dc076e63f16070"},
            {"app_id": "1059457864135368", "app_secret": "289cdbbcc9b4ef001b50c0b953e3d2a2"},
            {"app_id": "1059458277468660", "app_secret": "1d9df8c655937c2789b621037886503a"}
        ]
        return configFacebookApp

    def getconfigFacebookApp42(self):
        configFacebookApp = [
            {"app_id": "1059459067468581", "app_secret": "fb83c353a4848fc2203326ca650a5e96"},
            {"app_id": "1059460907468397", "app_secret": "ad66bd639986d66923084aed49c61a4e"},
            {"app_id": "1059461307468357", "app_secret": "8bfce31f4919c65075765ad650216901"},
            {"app_id": "1059461604134994", "app_secret": "9adfaf9cd0bf2e1dd6419fb032b72bcd"},
            {"app_id": "1059461874134967", "app_secret": "561aa7be25ed1c8f8f524fed95c2beb9"}
        ]
        return configFacebookApp

    def getconfigFacebookApp43(self):
        configFacebookApp = [
            {"app_id": "1059462137468274", "app_secret": "a82a108901f90ae1b1828bb5b9ee651a"},
            {"app_id": "1059462524134902", "app_secret": "ea2c16ca4df12e7ff7fcce6b1729ae39"},
            {"app_id": "1059462900801531", "app_secret": "ec2b85d4637caf63a4137f9696c108f1"},
            {"app_id": "1059463054134849", "app_secret": "1ca10339c6a6178146d9d9471203395f"},
            {"app_id": "1059467564134398", "app_secret": "7610ed771910cc66b7cc45a9db889c72"}
        ]
        return configFacebookApp

    def getconfigFacebookApp44(self):
        configFacebookApp = [
            {"app_id": "1059468077467680", "app_secret": "e047be6fd047c1d7b080b5b6f3c3fe67"},
            {"app_id": "1059468400800981", "app_secret": "2c9676c11cd29e611604c46a6d2e3f38"},
            {"app_id": "1059472034133951", "app_secret": "8b5aceb6fd1ca30e2c5aa2825cb07be8"},
            {"app_id": "1059472230800598", "app_secret": "38044a4338cd0b301d006bfb553da283"},
            {"app_id": "1059474517467036", "app_secret": "4df4bd6d7c74326410d569ac5442da8e"}
        ]
        return configFacebookApp

    def getconfigFacebookApp45(self):
        configFacebookApp = [
            {"app_id": "1059477274133427", "app_secret": "b696deec985e0687bbe1c04b26be5bc1"},
            {"app_id": "1059478037466684", "app_secret": "fff5dc2c97473e7d45a7a848b515d973"},
            {"app_id": "1059478457466642", "app_secret": "d4da7a04eb7c4ef9fa2325c6b272e6d5"},
            {"app_id": "1059479207466567", "app_secret": "657ae84822aa703cecff4d582540e5ed"},
            {"app_id": "1059479390799882", "app_secret": "87dd6efa6a4fb3ec2acbcfc120ca0414"}
        ]
        return configFacebookApp

    def getconfigFacebookApp46(self):
        configFacebookApp = [
            {"app_id": "1059479610799860", "app_secret": "f425828bb4e67a711eb8f7ea7061085a"},
            {"app_id": "1059479717466516", "app_secret": "c0f7b381cd073e1729ee3f99bd7af689"},
            {"app_id": "1059479830799838", "app_secret": "4728e0feb6d38e86f84a3eb16e002861"},
            {"app_id": "1059480334133121", "app_secret": "3b53691bde49c3b82f11335a246e07e0"},
            {"app_id": "1196681247062273", "app_secret": "a24d1552db6e8104a99eb697d7d63bde"}
        ]
        return configFacebookApp

    def getconfigFacebookApp47(self):
        configFacebookApp = [
            {"app_id": "1196683323728732", "app_secret": "080696f1225625d7e71a9fd75969588f"},
            {"app_id": "520493158152366", "app_secret": "ab1b658d2fc80819ee549f87694bff0f"},
            {"app_id": "114270262325442", "app_secret": "613b5aa16a5582f17cd20e1dc9b37128"},
            {"app_id": "1336856666329501", "app_secret": "6656360f3f2068274be3b8d015536869"},
            {"app_id": "695832507221962", "app_secret": "31992668fcc1a140b13d7496dfdf13e1"}
        ]
        return configFacebookApp

    def getconfigFacebookApp48(self):
        configFacebookApp = [
            {"app_id": "1596938307301330", "app_secret": "7bc2f5e405c0364bfebd33b96847f8b2"},
            {"app_id": "1691676151095759", "app_secret": "3babbc2d650bab96247ca5034e1b629b"},
            {"app_id": "104593673295362", "app_secret": "65d163e259183a5765450fe9a77dbb64"},
            {"app_id": "134746276936160", "app_secret": "0638262a7e9b0dfecd69871ac07aac8d"},
            {"app_id": "137214856693213", "app_secret": "4571db758beb71543abe0774293d8488"}
        ]
        return configFacebookApp

    def getconfigFacebookApp49(self):
        configFacebookApp = [
            {"app_id": "476125355922754", "app_secret": "f453a24d8d9d74e811e94f11d3b7c1ca"},
            {"app_id": "137363930011390", "app_secret": "e8be74599ce2da2a9e656511fd65597b"},
            {"app_id": "1628443977482514", "app_secret": "f4bd933647edb2eae7e60ca1e499c6b9"},
            {"app_id": "938745592901526", "app_secret": "a01a8eb68e616e03ce92760e7feb0097"},
            {"app_id": "506195366249543", "app_secret": "cb7553cd3f4c4275a33da0f4c3516af4"}
        ]
        return configFacebookApp

    def getconfigFacebookApp50(self):
        configFacebookApp = [
            {"app_id": "274655382883675", "app_secret": "50552279950b21ac72f61aedad140f36"},
            {"app_id": "629972620514788", "app_secret": "86f8ce81739cec13f471fd12a45fd8f6"},
            {"app_id": "1738161729732490", "app_secret": "4067af909f8c8d06fb0ab0b8b9889ee9"},
            {"app_id": "1714823095438895", "app_secret": "2021ddbd45accf397e586f8ec6de190a"},
            {"app_id": "1063557703714269", "app_secret": "9ef53c85a500bc1ec3173c7054d58219"}
        ]
        return configFacebookApp

    def getconfigFacebookApp51(self):
        configFacebookApp = [
            {"app_id": "1270223509673778", "app_secret": "2c64b6d292bbbbe286c0471a63cc522e"},
            {"app_id": "867962159999252", "app_secret": "4feb3420848fc0bb075cf94eb0b56f50"},
            {"app_id": "719133948189900", "app_secret": "d29bc62f4a3d102f3297f3e646577e24"},
            {"app_id": "180867228980253", "app_secret": "b548acbe8aac294311c4d5ce37bf098d"},
            {"app_id": "457690681105001", "app_secret": "33183b292d98bceebc148ab1bd18a586"}
        ]
        return configFacebookApp

    def getconfigFacebookApp52(self):
        configFacebookApp = [
            {"app_id": "124579218238699", "app_secret": "0a3748a3291da89cb6cdaee0f2f44313"},
            {"app_id": "408214979595630", "app_secret": "96565d73220cd54d34c37d8ba200332d"},
            {"app_id": "179695709275245", "app_secret": "8d98b5fe42f9062515d1d42e9f9eb045"},
            {"app_id": "1890449827949657", "app_secret": "e0815134714ec45deb54dd7665c4fedd"},
            {"app_id": "1522990587784198", "app_secret": "5ae15ba54d3781115a808a1244b2f8ac"},
            {"app_id": "1692244687694382", "app_secret": "13537f3f25f9b86dba27795095fc46cb"},
            {"app_id": "1034689026598465", "app_secret": "a043fc0afc988730709fbaf85e296f60"},
            {"app_id": "475066149359165", "app_secret": "c1d51e73265dd823dbba1643bb0cef30"},
            {"app_id": "245987669099623", "app_secret": "838a323d717df2c3edf19bf00f127b01"},
            {"app_id": "1024619377629892", "app_secret": "62b013b3e9278c0854c9096a9bc96417"}
        ]
        return configFacebookApp

    def getconfigFacebookApp53(self):
        configFacebookApp = [
            {"app_id": "941993495899573", "app_secret": "20024bd0bff5b352f249e6cbdc0c5b7a"},
            {"app_id": "592716317577246", "app_secret": "7d76679a02f7706b324b3ee3824fae9f"},
            {"app_id": "1563222717306457", "app_secret": "cc8873c51c59cd47398f457d59f3e7c3"},
            {"app_id": "1141697675893641", "app_secret": "224a24ed71e4f2970b14ec9e7ff459d3"},
            {"app_id": "168690803533230", "app_secret": "a843f2b4e1049570517cc4b4a1d640c6"}
        ]
        return configFacebookApp

    def getconfigFacebookApp54(self):
        configFacebookApp = [
            {"app_id": "276821812666825", "app_secret": "77742be48af7d17f0f5cbda959f807fd"},
            {"app_id": "1231439810207745", "app_secret": "616a48bd9278d93c64452d23e26fbc3b"},
            {"app_id": "1629937037328802", "app_secret": "c63de8456720ee0aed5535761e103fad"},
            {"app_id": "116296212122510", "app_secret": "c0f5c3cc8817d7af0b04f94e85e65d14"},
            {"app_id": "241829006189802", "app_secret": "41f8b1d1f0c1045b47a1cec344b056ee"}
        ]
        return configFacebookApp

    def getconfigFacebookApp55(self):
        configFacebookApp = [
            {"app_id": "1591237677854285", "app_secret": "e3a4c6c7c8762296b2c90c3a0418b277"},
            {"app_id": "708687202607572", "app_secret": "23b9f6176e7a06734e91451fe8905860"},
            {"app_id": "886637454795745", "app_secret": "515d97a60d4932a1c07e6dbcb9f04440"},
            {"app_id": "1152168641493946", "app_secret": "ddeebb075c672d193a53530661aeeaa5"},
            {"app_id": "500781690124660", "app_secret": "1494f9b71682122a1ef36d4b3592bf2e"}
        ]
        return configFacebookApp

    def getconfigFacebookApp56(self):
        configFacebookApp = [
            {"app_id": "279668539042273", "app_secret": "d375944e199eb954cb882262e266bdf0"},
            {"app_id": "572646752913961", "app_secret": "9f8f4bd16bf604b70676f355e62e27a7"},
            {"app_id": "1592509117713968", "app_secret": "664eb895631c5f9d65580f23b904551d"},
            {"app_id": "1025521930866769", "app_secret": "b3f1bd0318cd8cd5aa1c672765a1fc87"},
            {"app_id": "456919944506553", "app_secret": "15afa48fbd8fccd45f78b7ba823800ed"}
        ]
        return configFacebookApp

    def getconfigFacebookApp57(self):
        configFacebookApp = [
            {"app_id": "1060690890678732", "app_secret": "057c86f3b1fc1fac9b4064d5caa912ef"},
            {"app_id": "1060691054012049", "app_secret": "1128bc75fa2cf13691c7420cd8efe9c0"},
            {"app_id": "1060691180678703", "app_secret": "5831ef9323e533a72d7d29e41f9d901f"},
            {"app_id": "1060692354011919", "app_secret": "f03aa5bb131f942992278ad7557b4081"},
            {"app_id": "1060692474011907", "app_secret": "02d9aef43615c208838e300946c13003"}
        ]
        return configFacebookApp

    def getconfigFacebookApp58(self):
        configFacebookApp = [
            {"app_id": "1060692720678549", "app_secret": "9d578564c5c07a7d4d8f855cc170f897"},
            {"app_id": "1060692880678533", "app_secret": "f8e91ad646dae4d4c0ce6aa9b3694c9f"},
            {"app_id": "1060692977345190", "app_secret": "38c4da4f8c1b216fc530bcb2bd4ebcf9"},
            {"app_id": "1060700930677728", "app_secret": "eed452d614bcceaad3d7b4b5d1db6377"},
            {"app_id": "1060701134011041", "app_secret": "7b3f979bb5ed8c60efedd47bfb14e478"}
        ]
        return configFacebookApp

    def getconfigFacebookApp59(self):
        configFacebookApp = [
            {"app_id": "1060701237344364", "app_secret": "22dd26e763c7e30696951aba8f3d1678"},
            {"app_id": "1060701397344348", "app_secret": "fdcea37af016e89294762eeda12e6938"},
            {"app_id": "1060701617344326", "app_secret": "098d21de42c56dcf6aba60a62141c73a"},
            {"app_id": "1060701964010958", "app_secret": "8d9442a16315bb0968dac7482a6f9c7f"},
            {"app_id": "1060702554010899", "app_secret": "81e1323a8d775a89a9c756b8be926755"}
        ]
        return configFacebookApp

    def getconfigFacebookApp60(self):
        configFacebookApp = [
            {"app_id": "1060702734010881", "app_secret": "d38f5ce558aa448ca5889e3b7f4df9ae"},
            {"app_id": "1060702954010859", "app_secret": "2d0f07f3bc43f6813aac16e463ffb140"},
            {"app_id": "1060703060677515", "app_secret": "a642eba59629dbad9f4eb42c3fb95449"},
            {"app_id": "1060703290677492", "app_secret": "8477e668179272859233d8202e39cf65"},
            {"app_id": "1060704710677350", "app_secret": "313caf9ae44d775a01ad9439b4c6ee6c"}
        ]
        return configFacebookApp


    def getAccessTokenUser(self):
        # return "access_token=EAAH03ZCuZBFa4BAO6JoFXO3e8k45jGWZBIxEaczgZAGeNMZCSs1pfLQfJJSZBeh2ZCSZAbuMZAP1TmMzHcCcjKvvp9ZCIDySzYca2J8cPNUpKE618VYgGU9nJsnApPElVTokY91OwOU1UlUou5fHwiTsyEDPaQlL1NjpAZD"
        # NICK cdhung87@gmail.com | APP giaytot
        # return "access_token=EAAKjn7ARf5QBAG96iGO7S0Sb4V3ccwKqLSLay8eqCSLZCFR6Yd9f0cMXIDvK5wI75lfbUW4EKZCidvmmvZBtZArZBzzEq5xpVS26J04XMmJtwvorJcqdjf3KS7fHxiSuSitziFomjvTiL4kv4NKZCQh4rREBb1OD4ZD"
        # NICK cdhung87@gmail.com -- Access_token user join group
        return "access_token=EAAAAUaZA8jlABAIxEnlcj6bpdTqRs82EacFiZCQQVW4LKcGRAEvvDe7VJ3ceFZADxcr77OF8IpmVLCJxd4oLcIX8tBeg9OIgAaj8MO714oejbipb1ZAq17oXLLzEai2c2BPmF01NJ1Bn1jPsrbmSqeqR1n9qHwDSabKNdzYSJyF3sZACjyUSx"

    def getListAccessTokenUser(self):
        configFacebookApp = [
            # NICK ngochuyn1208@gmail.com | 12345@huyen
            {"access_token": "EAAAAUaZA8jlABAD1ZAQ3ZBDZAu9nEbzK3NsewefkfUOSbTylbkdQMxq8sPXfjUXnkKxnc7EPwcpm7ZAad8cgU34oOHoj9yaXoOzZBEYTfLtL7pr22BYdn4wx3lyRkZCo0oVmQx8uAYmmZAfZA3IyhSIG5hgY1y7dU5b4CKNZCuoMKiIAZDZD"},
            # NICK ngochuyn1208@gmail.com | 12345@huyen
            {"access_token": "EAAAAUaZA8jlABAD1ZAQ3ZBDZAu9nEbzK3NsewefkfUOSbTylbkdQMxq8sPXfjUXnkKxnc7EPwcpm7ZAad8cgU34oOHoj9yaXoOzZBEYTfLtL7pr22BYdn4wx3lyRkZCo0oVmQx8uAYmmZAfZA3IyhSIG5hgY1y7dU5b4CKNZCuoMKiIAZDZD"},
        ]
        return configFacebookApp


    def getServerVersion(self):
        return "SV01"