# -*- coding: utf-8 -*-
__author__ = 'DucHung'
import scrapy
from scrapy.conf import settings
import sys; print sys.path
from scrapy.http import Request
from scrapy.http import FormRequest
import MySQLdb
from elasticsearch import Elasticsearch
import time
from datetime import datetime
from datetime import date
from scrapy.http.headers import Headers
from scrapy_splash import SplashRequest
from scrapy.selector import Selector
import json
from pytz import timezone
import pytz
import urllib
from random import randrange
import urllib2
import dateutil.parser
import calendar
import random
import six
from token_facebook import tokenfacebook
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher

class ScrapefacebookItem(scrapy.Item):
    # define the fields for your item here like:

    # ten nguoi post
    fb_from_name = scrapy.Field()
    # user id nguoi post
    fb_from_uid = scrapy.Field()
    # Noi dung cua post
    fb_message = scrapy.Field()
    # Thoi diem tao post
    fb_created = scrapy.Field()
    # Thoi diem sua post
    fb_updated = scrapy.Field()
    # So shared cua post
    fb_share_count = scrapy.Field()
    # So like cua post
    fb_like_count = scrapy.Field()
    # Id cua post
    fb_post_id = scrapy.Field()
    # Type cua post (photo, link, ...)
    fb_post_type = scrapy.Field()
    # Link post (neu co)
    fb_post_link = scrapy.Field()
    # Du lieu lay duoc tu facebook
    fb_data = scrapy.Field()
    # Id facebook page
    fb_page_id = scrapy.Field()
    # Ten facebook page
    fb_page_name = scrapy.Field()
    # convert tu fb_updated
    fb_updated_int = scrapy.Field()
    # facebook id cha
    fb_parent_id = scrapy.Field()
    # facebook id ong
    fb_grand_parent_id = scrapy.Field()
    # Nganh
    branch = scrapy.Field()
    sub_branch = scrapy.Field()
    # Da crawl 1, can crawl lai 0
    is_crawl = scrapy.Field()
    # Thoi diem crawl dau tien
    crawled_first_time = scrapy.Field()
    # Thoi diem crawl gan nhat
    crawled_time = scrapy.Field()
    # Thoi diem loc
    filtered_time = scrapy.Field()
    # Tag
    tag = scrapy.Field()
    # Doi tuong
    object = scrapy.Field()
    # Ten doi tuong page
    page_name_object = scrapy.Field()
    # Thoi diem loc trend
    trend_filtered_time = scrapy.Field()
    # Thoi diem check trung
    duplicate_filtered_time = scrapy.Field()
    # Trung 1, khong trung 0
    is_duplicated = scrapy.Field()
    # Da hash message check trung 1, chua hash 0
    is_hashed = scrapy.Field()
    # Da quet trend hay chua
    is_trend_filtered = scrapy.Field()
    # Ma dinh danh trang thai da filter lai hay chua, truong hop co su thay doi mac dinh la 0, can phai quet lai la 1
    is_filtered_updated = scrapy.Field()
    # Kieu :(0 la post, 1 la comment, 2 la reply)
    post_type = scrapy.Field()
    # (id bo do mongo tu sinh)
    parent_id = scrapy.Field()
    # (id ong do mongo tu sinh)
    grand_parent_id = scrapy.Field()
    # child_count: (Tong so con cua mot message, mac dinh la 0, post = count comment + count reply)
    child_count = scrapy.Field()
    # Diem danh gia
    point = scrapy.Field()
    # Do lien quan den doi tuong
    point_involve = scrapy.Field()
    # group page
    group_page = scrapy.Field()
    # Da chay qua bo loc 1, chua chay 0
    is_filtered_banking = scrapy.Field()
    is_filtered_education = scrapy.Field()
    is_filtered_tele = scrapy.Field()
    is_filtered_leisure = scrapy.Field()
    is_filtered_media = scrapy.Field()
    is_filtered_cele = scrapy.Field()
    is_filtered_service = scrapy.Field()
    is_filtered_property = scrapy.Field()
    is_filtered_clothing = scrapy.Field()
    is_filtered_transport = scrapy.Field()
    is_filtered_tourism = scrapy.Field()
    is_filtered_building = scrapy.Field()
    is_filtered_charity = scrapy.Field()
    is_filtered_argi = scrapy.Field()
    is_filtered_pharma = scrapy.Field()
    is_filtered_fina = scrapy.Field()
    is_filtered_food = scrapy.Field()
    is_filtered_drink = scrapy.Field()
    is_filtered_visual = scrapy.Field()
    is_filtered_it = scrapy.Field()
    is_filtered_furniture = scrapy.Field()
    is_filtered_household = scrapy.Field()
    is_filtered_other_1 = scrapy.Field()
    is_filtered_other_2 = scrapy.Field()
    is_filtered_other_3 = scrapy.Field()
    is_filtered_other_4 = scrapy.Field()
    is_filtered_other_5 = scrapy.Field()

class UpdateAccountSpider(scrapy.Spider):
    def __init__(self, brand_id = None, object_id=None, type=None):
        self.access_token = "access_token=EAAAAUaZA8jlABAMqclipAW89ohdQvZA4eiXTmcQciPZAeqp3jkmRbZB7tLHXmofGFpvONHmsdWeYLxqO7GrddaELQHOphGZBVBMC1RCX2C76yZAZAPRx9Ru5v1pggaZCJS0FzoH42wgZAoWZC0NaeYoBZAZAZAk9Ilw3O7xpr2bKsMNtdG34xZBZBHLEUQvVrRkZAlNv3jAZD"
        self.DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        self.DATETIME_FORMAT_BEGIN = "%Y-%m-%d 00:00:00"
        self.DATETIME_FORMAT_END = "%Y-%m-%d 23:59:59"
        self.actions_error=[]
        self.DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        self.es_doc_type = "data"
        self.es_index = "ainova"
        self.es = Elasticsearch(["http://localhost:9200/"])
        dispatcher.connect(self.spider_closed, signals.spider_closed)

        # Load access_token.........................................
        tokens = tokenfacebook().getconfigToken1()
        tms = len(tokens)
        type_channel = random.randrange(0,tms)
        type_channel = int(type_channel)
        token = "access_token="+tokens[type_channel]["access_token"]
        self.access_token = token
        print self.access_token

    handle_httpstatus_list = [403, 404, 400]
    name = "getinfobyfile"
    # download_delay = 2
    allowed_domains = ["facebook.com"]

    custom_settings = {
        'URLLENGTH_LIMIT': 20830,
        'CONCURRENT_REQUESTS': 1
    }  

    def start_requests(self):

        dataPost = [
            "665840443475172_2094724710586731",
            "730792090599120_1109321382575661",
            "967995570062111_2336541433268682",
            "567520573695920",

        ]
        file_input= "/root/crawler/fb_crawler/fb_crawler/spiders/fbid/id.txt"
        file = open(file_input)
        data = file.read();
        file.close()
        data = data.split()

        for fb_post_id in data:
            if "facebook" in fb_post_id:
                fb_post_id = fb_post_id.split(".com/")[1]
                if "_" in fb_post_id:
                    fb_post_id2 = fb_post_id.split("_")[1]
                    meta = {"fb_post_id": fb_post_id2, "branch" : "PERSONAL", "sub_branch": "DRINK"}
                    url_request = "https://graph.facebook.com/v1.0/"+str(fb_post_id2)+"?"+self.access_token
                    yield Request(url_request, callback=self.parse_result_info, meta=meta)

            meta = {"fb_post_id": fb_post_id, "branch" : "PERSONAL", "sub_branch": "DRINK"}
#            meta = {"fb_post_id": fb_post_id, "branch" : "DP_config", "sub_branch": "BANKING"}
            url_request = "https://graph.facebook.com/v1.0/"+str(fb_post_id)+"?"+self.access_token
            yield Request(url_request, callback=self.parse_result_info, meta=meta)
#            time.sleep(5)

    def parse_result_info(self, response):
        true_info = json.loads(response.body)
        print true_info
        info = {}
        info["fb_user_id"] = true_info["from"]["id"]
        info["fb_user_name"] = true_info["from"]["name"]
        info["fb_share"] = 0
        info["fb_like"] = 0
        info["fb_message"] = ""
        if "message" in true_info:
            info["fb_message"] = true_info["message"]

        if "story" in true_info:
            info["fb_message"] = info["fb_message"] +" "+ true_info["story"]

        if "description" in true_info:
            info["fb_message"] = info["fb_message"] +" "+ true_info["description"]

        if "name" in true_info:
            info["fb_message"] = info["fb_message"] +" "+ true_info["name"]

        print info["fb_message"]

        if "updated_time" in true_info:
            x = dateutil.parser.parse(true_info['updated_time'])
        elif "created_time" in true_info:
            x = dateutil.parser.parse(true_info['created_time'])

        if "updated_time" in true_info:
            info["fb_created"] = true_info['updated_time']
        if "created_time" in true_info:
            info["fb_created"] = true_info['created_time']

        time_int = calendar.timegm(x.timetuple())

        current_time = int(time.time())

        item = ScrapefacebookItem()
        # INFO INSERT DATA
        item['fb_from_name'] = info["fb_user_name"]
        item['fb_from_uid'] = str(info["fb_user_id"])
        item['fb_message'] = info["fb_message"]
        item['fb_created'] = info["fb_created"]
        item['fb_updated'] = ''
        item['fb_updated_int'] = time_int
        item['fb_share_count'] = info["fb_share"]
        item['is_duplicated'] = 2
        item['fb_post_id'] = response.meta["fb_post_id"]
        item['fb_post_type'] = 0
        item['fb_post_link'] = ''
        item['crawled_time'] = int(current_time)
        item['fb_page_id'] = str(info["fb_user_id"])
        item['fb_page_name'] = info["fb_user_name"]
        item['branch'] = response.meta['branch']
        item['sub_branch'] = response.meta['sub_branch']
        item['fb_like_count'] = info["fb_like"]
        item['fb_data'] = ''
        item['is_crawl'] = 0
        item['crawled_first_time'] = item['crawled_time']
        item['tag'] = ''
        item['object'] = ''
        item['is_hashed'] = 0
        item['child_count'] = 0
        item['parent_id'] = ''
        item['grand_parent_id'] = ''
        item['fb_parent_id'] = ''
        item['fb_grand_parent_id'] = ''
        item['post_type'] = 0
        item['page_name_object'] = ''
        item['group_page'] = "UGR1"

        # filtered list
        if (item['fb_post_id'].find('_') != -1):
            duplicate = self.es.exists(index=self.es_index, doc_type=self.es_doc_type, id=item['fb_post_id'])
            if(duplicate is True):
                print "==========================================="
                print "DUPLICATE"
                print "================= " + str(response.meta["fb_post_id"].strip().encode('utf-8'))
                print "==========================================="
                # self.updateStatusUser(response.meta["fb_post_id"])
                pass
            else:
                try:
                    print "==========================================="
                    print "INSERT: " + str(response.meta["fb_post_id"].strip().encode('utf-8'))

                # insert to DB mysql
#                self._set_raw(response.meta["fb_post_id"], item)

                # update status DB
#                self.updateStatusUser(response.meta["fb_post_id"])

                # print item
                    print "=============== SUCCESSFULLY =============="
                    print "==========================================="

                    self.es.index(index=self.es_index, doc_type=self.es_doc_type, id=item['fb_post_id'], body=dict(item))
                    pass
                except Exception,e:
                    print "==========================================="
                    print str(e)
                    print "===================== ERROR ==> INSERT"
                    print "==========================================="
                    actions_error = {
                    "fb_id": response.meta["fb_post_id"]
                    }
        else: 
            print ("id ngan")
            post_id_new = item['fb_from_uid']+"_"+item['fb_post_id']
            print post_id_new
            meta_new = {"fb_post_id": post_id_new, "branch" : "PERSONAL", "sub_branch": "DRINK"}
#            meta = {"fb_post_id": fb_post_id, "branch" : "DP_config", "sub_branch": "BANKING"}
            url_request_new = "https://graph.facebook.com/v1.0/"+str(post_id_new)+"?"+self.access_token
            yield Request(url_request_new, callback=self.parse_result_info2, meta=meta_new)

    def parse_result_info2(self, response):
        true_info = json.loads(response.body)

        info = {}
        info["fb_user_id"] = true_info["from"]["id"]
        info["fb_user_name"] = true_info["from"]["name"]
        info["fb_share"] = 0
        info["fb_like"] = 0
        info["fb_message"] = ""
        if "message" in true_info:
            info["fb_message"] = true_info["message"]

        if "story" in true_info:
            info["fb_message"] = info["fb_message"] +" "+ true_info["story"]

        if "description" in true_info:
            info["fb_message"] = info["fb_message"] +" "+ true_info["description"]

        if "name" in true_info:
            info["fb_message"] = info["fb_message"] +" "+ true_info["name"]

        print info["fb_message"]

        if "updated_time" in true_info:
            x = dateutil.parser.parse(true_info['updated_time'])
        elif "created_time" in true_info:
            x = dateutil.parser.parse(true_info['created_time'])

        if "updated_time" in true_info:
            info["fb_created"] = true_info['updated_time']
        elif "created_time" in true_info:
            info["fb_created"] = true_info['created_time']

        time_int = calendar.timegm(x.timetuple())

        current_time = int(time.time())

        item = ScrapefacebookItem()
        # INFO INSERT DATA
        item['fb_from_name'] = info["fb_user_name"]
        item['fb_from_uid'] = str(info["fb_user_id"])
        item['fb_message'] = info["fb_message"]
        item['fb_created'] = info["fb_created"]
        item['fb_updated'] = ''
        item['fb_updated_int'] = time_int
        item['fb_share_count'] = info["fb_share"]
        item['is_duplicated'] = 2
        item['fb_post_id'] = response.meta["fb_post_id"]
        item['fb_post_type'] = 0
        item['fb_post_link'] = ''
        item['crawled_time'] = int(current_time)
        item['fb_page_id'] = str(info["fb_user_id"])
        item['fb_page_name'] = info["fb_user_name"]
        item['branch'] = response.meta['branch']
        item['sub_branch'] = response.meta['sub_branch']
        item['fb_like_count'] = info["fb_like"]
        item['fb_data'] = ''
        item['is_crawl'] = 0
        item['crawled_first_time'] = item['crawled_time']
        item['tag'] = ''
        item['object'] = ''
        item['is_hashed'] = 0
        item['child_count'] = 0
        item['parent_id'] = ''
        item['grand_parent_id'] = ''
        item['fb_parent_id'] = ''
        item['fb_grand_parent_id'] = ''
        item['post_type'] = 0
        item['page_name_object'] = ''
        item['group_page'] = "UGR1"

        # filtered list
        duplicate = self.es.exists(index=self.es_index, doc_type=self.es_doc_type, id=item['fb_post_id'])
        if(duplicate is True):
            print "==========================================="
            print "DUPLICATE"
            print "================= " + str(response.meta["fb_post_id"].strip().encode('utf-8'))
            print "==========================================="
            # self.updateStatusUser(response.meta["fb_post_id"])
            pass
        else:
            try:
                print "==========================================="
                print "INSERT: " + str(response.meta["fb_post_id"].strip().encode('utf-8'))

                # insert to DB mysql
#                self._set_raw(response.meta["fb_post_id"], item)

                # update status DB
#                self.updateStatusUser(response.meta["fb_post_id"])

                # print item
                print "=============== SUCCESSFULLY =============="
                print "==========================================="

                self.es.index(index=self.es_index, doc_type=self.es_doc_type, id=item['fb_post_id'], body=dict(item))
                pass
            except Exception,e:
                print "==========================================="
                print str(e)
                print "===================== ERROR ==> INSERT"
                print "==========================================="
                actions_error = {
                "fb_id": response.meta["fb_post_id"]
                }


    def spider_closed(self, spider, reason):

        # print self.actions
        import json

        if len(self.actions_error) > 0:
            print "==========================="
            filename = "/home/crawler/updatePage/updatePage/spiders/log_error_get_post_id"
            open(filename, 'a+').write(json.dumps(self.actions_error) + "\n")
            print "==========================="
                
        print "END"
