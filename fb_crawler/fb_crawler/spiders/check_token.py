# -*- encoding: utf-8 -*-

import scrapy
import urllib2
import json
import time
import MySQLdb
from scrapy.http import Request
import ConfigParser
import os
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import sys
from datetime import datetime
from datetime import timedelta
import math

#import pymongo
#from pymongo import MongoClient
from scrapy.utils.project import get_project_settings
from elasticsearch import Elasticsearch
from helper import Helper
from token_facebook import tokenfacebook


class ChecktokenSpider(scrapy.Spider):
    def __init__(self, *args, **kwargs):
        config = ConfigParser.ConfigParser()
        config.read('extra_config.cfg')

        self.extra_config = config

        dispatcher.connect(self.spider_closed, signals.spider_closed)
        self.tokens = tokenfacebook().getconfigToken1()
        self.tokens_remove = []

    handle_httpstatus_list = [403, 400]
    name = "checktoken"
    allowed_domains = ["facebook.com"]

    #mysql connection db
    def mysqConnect(self):
        self.conn = MySQLdb.connect(user=self.extra_config.get('mysql','db_username'),
                                    passwd=self.extra_config.get('mysql','db_password'),
                                    db=self.extra_config.get('mysql','db_name'),
                                    host=self.extra_config.get('mysql','db_host'),
                                    charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    # mysql query db
    def mysqlQuery(self, sql, params=()):
        try:
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except (AttributeError, MySQLdb.OperationalError) as e:
            print 'exception generated during sql connection: ', e
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor

    def start_requests(self):
        tokens = tokenfacebook().getconfigToken1()
        # print self.tokens[0]
        # self.tokens.remove(self.tokens[0])
        # print self.tokens[0]
        # # i = 0
        for token in tokens:
            # print "==================="
            # print token["access_token"]
            # print "==================="

            url = "https://graph.facebook.com/v1.0/me/?access_token="+token["access_token"]
            yield Request(url, meta={"access_token": token["access_token"]})

    def parse(self, response):
        jsonresponse = json.loads(response.body_as_unicode())
        if response.status == 403 or response.status == 400:
            print "===================================="
            # print jsonresponse["error"]["error_subcode"]
            # 452: The session has been invalidated because
            # 490: Checkpoint account
            print response.meta["access_token"]
            print jsonresponse["error"]["error_subcode"]
            self.tokens_remove.append(response.meta["access_token"])
            print "===================================="
        # else:

    def spider_closed(self, spider, reason):
        for token in self.tokens:
            for token_remove in self.tokens_remove:
                if token["access_token"] == token_remove:
                    self.tokens.remove(token)

        string = ""
        for token_remove in self.tokens_remove:
            string = string + token_remove + "\n"

        # SAVE FILE TOKEN DIE
        filename = "response.txt"
        open(filename, 'wb').write(string)

        # SAVE FILE HELPER
        file_object = open("/data/python_prj/fb_crawler/fb_crawler/spiders/token_facebook.py", "r")
        info_object = file_object.read()
        file_object.close()

        string = info_object.split("= [")[1].split("]")[0]

        new_string = "\n"
        for token in self.tokens:
            new_string = new_string + '\t\t\t{"access_token": "'+token["access_token"]+'"}, \n'

        file_object = open("/data/python_prj/fb_crawler/fb_crawler/spiders/token_facebook.py", "w")
        file_object.write(info_object.replace(string, new_string))
        file_object.close()

        print "============================"
        print self.tokens
        print len(self.tokens)
        print len(self.tokens_remove)
        print "============================"
