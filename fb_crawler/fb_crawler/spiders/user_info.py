# -*- coding: utf-8 -*-
import scrapy
from scrapy.conf import settings
from scrapy.http import Request
from scrapy.http import FormRequest
from elasticsearch import Elasticsearch
from elasticsearch import helpers
import time
import math
from datetime import datetime
from datetime import timedelta
from scrapy.http.headers import Headers
from scrapy_splash import SplashRequest
from scrapy.selector import Selector
import json
from pytz import timezone
import pytz
import urllib
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import openpyxl

class UpdateAccountSpider(scrapy.Spider):
    def __init__(self, brand_id = None, object_id=None, type=None):
        self.access_token = "access_token=EAAAAUaZA8jlABAArvrsFk7T2VZAaz51WvwgpvMZAh1PbO9jO6ZCA1j9yKTukhz7tCTZCw9NIqZBWSIAK3axP4pQCl7j0LtJvUAf8jJiPI2RVlMhapXOHGuTFpSqSXZCsStuLWeOAZAgKSuNVr9ZCUk8VdnSZBVZAq77ZAs3etIhq88AgtYGjb0qd4upzlokhdBUt64IZD"
        self.DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        self.DATETIME_FORMAT_BEGIN = "%Y-%m-%d 00:00:00"
        self.DATETIME_FORMAT_END = "%Y-%m-%d 23:59:59"
        self.actions=[]
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    handle_httpstatus_list = [403, 404, 400]
    name = "updateInfoToExcel"
    # download_delay = 2
    allowed_domains = ["facebook.com"]

    custom_settings = {
        'URLLENGTH_LIMIT': 20830,
        'CONCURRENT_REQUESTS': 1
    }

    def start_requests(self):

        dataPost = [
            "100003037518390",
            "100005804046575",
            "100004113244302",
            "100010003839540",
        ]

        for fb_id in dataPost:
            url_request = "https://graph.facebook.com/v1.0/"+fb_id+"?"+self.access_token
            yield Request(url_request, meta={"fb_id": fb_id}, dont_filter = True)
#            time.sleep(5)

    def parse(self, response):
        fb_id = response.meta["fb_id"]

        try:
            info_request = json.loads(response.body_as_unicode())

            # get share count
            try:
                sex = info_request["gender"]
            except Exception:
                sex = ""

            try:
                location = info_request["location"]["name"]
            except Exception:
                location = ""
            try:
                hometown = info_request["hometown"]["name"]
            except Exception:
                hometown = ""

            try:
                education = info_request["education"][0]["school"]["name"]
            except Exception:
                education = ""

            try:
                education_type = info_request["education"][0]["type"]
            except Exception:
                education_type = ""

            try:
                relationship = info_request["relationship_status"]
            except Exception:
                relationship = ""

            work_type = ""
            try:
                list_work = info_request["work"]
                for work in list_work:
                    work_name = work["employer"]["name"]
                    work_type += work_name + "\n"
            except Exception:
                work_type = ""

            try:
                birthday = info_request["birthday"]
            except Exception:
                birthday = ""
            try:
                name = info_request["name"]
            except Exception:
                name = ""
            try:
                mobile_phone = info_request["mobile_phone"]
            except Exception:
                mobile_phone = ""
            try:
                interested_in = info_request["interested_in"]
            except Exception:
                interested_in = ""
            try:
                email = info_request["email"]
            except Exception:
                email = ""
            try:
                favorite_athletes = info_request["favorite_athletes"]["name"]
            except Exception:
                favorite_athletes = ""


            action = {
                "fb_id": fb_id,
                "location": location,
                "education": education,
                "sex": sex,
                "education_type": education_type,
                "work_type": work_type,
                "relationship": relationship,
                "birthday": birthday,
                "name": name,
                "mobile_phone": mobile_phone,
                "interested_in": interested_in,
                "email": email,
                "hometown": hometown,
                "favorite_athletes": favorite_athletes
            }
        except Exception,f:
            action = {
                "fb_id": fb_id,
                "location": "",
                "education": "",
                "sex": "",
                "education_type": "",
                "work_type": "",
                "relationship": "",
                "birthday": "",
                "name": "",
                "mobile_phone": "",
                "interested_in": "",
                "email": "",
                "hometown": "",
                "favorite_athletes": "",
            }

        self.actions.append(action)

    def spider_closed(self, spider, reason):

        print self.actions

        print "==================="
        wb = openpyxl.load_workbook('/data/python_prj/fb_crawler/user_info/example91.xlsx')
        sheet = wb.get_sheet_by_name('Sheet1')
        i = 1
        for info in self.actions:
            try:
                sheet['A' + str(i + 6)].value = str(info["fb_id"])
            except Exception:
                sheet['A' + str(i + 6)].value = info["fb_id"]
            try:
                sheet['B' + str(i + 6)].value = str(info["location"])
            except Exception:
                sheet['B' + str(i + 6)].value = info["location"]
            try:
                sheet['C' + str(i + 6)].value = str(info["education"])
            except Exception:
                sheet['C' + str(i + 6)].value = info["education"]
            try:
                sheet['D' + str(i + 6)].value = str(info["education_type"])
            except Exception:
                sheet['D' + str(i + 6)].value = info["education_type"]
            try:
                sheet['E' + str(i + 6)].value = str(info["sex"])
            except Exception:
                sheet['E' + str(i + 6)].value = info["sex"]
            try:
                sheet['F' + str(i + 6)].value = str(info["work_type"])
            except Exception:
                sheet['F' + str(i + 6)].value = info["work_type"]
            try:
                sheet['G' + str(i + 6)].value = str(info["relationship"])
            except Exception:
                sheet['G' + str(i + 6)].value = info["relationship"]
            try:
                sheet['H' + str(i + 6)].value = str(info["birthday"])
            except Exception:
                sheet['H' + str(i + 6)].value = info["birthday"]
            try:
                sheet['I' + str(i + 6)].value = str(info["name"])
            except Exception:
                sheet['I' + str(i + 6)].value = info["name"]
            try:
                sheet['I' + str(i + 6)].value = str(info["mobile_phone"])
            except Exception:
                sheet['I' + str(i + 6)].value = info["mobile_phone"]
            try:
                sheet['J' + str(i + 6)].value = str(info["interested_in"])
            except Exception:
                sheet['J' + str(i + 6)].value = info["interested_in"]
            try:
                sheet['K' + str(i + 6)].value = str(info["email"])
            except Exception:
                sheet['K' + str(i + 6)].value = info["email"]
            try:
                sheet['L' + str(i + 6)].value = str(info["hometown"])
            except Exception:
                sheet['L' + str(i + 6)].value = info["hometown"]
            try:
                sheet['M' + str(i + 6)].value = str(info["favorite_athletes"])
            except Exception:
                sheet['M' + str(i + 6)].value = info["favorite_athletes"]
            i+=1

        file_name = "DATA_EXPORT_PROFILE"
        file_name_export = str(file_name) + ".xlsx"
        wb.save("/data/python_prj/fb_crawler/user_info/"+file_name_export)
        print "END"
