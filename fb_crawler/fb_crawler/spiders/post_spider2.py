# -*- encoding: utf-8 -*-
#from pip._vendor.requests import api

import scrapy
import re
import urllib2
import json
import time
import calendar
import dateutil.parser
import MySQLdb
import ConfigParser
import datetime
import sys
from elasticsearch import Elasticsearch

TAG_RE = re.compile(r'<[^>]+>')
MATCH_GROUP_NAME_RE = re.compile(r'[a-zA-Z]')

from fb_crawler.items import fb_crawlerItem
from scrapy.http import Request
import os

from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from helper import Helper
from helper_v2 import HelperV2
import random

class PostSpider(scrapy.Spider):
    def __init__(self, service_id=None,pass_day=None,limit=None,group=None, str_func=None, *args, **kwargs):
#        config = ConfigParser.ConfigParser()
#        config.read('../extra_config.cfg')

        if(group == None):
            print "You must put group before running scrape."
            sys.exit(0)

        self.group_page = group
        self.str_func = str_func
#        self.extra_config = config
#        self.parent_table = config.get('mysql','table_page')
        self.process_id = os.getpid()
#        self.mysqConnect()

#        dispatcher.connect(self.spider_closed, signals.spider_closed)
#        dispatcher.connect(self.spider_opened, signals.spider_opened)

        self.es_doc_type = "data"
        self.es_index = "ainova"
        self.es = Elasticsearch(["http://localhost:9200/"])
        self.service_id = service_id
        self.since = ""
        self.limit = ""
        if(limit == None) :
            if(pass_day == None):
                pass_day = 7
            pass_day = int(pass_day)
            s = int(time.time()) - (24*60*60*pass_day)
            self.since = "&since=" + str(s)
        else:
            self.limit = "&limit=" + str(limit)

        self.app_index = 0
#        self.setConfig()
        # self.access_token = self.getAcessToken()
        self.access_token_user = self.setConfigAccessToken()

    handle_httpstatus_list = [400,403,404]
    name = "post2"
    allowed_domains = ["facebook.com"]

    def mysqConnect(self):
        self.conn = MySQLdb.connect(user=self.extra_config.get('mysql','db_username'),
                                    passwd=self.extra_config.get('mysql','db_password'),
                                    db=self.extra_config.get('mysql','db_name'),
                                    host=self.extra_config.get('mysql','db_host'),
                                    charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    def query(self, sql, params=()):
        try:
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except (AttributeError, MySQLdb.OperationalError) as e:
            print 'exception generated during sql connection: ', e
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor

    def setConfig(self):
        #self.configFacebookApp = Helper().getconfigFacebookApp+self.str_func()
        #self.configFacebookApp = Helper().globals()["getconfigFacebookApp"+self.str_func]()
        mod = globals()['Helper']()
        func = getattr(mod, "getconfigFacebookApp"+self.str_func)
        self.configFacebookApp = func()

    def setConfigAccessToken(self):
        tokens = HelperV2().getconfigToken1()
        return "access_token="+tokens[random.randrange(0, len(tokens))]["access_token"]

    def getAcessToken(self):
        app_id = self.configFacebookApp[self.app_index]['app_id']
        app_secret = self.configFacebookApp[self.app_index]['app_secret']
        api_endpoint = "https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id="+app_id+"&client_secret="+app_secret
        api_request = urllib2.Request(api_endpoint)
        api_response = urllib2.urlopen(api_request)
        return "access_token=" + json.loads(api_response.read())["access_token"]

    def isGroupName(self, page):
        if MATCH_GROUP_NAME_RE.match(page):
            return True
        return False

    def start_requests(self):
        page = 1
        limit = 1000
        dataPost = [           
            '{"page_id":"46889335859","page_name":"Baomoi"}',
        ]
        while True:
            print("\n------------------------")
            print("#Page: " + str(page) + "\n")
            start = (page - 1) * limit
#            sql = """SELECT id, page_id, page_type, page_name, branch, sub_branch FROM """+self.parent_table+""" WHERE group_page = %s ORDER BY id DESC LIMIT %s,%s"""
#            curr = self.query(sql,(self.group_page, start,limit))
#            if curr :
#                list_data = curr.fetchall()
#                if len(list_data) > 0:
#                    for row in list_data:
#                        id, fb_page_id, fb_page_type, fb_page_name, branch, sub_branch = row
            if len(dataPost) >0:
                if len(dataPost) >0:
                    for fb_id in dataPost:

                        output = json.loads(fb_id)
                        fb_id = output['page_id']

                        fb_page_id = fb_id
                        fb_page_type = "pages"
                        fb_page_name = output['page_name']
                        branch = "DRINK"
                        sub_branch = ""
                        graph = "posts"
                        point_involve = 0
                        if fb_page_type == "group":
                            graph = "feed"
                            point_involve = 1
                        if(self.access_token_user == None or self.access_token_user == ''):
                            self.access_token_user = self.setConfigAccessToken()

                        if fb_page_type == "group":
                            url = "https://graph.facebook.com/"+fb_page_id+"/"+graph+"/?"+self.access_token_user+"&fields=type,id,shares,link,from,message,created_time,updated_time,likes.summary(true)" + self.since + self.limit
                        else:
                            url = "https://graph.facebook.com/"+fb_page_id+"/"+graph+"/?"+self.access_token_user+"&fields=type,id,shares,link,from,message,created_time,updated_time,likes.summary(true)" + self.since + self.limit
                        if self.isGroupName(fb_page_id):
                            # print("Name: https://graph.facebook.com/"+page_id+"/?"+self.getAcessToken())
                            # url = "https://graph.facebook.com/"+page_id+"/?"+self.getAcessToken()
                            # url = "https://graph.facebook.com/search/?q="+page_id+"&"+self.getAcessToken() + "&type=group"
                            # print(url)
                            pass
                        else:
                            meta = {"fb_page_id": fb_page_id, "fb_page_name": fb_page_name, "branch": branch,"sub_branch": sub_branch, "first_page": 1, "id": id, "point_involve": point_involve}
                            # yield self.make_requests_from_url(url)
                            yield Request(url, meta=meta)
                else:
                    break
            else:
                break
            page = page + 1
            break

    def parse(self, response):
        current_time = time.time()
        jsonresponse = json.loads(response.body_as_unicode())

        if(response.status == 400 or response.status == 404):
            pass
            #if response.meta['first_page'] == 1:
                #self._set_status_code(response.status, response.meta['id'])
        if response.status == 403:
            # size_config = len(self.configFacebookApp)
            # insert app blocked to mysql
            # self._set_denied_app()
            # renew app new
            # self.app_index = self.app_index + 1
            # if self.app_index == size_config:
            #     self.app_index = 0
            # renew acesstoken
            self.access_token_user = self.setConfigAccessToken()
        else:
            if 'data' in jsonresponse:
                posts = jsonresponse['data']
                if len(posts) > 0:
                    for post in posts:
                        if 'message' in post:
                            item = fb_crawlerItem()
                            item['fb_from_name'] = ''
                            item['fb_from_uid'] = ''
                            if 'from' in post:
                                if 'name' in post['from']:
                                    item['fb_from_name'] = post['from']['name']
                                if 'id' in post['from']:
                                    item['fb_from_uid'] = post['from']['id']

                            item['fb_message'] = self.get_parse_data('message', post)
                            item['fb_created'] = ''
                            if 'created_time' in post:
                                item['fb_created'] = post['created_time']
                            else:
                                if 'updated_time' in post:
                                    item['fb_created'] = post['updated_time']

                            item['fb_updated'] = ''
                            item['fb_updated_int'] = 0
                            if 'updated_time' in post:
                                item['fb_updated'] = post['updated_time']
                                x = dateutil.parser.parse(post['updated_time'])
                                stamp = calendar.timegm(x.timetuple())
                                item['fb_updated_int'] = stamp

                            # a = post['updated_time']
                            # x.astimezone(pytz.timezone('Asia/Ho_Chi_Minh'))
                            # item['fb_updated_int'] = time.mktime(x.timetuple())

                            if 'shares' in post:
                                item['fb_share_count'] = post['shares']['count']
                            else:
                                item['fb_share_count'] = 0

                            item['fb_post_id'] = self.get_parse_data('id', post)
#                            item['fb_post_type'] = self.get_parse_data('type', post)
                            item['fb_post_type'] = 0

                            if 'link' in post:
                                item['fb_post_link'] = post['link']
                            else:
                                item['fb_post_link'] = ''

                            item['crawled_time'] = int(current_time)
                            item['fb_page_id'] = response.meta['fb_page_id']
                            item['fb_page_name'] = response.meta['fb_page_name']
                            item['branch'] = response.meta['branch']
                            item['sub_branch'] = response.meta['sub_branch']

                            like_count = 0
                            if 'likes' in post:
                                if 'summary' in post['likes']:
                                    if 'total_count' in post['likes']['summary']:
                                        like_count = post['likes']['summary']['total_count']

                            item['fb_like_count'] = like_count
                            #item['fb_data'] = json.dumps(post)
                            item['fb_data'] = ''
                            item['is_crawl'] = 0
                            item['crawled_first_time'] = item['crawled_time']
                            item['tag'] = ''
                            item['object'] = ''
                            item['is_duplicated'] = 1
                            item['is_hashed'] = 0
                            item['child_count'] = 0
                            item['parent_id'] = ''
                            item['grand_parent_id'] = ''
                            item['fb_parent_id'] = ''
                            item['fb_grand_parent_id'] = ''
                            item['post_type'] = 0
                            item['page_name_object'] = ''
                            item['group_page'] = self.group_page

                            yield item

                    if(self.limit == ""):
                        if 'paging' in jsonresponse:
                            paging = jsonresponse['paging']
                            if 'next' in paging:
                                next = paging['next']
                                response.meta['first_page'] = 0
                                yield Request(next, meta=response.meta)
            # elif response.meta['first_page'] == 1:
            #     self._set_priority(response.meta['id'])


    def get_parse_data(self, key, data):
        if key in data:
            return data[key]
        else:
            return ''

    def spider_opened(self,spider):
        if (self.service_id != None):
            current_time = time.time()
            sql1 = """SELECT id, process_id FROM crawl_manager_service_job WHERE service_id = %s AND status = 0 AND group_page = %s AND server_version = %s ORDER BY id DESC LIMIT 1"""
            process_info = self.query(sql1,(self.service_id, self.group_page, Helper().getServerVersion()))
            list_data = process_info.fetchone()
            if(list_data):
                sql = """DELETE FROM crawl_manager_service_job WHERE id = %s"""
                self.query(sql,(list_data[0],))
                command_str = 'kill -9 ' + list_data[1]
                os.system(command_str)

            sql = """INSERT INTO crawl_manager_service_job(service_id, process_id, status, start_time, group_page, server_version) VALUE (%s, %s, %s, %s, %s, %s)"""
            curr = self.query(sql,(self.service_id, self.process_id, 0, current_time, self.group_page, Helper().getServerVersion()))

    def spider_closed(self, spider, reason):
        if (self.service_id != None):
            current_time = time.time()
            sql = """UPDATE crawl_manager_service_job SET `status` = %s, `finish_time` = %s, `finish_reason` = %s WHERE `service_id` = %s AND `process_id` = %s"""
            curr = self.query(sql,(1,current_time,reason, self.service_id, self.process_id))

    def _set_priority(self, page_id):
        sql = """UPDATE """ + self.parent_table + """ SET `is_priority` = %s WHERE `id` = %s"""
        curr = self.query(sql,(0,page_id))

    def _set_status_code(self, code, page_id):
        sql = """UPDATE """ + self.parent_table + """ SET `status_code` = %s WHERE `id` = %s"""
        curr = self.query(sql,(code,page_id))

    def _set_lastest_post(self, time_stamp, page_id):
        sql = """UPDATE """ + self.parent_table + """ SET `lastest_post` = %s WHERE `id` = %s"""
        curr = self.query(sql,(time_stamp,page_id))

    def _set_is_code(self, code, id):
        sql = """UPDATE fb_page SET `is_code` = %s WHERE `id` = %s"""
        curr = self.query(sql,(code, id))

    def _set_is_filtered_time(self, code, id):
        sql = """UPDATE fb_page SET `filtered_time` = %s WHERE `id` = %s"""
        curr = self.query(sql,(code, id))

    def _set_denied_app(self):
        current_time = time.time()
        sql = """INSERT INTO fb_denied_app(app_id, created) VALUE (%s, %s)"""
        curr = self.query(sql,(self.configFacebookApp[self.app_index]['app_id'], current_time))