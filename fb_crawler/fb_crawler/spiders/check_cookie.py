# -*- encoding: utf-8 -*-

import scrapy
import urllib2
import json
import time
import MySQLdb
from scrapy.http import Request
import ConfigParser
import os
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import sys
from datetime import datetime
from datetime import timedelta
import math

#import pymongo
#from pymongo import MongoClient
from scrapy.utils.project import get_project_settings
from elasticsearch import Elasticsearch
from helper import Helper
from token_facebook import tokenfacebook


class ChecktokenSpider(scrapy.Spider):
    def __init__(self, *args, **kwargs):
        config = ConfigParser.ConfigParser()
        config.read('../extra_config.cfg')

        self.extra_config = config

        dispatcher.connect(self.spider_closed, signals.spider_closed)
        self.tokens = tokenfacebook().getconfigToken1()
        self.tokens_remove = []
        self.cookies = []

    handle_httpstatus_list = [403, 400]
    name = "checkcookiev1"
    allowed_domains = ["facebook.com"]

    def start_requests(self):
        tokens = tokenfacebook().getconfigToken1()
        # print self.tokens[0]
        # self.tokens.remove(self.tokens[0])
        # print self.tokens[0]
        # # i = 0
        for token in tokens:
            # print "==================="
            # print token["access_token"]
            # print "==================="

            url = "https://api.facebook.com/method/auth.getSessionforApp?format=json&new_app_id=350685531728&generate_session_cookies=1&session_cookies=true&access_token="+token["access_token"]
            yield Request(url, meta={"access_token": token["access_token"]})

    def parse(self, response):
        full_cookie = str(response.body)
        if response.status == 200 or response.status == 400:
            print "===================================="
            self.cookies.append(full_cookie)
            print "===================================="
        # else:

    def spider_closed(self, spider, reason):
        for cookie in self.cookies:
            print cookie
        new_string = "\n"
        for token in self.cookies:
            new_string = new_string + "\t\t\t{\"cookie\": '"+token+"'}, \n"

        file_object = open("/data/python_prj/fb_crawler/fb_crawler/spiders/cookie_facebooks.py", "w")
        start = """
import os, time
class cookiefacebooks:
\tdef getconfigCookie(self):
\t\tconfigFacebookApp = [
        """
        end = """
\t\t\t]
\t\treturn configFacebookApp
"""
        file_object.write(start)
        file_object.write(new_string)
        file_object.write(end)
        file_object.close()
