# -*- coding: utf-8 -*-

from _mysql import result
import scrapy
from scrapy.conf import settings
from scrapy.http import Request
from scrapy.http import FormRequest
import MySQLdb
from elasticsearch import Elasticsearch
import time
from datetime import datetime
from datetime import date
from scrapy.http.headers import Headers
from scrapy_splash import SplashRequest
from scrapy.selector import Selector
import json
from pytz import timezone
import pytz
import urllib
import demjson
from random import randrange
import urllib2
import dateutil.parser
import calendar
import random
import six
from token_facebook import tokenfacebook
from cookie_facebooks import cookiefacebooks
import ConfigParser

class WebItem(scrapy.Item):
    # define the fields for your item here like:
    fb_user_id = scrapy.Field()
    fb_user_name = scrapy.Field()
    fb_created = scrapy.Field()
    fb_message = scrapy.Field()
    fb_like = scrapy.Field()
    fb_share = scrapy.Field()
    pass

class ScrapefacebookItem(scrapy.Item):
    # define the fields for your item here like:

    # ten nguoi post
    fb_from_name = scrapy.Field()
    # user id nguoi post
    fb_from_uid = scrapy.Field()
    # Noi dung cua post
    fb_message = scrapy.Field()
    # Thoi diem tao post
    fb_created = scrapy.Field()
    # Thoi diem sua post
    fb_updated = scrapy.Field()
    # So shared cua post
    fb_share_count = scrapy.Field()
    # So like cua post
    fb_like_count = scrapy.Field()
    # Id cua post
    fb_post_id = scrapy.Field()
    # Type cua post (photo, link, ...)
    fb_post_type = scrapy.Field()
    # Link post (neu co)
    fb_post_link = scrapy.Field()
    # Du lieu lay duoc tu facebook
    fb_data = scrapy.Field()
    # Id facebook page
    fb_page_id = scrapy.Field()
    # Ten facebook page
    fb_page_name = scrapy.Field()
    # convert tu fb_updated
    fb_updated_int = scrapy.Field()
    # facebook id cha
    fb_parent_id = scrapy.Field()
    # facebook id ong
    fb_grand_parent_id = scrapy.Field()
    # Nganh
    branch = scrapy.Field()
    sub_branch = scrapy.Field()
    # Da crawl 1, can crawl lai 0
    is_crawl = scrapy.Field()
    # Thoi diem crawl dau tien
    crawled_first_time = scrapy.Field()
    # Thoi diem crawl gan nhat
    crawled_time = scrapy.Field()
    # Thoi diem loc
    filtered_time = scrapy.Field()
    # Tag
    tag = scrapy.Field()
    # Doi tuong
    object = scrapy.Field()
    # Ten doi tuong page
    page_name_object = scrapy.Field()
    # Thoi diem loc trend
    trend_filtered_time = scrapy.Field()
    # Thoi diem check trung
    duplicate_filtered_time = scrapy.Field()
    # Trung 1, khong trung 0
    is_duplicated = scrapy.Field()
    # Da hash message check trung 1, chua hash 0
    is_hashed = scrapy.Field()
    # Da quet trend hay chua
    is_trend_filtered = scrapy.Field()
    # Ma dinh danh trang thai da filter lai hay chua, truong hop co su thay doi mac dinh la 0, can phai quet lai la 1
    is_filtered_updated = scrapy.Field()
    # Kieu :(0 la post, 1 la comment, 2 la reply)
    post_type = scrapy.Field()
    # (id bo do mongo tu sinh)
    parent_id = scrapy.Field()
    # (id ong do mongo tu sinh)
    grand_parent_id = scrapy.Field()
    # child_count: (Tong so con cua mot message, mac dinh la 0, post = count comment + count reply)
    child_count = scrapy.Field()
    # Diem danh gia
    point = scrapy.Field()
    # Do lien quan den doi tuong
    point_involve = scrapy.Field()
    # group page
    group_page = scrapy.Field()
    # Da chay qua bo loc 1, chua chay 0
    is_filtered_banking = scrapy.Field()
    is_filtered_education = scrapy.Field()
    is_filtered_tele = scrapy.Field()
    is_filtered_leisure = scrapy.Field()
    is_filtered_media = scrapy.Field()
    is_filtered_cele = scrapy.Field()
    is_filtered_service = scrapy.Field()
    is_filtered_property = scrapy.Field()
    is_filtered_clothing = scrapy.Field()
    is_filtered_transport = scrapy.Field()
    is_filtered_tourism = scrapy.Field()
    is_filtered_building = scrapy.Field()
    is_filtered_charity = scrapy.Field()
    is_filtered_argi = scrapy.Field()
    is_filtered_pharma = scrapy.Field()
    is_filtered_fina = scrapy.Field()
    is_filtered_food = scrapy.Field()
    is_filtered_drink = scrapy.Field()
    is_filtered_visual = scrapy.Field()
    is_filtered_it = scrapy.Field()
    is_filtered_furniture = scrapy.Field()
    is_filtered_household = scrapy.Field()
    is_filtered_other_1 = scrapy.Field()
    is_filtered_other_2 = scrapy.Field()
    is_filtered_other_3 = scrapy.Field()
    is_filtered_other_4 = scrapy.Field()
    is_filtered_other_5 = scrapy.Field()

class GetNewPostEachAccountCustomSpider(scrapy.Spider):
    def __init__(self, account=None, type=None, real_type=None, type_channel=None):

        config = ConfigParser.ConfigParser()
        config.read('extra_config.cfg')
        self.extra_config = config
        self.parent_table = "fb_page"
        self.mysqConnect()

        self.xpath_box = './/div[@class="hidden_elem"]//comment()'
        self.xpath_post = './/p/text() | .//p//a//text() | .//div[@class="mtm"]//div/text() | .//div[@class="mtm"]//a/text() | (.//span[@role="presentation"])[last()]/text()'
        self.xpath_post_user_name = './/h5//a/text()'
        self.xpath_post_user_id = './/a/@data-hovercard'
        self.xpath_post_created = './/abbr/@data-utime'
        self.DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S%z"
        
        self.es_doc_type = config.get('elasticsearch', 'doc_type')
        self.es_index = config.get('elasticsearch', 'index')
        self.es = Elasticsearch([config.get('elasticsearch', 'dns')])

        self.cookies =""
        self.filename2 = "/data/python_prj/fb_crawler/fb_crawler/spiders/page.txt"
        self.headers = Headers(
            {
                'Content-Type': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'User-Agent'  : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'
            }
        )
        script = """
        function main(splash)
            assert(splash:go(splash.args.url))
        end
        """
        self.splash_args = {
            'html': 1,
            'png': 1,
            'width': 1920,
            'height': 1080,
            'render_all': 1,
            'wait': 20,
            'lua_source': script
        }
        if(account == None):
            self.account = "sacombank"
        else:
            self.account = account

        if(type == None):
            self.type = 1
        else:
            self.type = int(type)

        if(real_type == None):
            self.real_type = 0
        else:
            self.real_type = int(real_type)

# Load access_token.........................................

        if(type_channel == None):
            self.type_channel = 0
        else:
            self.type_channel = int(type_channel)

        tokens = tokenfacebook().getconfigToken1()
        tms = len(tokens)
        if(type_channel == None):
            self.type_channel = random.randrange(0,tms)
        else:
            self.type_channel = int(type_channel)

        self.limit_docs = 50
        token = "access_token="+tokens[self.type_channel]["access_token"]
        self.access_token = token
        print self.access_token

# Load cookie...................................................

        list_cookie = cookiefacebooks().getconfigCookie()
        tms = len(list_cookie)
        type_cookie = random.randrange(0,tms)
        cookie_user = list_cookie[type_cookie]["cookie"]
        output = json.loads(cookie_user)
#        print output['session_cookies']
#        output = json.loads('{"session_key":"5.6SiTlBOjqP-Hmg.1546588214.36-100032186028208","uid":100032186028208,"secret":"eda12d1a93bf099176b0bbade9e2bdea","access_token":"EAAAAUaZA8jlABAE5rhNuV8A6TQ6MhXLATwCTlZCB5HOCffcZB9D7750ZCbbZBviXfMXYiZAYITu9BiFjfFj2YssB9ZAyS4veg8FIoKqrZAhy9wQEZBZA6Dt4RXrlgCgYXrGA3VHPhaBd3alQRYx91SKWESZCWaSju6SQcYgX1kHzMGygA8T06Ctriq1","machine_id":"hhYvXNtLnDEsGiVx0iQnsirQ","session_cookies":[{"name":"c_user","value":"100032186028208","expires":"Sat, 04 Jan 2020 08:17:10 GMT","expires_timestamp":1578125830,"domain":".facebook.com","path":"\/","secure":true},{"name":"xs","value":"36:6SiTlBOjqP-Hmg:2:1546588214:-1:-1","expires":"Sat, 04 Jan 2020 08:17:10 GMT","expires_timestamp":1578125830,"domain":".facebook.com","path":"\/","secure":true,"httponly":true},{"name":"fr","value":"1Rh4Gyai9U08pgyhP.AWVmPT7-P1nlzM0MAk3PuRiQrQA.BcLxaG..AAA.0.0.BcLxaG.AWWbwpFy","expires":"Sat, 04 Jan 2020 08:17:10 GMT","expires_timestamp":1578125830,"domain":".facebook.com","path":"\/","secure":true,"httponly":true},{"name":"datr","value":"hhYvXNtLnDEsGiVx0iQnsirQ","expires":"Sun, 03 Jan 2021 08:17:10 GMT","expires_timestamp":1609661830,"domain":".facebook.com","path":"\/","secure":true,"httponly":true}],"confirmed":true,"user_storage_key":"bcbb56eeff8ddbe75cd345c3ba9924d6c1131d4418a54148681ef134969c0b52"}')
        self.cookies = output['session_cookies']
#        self.cookies='{"session_key":"5.6SiTlBOjqP-Hmg.1546588214.36-100032186028208","uid":100032186028208,"secret":"eda12d1a93bf099176b0bbade9e2bdea","access_token":"EAAAAUaZA8jlABAE5rhNuV8A6TQ6MhXLATwCTlZCB5HOCffcZB9D7750ZCbbZBviXfMXYiZAYITu9BiFjfFj2YssB9ZAyS4veg8FIoKqrZAhy9wQEZBZA6Dt4RXrlgCgYXrGA3VHPhaBd3alQRYx91SKWESZCWaSju6SQcYgX1kHzMGygA8T06Ctriq1","machine_id":"hhYvXNtLnDEsGiVx0iQnsirQ","session_cookies":[{"name":"c_user","value":"100032186028208","expires":"Sat, 04 Jan 2020 08:17:10 GMT","expires_timestamp":1578125830,"domain":".facebook.com","path":"\/","secure":true},{"name":"xs","value":"36:6SiTlBOjqP-Hmg:2:1546588214:-1:-1","expires":"Sat, 04 Jan 2020 08:17:10 GMT","expires_timestamp":1578125830,"domain":".facebook.com","path":"\/","secure":true,"httponly":true},{"name":"fr","value":"1Rh4Gyai9U08pgyhP.AWVmPT7-P1nlzM0MAk3PuRiQrQA.BcLxaG..AAA.0.0.BcLxaG.AWWbwpFy","expires":"Sat, 04 Jan 2020 08:17:10 GMT","expires_timestamp":1578125830,"domain":".facebook.com","path":"\/","secure":true,"httponly":true},{"name":"datr","value":"hhYvXNtLnDEsGiVx0iQnsirQ","expires":"Sun, 03 Jan 2021 08:17:10 GMT","expires_timestamp":1609661830,"domain":".facebook.com","path":"\/","secure":true,"httponly":true}],"confirmed":true,"user_storage_key":"bcbb56eeff8ddbe75cd345c3ba9924d6c1131d4418a54148681ef134969c0b52"}'
        print self.cookies
#        cookies = [self._format_cookie(x) for x in self.cookies]
#................................................................

    handle_httpstatus_list = [403, 404]
    name = "getpagekeyword"
    # download_delay = 2
    allowed_domains = ["facebook.com"]
    def _format_cookie(self, cookie):
        # build cookie string
        cookie_str = '%s=%s' % (cookie['name'], cookie['value'])

        if cookie.get('path', None):
            cookie_str += '; Path=%s' % cookie['path']
        if cookie.get('domain', None):
            cookie_str += '; Domain=%s' % cookie['domain']

        return cookie_str

    def start_requests(self):
        yield Request("https://mbasic.facebook.com/")

    def mysqConnect(self):
        self.conn = MySQLdb.connect(user=self.extra_config.get('mysql','db_username'),
                                    passwd=self.extra_config.get('mysql','db_password'),
                                    db=self.extra_config.get('mysql','db_name'),
                                    host=self.extra_config.get('mysql','db_host'),
                                    charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    def getPromoteByArticle(self, status):
        price = 0

        if status == 10:
            price = 1

        if status == 5:
            price = 0.5

        if status == 1:
            price = 0.1

        return price

    def mysqlQuery(self, sql, params=()):
        try:
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except (AttributeError, MySQLdb.OperationalError) as e:
            print 'exception generated during sql connection: ', e
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor

    def getListUid(self):
        sql = """SELECT account, keywords FROM account_keywords WHERE status = 1 AND account = %s"""
        curr = self.mysqlQuery(sql, (self.account,))
        if curr :
            return curr.fetchall()
        else:
            return []

    def parse(self, response):
        # ------------------

        print "================ LOGIN LOG WITH COOKIE ===================="
        print self.cookies
        print "================ END LOGIN LOG WITH COOKIE ================"

        return FormRequest.from_response(
            response,
 #           formxpath='.//form[@id="login_form"]',
 #           formdata={'email': user_name, 'pass': user_pass},
            headers = self.headers, 
            cookies = self.cookies,
            callback=self.after_login,
            dont_filter=True
        )

    def after_login(self, response):
        try:
            print("\n------------------------")
            sql = """SELECT id, keyword FROM keyword where status_page =10"""
            curr = self.query(sql)
            if curr :
                list_data = curr.fetchall()
                print list_data
                if len(list_data) > 0:
                    for row in list_data:
                        print row
                        id, keyword = row
                        print id, keyword
                        keyword = u''.join(keyword).encode('utf-8').strip()
                        url_request = "https://m.facebook.com/search/pages/?q="+urllib.quote(keyword)
                        meta = {"keyword": keyword, "page": 1}
                        yield SplashRequest(url_request.strip().encode("utf-8"), self.parse_info, endpoint='render.html', args=self.splash_args, headers=self.headers, meta=meta)
                        self.update_keyword(id)
        except Exception:
            print "Error"

#        self.update_page(1, "1552352435062030", "Tiem banh cua me", 1, "DRINK", "banh","GR1")
#        keyword = u'trà sữa'
#        keyword = u''.join(keyword).encode('utf-8').strip()
#        url_request = "https://m.facebook.com/search/pages/?q="+urllib.quote(keyword)
#        meta = {"keyword": keyword, "page": 1}
#        yield SplashRequest(url_request.strip().encode("utf-8"), self.parse_info, endpoint='render.html', args=self.splash_args, headers=self.headers, meta=meta)



    def parse_info(self, response):
#        print response.body
        keyword = response.meta["keyword"]
        try:
            posts = response.body.split('page_id:')
            if len(posts) > 0:
                i = 0
                for post in posts:
                    if i > 0:
                        try:
                            fb_post_id = str(post.split(',')[0]).strip()
                            fb_post_id = fb_post_id.replace('"', "").strip()
                            print fb_post_id
                            output = {}
                            output['keyword'] = keyword
                            output["page_id"] = fb_post_id
                            meta = {"page_id": fb_post_id, "keyword" : keyword}
                            url_request = "https://graph.facebook.com/v1.0/"+str(fb_post_id)+"?"+self.access_token
                            yield Request(url_request, callback=self.parse_result_info, meta=meta)
                            json_data = json.dumps(output)
#                    open(filename2, 'a+').write(json_data+"\n")
                            with open(self.filename2, 'a') as f:
                                f.write(json_data+"\n")
                        except Exception, f:
                            pass
                    i+=1
            cursors = response.body.split('see_more_pager",')
#            print cursors
            if len(cursors) >0:
                for cursor in cursors:
                    try:
                        url_cursor = str(cursor.split('",')[0]).strip()
                        if "&cursor=" in url_cursor:
                            url_cursor = url_cursor.replace('"',"").replace("href:","")
#.replace("\\","")
                            print url_cursor
                            yield SplashRequest(url_cursor.strip().encode("utf-8"), self.parse_info, endpoint='render.html', args=self.splash_args, headers=self.headers, meta=response.meta)
                    except Exception, f:
                        print f

            print "HAVE NO CURSOR" 
        except Exception,f:
            print "HAVE NO CURSOR"
            pass

    def getInfoByPageId(self, post_id):
        api_endpoint = "https://graph.facebook.com/v1.0/"+post_id+"?"+self.access_token
#        access_token=EAAAAUaZA8jlABAKvm9DDwP8rAdsWgfyfVh20MYYlXL0Ku8NBHwuCGNTRAUD7POZBn1wD75iitMbi0wLPe5JQy5vH7Y7KRSgpYUAO4RY8GywF1HZCcI9bOlUEUZCpzQwaer8PbomKaQGKuvZBXEIvMZBOvxHfqpXYyu5yroCXqrp8Buarn84efC"

        print "==================="
        print api_endpoint
        print "==================="

        api_request = urllib2.Request(api_endpoint)
        api_response = urllib2.urlopen(api_request)
        return json.loads(api_response.read())

    def parse_result_info(self, response):
        keyword = response.meta["keyword"]
        page_id = response.meta["page_id"]
        true_info = json.loads(response.body)
        if "name" in true_info:
            page_name = true_info["name"]
            self.update_page(1, page_id, page_name, 1, "DRINK", keyword,"GR1")

    def checkErrorFbPostId(self, fb_post_id):
        sql = """SELECT fb_post_id FROM account_error WHERE fb_post_id = %s LIMIT 1"""
        process_info = self.mysqlQuery(sql,(fb_post_id,))
        list_data = process_info.fetchone()
        if(list_data):
            return True
        else:
            return False
    def update_keyword(self,_id):
        current_time = time.time()
        sql1 = """UPDATE keyword set status_page=0 WHERE id = %s"""
        process_info = self.query(sql1,([_id]))

    def update_page(self,page_type, page_id, page_name, created_by, branch, keyword,group_page):
        current_time = time.time()
        sql1 = """SELECT id FROM fb_page WHERE page_id = %s"""
        process_info = self.query(sql1,([page_id]))
        check = 0
        if process_info :
            list_data = process_info.fetchall()
            check = len(list_data)
        print check
        if check ==0:
            sql = """INSERT INTO fb_page(page_type, page_id, page_name, created_by, created_at, updated_at, branch, keyword,group_page) VALUE (%s, %s, %s, %s, %s, %s, %s, %s, %s)"""
            curr = self.query(sql,(page_type, page_id, page_name, created_by, current_time, current_time, branch, keyword,group_page))

    def query(self, sql, params=()):
        try:
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except (AttributeError, MySQLdb.OperationalError) as e:
            print 'exception generated during sql connection: ', e
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor


    def _set_raw(self, fb_current_id, time_post):
        vietname_zone = pytz.timezone('Asia/Ho_Chi_Minh')
        time_get = vietname_zone.localize(datetime.fromtimestamp(time.time())).strftime(self.DATETIME_FORMAT)
        sql = """INSERT INTO users_posts_test(fb_post_id, created_time, indexed, time_post, time_get) VALUE (%s, %s, %s, %s, %s)"""
        curr = self.mysqlQuery(sql,(fb_current_id, int(time.time()), 1, time_post, time_get))

    def _set_error(self, fb_post_id):
        sql = """INSERT INTO account_error(fb_post_id) VALUE (%s)"""
        curr = self.mysqlQuery(sql,(fb_post_id,))

    def updateStatusUser(self, fb_current_id):
        sql = """UPDATE users_posts_id SET `status` = 1 WHERE `fb_post_id` = %s"""
        curr = self.mysqlQuery(sql,(fb_current_id,))

    def updateStatusPage(self, fb_current_id):
        sql = """UPDATE users_posts_id SET `status` = 3 WHERE `fb_post_id` = %s"""
        curr = self.mysqlQuery(sql,(fb_current_id,))

    def updateStatusUserVer3(self, fb_current_id):
        sql = """UPDATE users_posts_id SET `status` = 4 WHERE `fb_post_id` = %s"""
        curr = self.mysqlQuery(sql,(fb_current_id,))

    def updateStatusUserVer2(self, fb_current_id):
        sql = """UPDATE users_posts_id SET `status` = 5 WHERE `fb_post_id` = %s"""
        curr = self.mysqlQuery(sql,(fb_current_id,))

    def _join_data(self, separator=None, data=None):
        if(separator != None and data != None):
            if len(data) > 0:
                s = data[0]
                for i in range(1, len(data)):
                    if(data[i]):
                        s = s + separator + data[i].strip()
                return s
        return ''

    def _to_dict(self, data):
        d = {}
        if(len(data) > 0):
            for i in range(0, len(data)):
                d[i] = data[i]
        return d