# -*- encoding: utf-8 -*-

import scrapy
import urllib2
import json
import time
import MySQLdb
from scrapy.http import Request
import ConfigParser
import os
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import sys
from datetime import datetime
from datetime import timedelta
import math

#import pymongo
#from pymongo import MongoClient
from scrapy.utils.project import get_project_settings
from elasticsearch import Elasticsearch
from helper import Helper
from token_facebook import tokenfacebook
import ConfigParser


class ChecktokenSpider(scrapy.Spider):
    def __init__(self, *args, **kwargs):
        config = ConfigParser.ConfigParser()
        config.read('extra_config.cfg')
        self.extra_config = config
        self.parent_table = "fb_page"
        self.mysqConnect()
        self.DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        self.DATETIME_FORMAT_BEGIN = "%Y-%m-%d 00:00:00"
        self.DATETIME_FORMAT_END = "%Y-%m-%d 23:59:59"
        self.actions=[]

        self.xpath_box = './/div[@class="hidden_elem"]//comment()'
        self.xpath_post = './/p/text() | .//p//a//text() | .//div[@class="mtm"]//div/text() | .//div[@class="mtm"]//a/text() | (.//span[@role="presentation"])[last()]/text()'
        self.xpath_post_user_name = './/h5//a/text()'
        self.xpath_post_user_id = './/a/@data-hovercard'
        self.xpath_post_created = './/abbr/@data-utime'
        self.DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S%z"
        
        self.es_doc_type = config.get('elasticsearch', 'doc_type')
        self.es_index = config.get('elasticsearch', 'index')
        self.es = Elasticsearch([config.get('elasticsearch', 'dns')])

        dispatcher.connect(self.spider_closed, signals.spider_closed)
        self.tokens = tokenfacebook().getconfigToken1()
        self.tokens_remove = []

    handle_httpstatus_list = [403, 400]
    name = "checktokenmysql"
    allowed_domains = ["facebook.com"]

    #mysql connection db
    def mysqConnect(self):
        self.conn = MySQLdb.connect(user=self.extra_config.get('mysql','db_username'),
                                    passwd=self.extra_config.get('mysql','db_password'),
                                    db=self.extra_config.get('mysql','db_name'),
                                    host=self.extra_config.get('mysql','db_host'),
                                    charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()
    def mysqlQuery(self, sql, params=()):
        try:
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except (AttributeError, MySQLdb.OperationalError) as e:
            print 'exception generated during sql connection: ', e
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor
    def query(self, sql, params=()):
        try:
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except (AttributeError, MySQLdb.OperationalError) as e:
            print 'exception generated during sql connection: ', e
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor

    def update_keyword(self,_id):
        current_time = time.time()
        sql1 = """UPDATE fb_token set status=0 WHERE id = %s"""
        process_info = self.query(sql1,([_id]))

    def start_requests(self):
        tokens = tokenfacebook().getconfigToken1()
        # print self.tokens[0]
        # self.tokens.remove(self.tokens[0])
        # print self.tokens[0]
        # # i = 0
        for token in tokens:
            # print "==================="
            # print token["access_token"]
            # print "==================="

            url = "https://graph.facebook.com/v1.0/me/?access_token="+token["access_token"]
            yield Request(url, meta={"access_token": token["access_token"]})
        try:
            print("\n------------------------")
            sql = """SELECT id, token FROM fb_token where status =10"""
            curr = self.query(sql)
            if curr :
                list_data = curr.fetchall()
                print list_data
                if len(list_data) > 0:
                    for row in list_data:
                        print row
                        id, token = row
                        print id, token
                        url = "https://graph.facebook.com/v1.0/me/?access_token="+token
                        yield Request(url, meta={"access_token": token})
                        self.update_keyword(id)
        except Exception, f:
            print f

    def parse(self, response):
        jsonresponse = json.loads(response.body_as_unicode())
        if response.status == 403 or response.status == 400:
            print "===================================="
            # print jsonresponse["error"]["error_subcode"]
            # 452: The session has been invalidated because
            # 490: Checkpoint account
            print response.meta["access_token"]
            print jsonresponse["error"]["error_subcode"]
            self.tokens_remove.append(response.meta["access_token"])
            print "===================================="
        # else:

    def spider_closed(self, spider, reason):
        for token in self.tokens:
            for token_remove in self.tokens_remove:
                if token["access_token"] == token_remove:
                    self.tokens.remove(token)

        string = ""
        for token_remove in self.tokens_remove:
            string = string + token_remove + "\n"

        # SAVE FILE TOKEN DIE
        filename = "response.txt"
        open(filename, 'wb').write(string)

        # SAVE FILE HELPER
        file_object = open("/data/python_prj/fb_crawler/fb_crawler/spiders/token_facebook.py", "r")
        info_object = file_object.read()
        file_object.close()

        string = info_object.split("= [")[1].split("]")[0]

        new_string = "\n"
        for token in self.tokens:
            new_string = new_string + '\t\t\t{"access_token": "'+token["access_token"]+'"}, \n'

        file_object = open("/data/python_prj/fb_crawler/fb_crawler/spiders/token_facebook.py", "w")
        file_object.write(info_object.replace(string, new_string))
        file_object.close()

        print "============================"
        print self.tokens
        print len(self.tokens)
        print len(self.tokens_remove)
        print "============================"
