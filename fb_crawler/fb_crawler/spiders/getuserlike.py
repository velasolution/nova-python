# -*- coding: utf-8 -*-

import scrapy
from scrapy.conf import settings
from scrapy.http import Request
from scrapy.http import FormRequest
import MySQLdb
from elasticsearch import Elasticsearch
import time
from datetime import datetime
from datetime import date
from scrapy.http.headers import Headers
from scrapy_splash import SplashRequest
from scrapy.selector import Selector
import json
from pytz import timezone
import pytz
import urllib
import demjson
from random import randrange
import urllib2
import dateutil.parser
import calendar
import random
import six
from token_facebook import tokenfacebook
from cookie_facebooks import cookiefacebooks

class GetNewPostEachAccountCustomSpider(scrapy.Spider):
    def __init__(self, account=None, type=None, real_type=None, type_channel=None):
        self.xpath_box = './/div[@class="hidden_elem"]//comment()'
        self.xpath_post = './/p/text() | .//p//a//text() | .//div[@class="mtm"]//div/text() | .//div[@class="mtm"]//a/text() | (.//span[@role="presentation"])[last()]/text()'
        self.xpath_post_user_name = './/h5//a/text()'
        self.xpath_post_user_id = './/a/@data-hovercard'
        self.xpath_post_created = './/abbr/@data-utime'
        self.DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S%z"
        self.cookies =""
        self.filename2 = "/data/python_prj/fb_crawler/fb_crawler/spiders/user_like.txt"
        self.headers = Headers(
            {
                'Content-Type': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'User-Agent'  : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'
            }
        )
        script = """
        function main(splash)
            assert(splash:go(splash.args.url))
        end
        """
        self.splash_args = {
            'html': 1,
            'png': 1,
            'width': 1920,
            'height': 1080,
            'render_all': 1,
            'wait': 20,
            'lua_source': script
        }

        if(type == None):
            self.type = 1
        else:
            self.type = int(type)

        if(real_type == None):
            self.real_type = 0
        else:
            self.real_type = int(real_type)

# Load access_token.........................................

        if(type_channel == None):
            self.type_channel = 0
        else:
            self.type_channel = int(type_channel)

        tokens = tokenfacebook().getconfigToken1()
        tms = len(tokens)
        if(type_channel == None):
            self.type_channel = random.randrange(0,tms)
        else:
            self.type_channel = int(type_channel)

        self.limit_docs = 50
        token = "access_token="+tokens[self.type_channel]["access_token"]
        self.access_token = token
        print self.access_token

# Load cookie...................................................

        list_cookie = cookiefacebooks().getconfigCookie()
        tms = len(list_cookie)
        type_cookie = random.randrange(0,tms)
        cookie_user = list_cookie[type_cookie]["cookie"]
        output = json.loads(cookie_user)
#        print output['session_cookies']
#        output = json.loads('{"session_key":"5.zYlmEDD6yutvzg.1545293866.25-100031394985827","uid":100031394985827,"secret":"e1e0427a7c583a743a2a72c17e584286","access_token":"EAAAAUaZA8jlABANbFoiH4cf3PGr56QOsaifc1osmkwhrTZAmMSLaGIRyRfL9GJQnKqZAFW2XQWA2DS1t6Ty6f9nHUcYqXHP50JxCdWB4RTRLZCGlcmYsQ16MkZAzcIByx0NWJd2njbKSnSqFnt8TH05lSpl5c7wOewwXWHRII5gZDZD","machine_id":"qz5iXP8w82ooOoX-6HC6m6DH","session_cookies":[{"name":"c_user","value":"100031394985827","expires":"Wed, 12 Feb 2020 03:34:03 GMT","expires_timestamp":1581478443,"domain":".facebook.com","path":"\/","secure":true},{"name":"xs","value":"25:zYlmEDD6yutvzg:2:1545293866:2953:16301","expires":"Wed, 12 Feb 2020 03:34:03 GMT","expires_timestamp":1581478443,"domain":".facebook.com","path":"\/","secure":true,"httponly":true},{"name":"fr","value":"17Qb9nT46TCufB1b8.AWURGcBuk8gwrKReOhANX7J3oFU.BcYj6r..AAA.0.0.BcYj6r.AWXMB_QC","expires":"Wed, 12 Feb 2020 03:34:03 GMT","expires_timestamp":1581478443,"domain":".facebook.com","path":"\/","secure":true,"httponly":true},{"name":"datr","value":"qz5iXP8w82ooOoX-6HC6m6DH","expires":"Thu, 11 Feb 2021 03:34:03 GMT","expires_timestamp":1613014443,"domain":".facebook.com","path":"\/","secure":true,"httponly":true}],"confirmed":true,"user_storage_key":"73d4274ad108e2bd6c234ba3602c483076a7ab50089e489977942844b8469c7a"}')
        self.cookies = output['session_cookies']
#        self.cookies='{"session_key":"5.6SiTlBOjqP-Hmg.1546588214.36-100032186028208","uid":100032186028208,"secret":"eda12d1a93bf099176b0bbade9e2bdea","access_token":"EAAAAUaZA8jlABAE5rhNuV8A6TQ6MhXLATwCTlZCB5HOCffcZB9D7750ZCbbZBviXfMXYiZAYITu9BiFjfFj2YssB9ZAyS4veg8FIoKqrZAhy9wQEZBZA6Dt4RXrlgCgYXrGA3VHPhaBd3alQRYx91SKWESZCWaSju6SQcYgX1kHzMGygA8T06Ctriq1","machine_id":"hhYvXNtLnDEsGiVx0iQnsirQ","session_cookies":[{"name":"c_user","value":"100032186028208","expires":"Sat, 04 Jan 2020 08:17:10 GMT","expires_timestamp":1578125830,"domain":".facebook.com","path":"\/","secure":true},{"name":"xs","value":"36:6SiTlBOjqP-Hmg:2:1546588214:-1:-1","expires":"Sat, 04 Jan 2020 08:17:10 GMT","expires_timestamp":1578125830,"domain":".facebook.com","path":"\/","secure":true,"httponly":true},{"name":"fr","value":"1Rh4Gyai9U08pgyhP.AWVmPT7-P1nlzM0MAk3PuRiQrQA.BcLxaG..AAA.0.0.BcLxaG.AWWbwpFy","expires":"Sat, 04 Jan 2020 08:17:10 GMT","expires_timestamp":1578125830,"domain":".facebook.com","path":"\/","secure":true,"httponly":true},{"name":"datr","value":"hhYvXNtLnDEsGiVx0iQnsirQ","expires":"Sun, 03 Jan 2021 08:17:10 GMT","expires_timestamp":1609661830,"domain":".facebook.com","path":"\/","secure":true,"httponly":true}],"confirmed":true,"user_storage_key":"bcbb56eeff8ddbe75cd345c3ba9924d6c1131d4418a54148681ef134969c0b52"}'
        print self.cookies
#        cookies = [self._format_cookie(x) for x in self.cookies]
#................................................................

    handle_httpstatus_list = [403, 404]
    name = "getuserlike"
    # download_delay = 2
    allowed_domains = ["facebook.com"]
    def _format_cookie(self, cookie):
        # build cookie string
        cookie_str = '%s=%s' % (cookie['name'], cookie['value'])

        if cookie.get('path', None):
            cookie_str += '; Path=%s' % cookie['path']
        if cookie.get('domain', None):
            cookie_str += '; Domain=%s' % cookie['domain']

        return cookie_str

    def start_requests(self):
        yield Request("https://mbasic.facebook.com/")


    def parse(self, response):
        # ------------------

        print "================ LOGIN LOG WITH COOKIE ===================="
        print self.cookies
        print "================ END LOGIN LOG WITH COOKIE ================"

        return FormRequest.from_response(
            response,
 #           formxpath='.//form[@id="login_form"]',
 #           formdata={'email': user_name, 'pass': user_pass},
            headers = self.headers, 
            cookies = self.cookies,
            callback=self.after_login,
            dont_filter=True
        )

    def after_login(self, response):
        dataPost = [           
            "2152895088104974",
        ]
        for fb_id in dataPost:
            url_request = "https://m.facebook.com/ufi/reaction/profile/browser/fetch/?limit=3000&ft_ent_identifier=2152895088104974&total_count=100000"
            meta = {"keyword": fb_id, "page": 1}
            yield SplashRequest(url_request.strip().encode("utf-8"), self.parse_info, endpoint='render.html', args=self.splash_args, headers=self.headers, meta=meta)



    def parse_info(self, response):
#        print response.body
        keyword = response.meta["keyword"]
        cursor ="&shown_ids="
        try:
            posts = response.body.split('shown_ids=')
            check = 0
            check_cursor = 0
            if len(posts) > 0:
                for post in posts:
                    if '&ft_ent_identifier' in post:
                        check =1
                        try:
                            fb_post_id = str(post.split('&ft_ent_identifier')[0]).strip()
                            fb_post_id = fb_post_id.replace('"', "").strip()
                            if check_cursor ==0:
                                cursor = cursor + fb_post_id
                                check_cursor =1
                        except Exception, f:
                            pass
            if check ==0:
                for post in posts:
                    if '&amp;ft_ent_identifier' in post:
                        try:
                            fb_post_id = str(post.split('&amp;ft_ent_identifier')[0]).strip()
                            fb_post_id = fb_post_id.replace('"', "").strip()
                            if check_cursor ==0:
                                cursor = cursor + fb_post_id
                                check_cursor =1
                            print fb_post_id
                        except Exception, f:
                            pass
        except Exception,f:
            pass
        cursor = cursor.replace("&shown_ids=%2C", "&shown_ids=")
        cursor = cursor.replace("\u00252C", "%2C")
        cursor = cursor.replace("&amp;", "&")
        print "cursor" +cursor
        print len(cursor)

        uids = cursor.split('%2C')
        for uid in uids:
            uid = uid.replace("&shown_ids=","")
            uid = str(uid.split('&total_count=')[0]).strip()
            output = {}
            output['post_id'] = keyword
            output["uid"] = uid
            json_data = json.dumps(output)
#                    open(filename2, 'a+').write(json_data+"\n")
            with open(self.filename2, 'a') as f:
                f.write(json_data+"\n")



        if len(cursor)>100:
            if '%2C' in cursor:
                url_request = "https://m.facebook.com/ufi/reaction/profile/browser/fetch/?limit=3000&ft_ent_identifier=2152895088104974"+cursor
                meta = {"keyword": keyword, "page": 1}
                yield SplashRequest(url_request.strip().encode("utf-8"), self.parse_info, endpoint='render.html', args=self.splash_args, headers=self.headers, meta=meta,dont_filter = True)