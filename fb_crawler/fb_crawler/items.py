# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class fb_crawlerItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    # ten nguoi post
    fb_from_name = scrapy.Field()
    # user id nguoi post
    fb_from_uid = scrapy.Field()
    # Noi dung cua post
    fb_message = scrapy.Field()
    # Thoi diem tao post
    fb_created = scrapy.Field()
    # Thoi diem sua post
    fb_updated = scrapy.Field()
    # So shared cua post
    fb_share_count = scrapy.Field()
    # So like cua post
    fb_like_count = scrapy.Field()
    # Id cua post
    fb_post_id = scrapy.Field()
    # Type cua post (photo, link, ...)
    fb_post_type = scrapy.Field()
    # Link post (neu co)
    fb_post_link = scrapy.Field()
    # Du lieu lay duoc tu facebook
    fb_data = scrapy.Field()
    # Id facebook page
    fb_page_id = scrapy.Field()
    # Ten facebook page
    fb_page_name = scrapy.Field()
    # convert tu fb_updated
    fb_updated_int = scrapy.Field()
    # facebook id cha
    fb_parent_id = scrapy.Field()
    # facebook id ong
    fb_grand_parent_id = scrapy.Field()
    # Nganh
    branch = scrapy.Field()
    sub_branch = scrapy.Field()
    # Da crawl 1, can crawl lai 0
    is_crawl = scrapy.Field()
    # Thoi diem crawl dau tien
    crawled_first_time = scrapy.Field()
    # Thoi diem crawl gan nhat
    crawled_time = scrapy.Field()
    # Tag
    tag = scrapy.Field()
    # Doi tuong
    object = scrapy.Field()
    # Ten doi tuong page
    page_name_object = scrapy.Field()
    # Trung 1, khong trung 0
    is_duplicated = scrapy.Field()
    # Da hash message check trung 1, chua hash 0
    is_hashed = scrapy.Field()
    # Kieu :(0 la post, 1 la comment, 2 la reply)
    post_type = scrapy.Field()
    # (id bo do mongo tu sinh)
    parent_id = scrapy.Field()
    # (id ong do mongo tu sinh)
    grand_parent_id = scrapy.Field()
    # child_count: (Tong so con cua mot message, mac dinh la 0, post = count comment + count reply)
    child_count = scrapy.Field()
    # group page
    group_page = scrapy.Field()
    # Da chay qua bo loc 1, chua chay 0


	
    pass
